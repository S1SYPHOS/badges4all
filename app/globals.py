from os.path import abspath, dirname, join

from flask import current_app as app
from jinja2 import Markup

from .utils import load_json


def current_version() -> str:
    # Get directory below this one
    path = dirname(dirname(abspath(__file__)))

    # Load JSON data from `package.json` file
    data = load_json(join(path, 'package.json'))

    # Return current version (or placeholder)
    return data['version'] if 'version' in data else '1.0.0'


def include_file(name, string=''):
    directory = app.static_folder
    target_file = join(directory, name)

    with open(target_file, 'r') as file:
        return Markup(file.read())
