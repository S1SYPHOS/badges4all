import 'alpinejs'

// Extend 'Window' object
declare global {
    interface Window {
        openFullscreen:any;
        closeFullscreen:any;
    }
}

// See https://www.w3schools.com/howto/howto_js_fullscreen.asp
const openFullscreen = function() {
    const elem = document.documentElement;

    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    }

    /* Safari */
    if (elem.webkitRequestFullscreen) {
        elem.webkitRequestFullscreen();
    }

    /* IE11 */
    if (elem.msRequestFullscreen) {
        elem.msRequestFullscreen();
    }
}

const closeFullscreen = function() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    }

    /* Safari */
    if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }

    /* IE11 */
    if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
}

window.openFullscreen = openFullscreen
window.closeFullscreen = closeFullscreen
