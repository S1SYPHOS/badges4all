from flask import Blueprint, abort


# Initialize blueprint
logs = Blueprint('logs', __name__)


@logs.route('/')
def idx():
    abort(403)  # Forbidden
