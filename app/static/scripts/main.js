(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[Object.keys(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __reExport = (target, module, desc) => {
    if (module && typeof module === "object" || typeof module === "function") {
      for (let key of __getOwnPropNames(module))
        if (!__hasOwnProp.call(target, key) && key !== "default")
          __defProp(target, key, { get: () => module[key], enumerable: !(desc = __getOwnPropDesc(module, key)) || desc.enumerable });
    }
    return target;
  };
  var __toModule = (module) => {
    return __reExport(__markAsModule(__defProp(module != null ? __create(__getProtoOf(module)) : {}, "default", module && module.__esModule && "default" in module ? { get: () => module.default, enumerable: true } : { value: module, enumerable: true })), module);
  };
  var __async = (__this, __arguments, generator) => {
    return new Promise((resolve, reject) => {
      var fulfilled = (value) => {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      };
      var rejected = (value) => {
        try {
          step(generator.throw(value));
        } catch (e) {
          reject(e);
        }
      };
      var step = (x) => x.done ? resolve(x.value) : Promise.resolve(x.value).then(fulfilled, rejected);
      step((generator = generator.apply(__this, __arguments)).next());
    });
  };

  // node_modules/alpinejs/dist/alpine.js
  var require_alpine = __commonJS({
    "node_modules/alpinejs/dist/alpine.js"(exports, module) {
      (function(global, factory) {
        typeof exports === "object" && typeof module !== "undefined" ? module.exports = factory() : typeof define === "function" && define.amd ? define(factory) : (global = global || self, global.Alpine = factory());
      })(exports, function() {
        "use strict";
        function _defineProperty(obj, key, value) {
          if (key in obj) {
            Object.defineProperty(obj, key, {
              value,
              enumerable: true,
              configurable: true,
              writable: true
            });
          } else {
            obj[key] = value;
          }
          return obj;
        }
        function ownKeys(object, enumerableOnly) {
          var keys = Object.keys(object);
          if (Object.getOwnPropertySymbols) {
            var symbols = Object.getOwnPropertySymbols(object);
            if (enumerableOnly)
              symbols = symbols.filter(function(sym) {
                return Object.getOwnPropertyDescriptor(object, sym).enumerable;
              });
            keys.push.apply(keys, symbols);
          }
          return keys;
        }
        function _objectSpread2(target) {
          for (var i = 1; i < arguments.length; i++) {
            var source = arguments[i] != null ? arguments[i] : {};
            if (i % 2) {
              ownKeys(Object(source), true).forEach(function(key) {
                _defineProperty(target, key, source[key]);
              });
            } else if (Object.getOwnPropertyDescriptors) {
              Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
            } else {
              ownKeys(Object(source)).forEach(function(key) {
                Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
              });
            }
          }
          return target;
        }
        function domReady() {
          return new Promise((resolve) => {
            if (document.readyState == "loading") {
              document.addEventListener("DOMContentLoaded", resolve);
            } else {
              resolve();
            }
          });
        }
        function arrayUnique(array) {
          return Array.from(new Set(array));
        }
        function isTesting() {
          return navigator.userAgent.includes("Node.js") || navigator.userAgent.includes("jsdom");
        }
        function checkedAttrLooseCompare(valueA, valueB) {
          return valueA == valueB;
        }
        function warnIfMalformedTemplate(el, directive) {
          if (el.tagName.toLowerCase() !== "template") {
            console.warn(`Alpine: [${directive}] directive should only be added to <template> tags. See https://github.com/alpinejs/alpine#${directive}`);
          } else if (el.content.childElementCount !== 1) {
            console.warn(`Alpine: <template> tag with [${directive}] encountered with an unexpected number of root elements. Make sure <template> has a single root element. `);
          }
        }
        function kebabCase(subject) {
          return subject.replace(/([a-z])([A-Z])/g, "$1-$2").replace(/[_\s]/, "-").toLowerCase();
        }
        function camelCase(subject) {
          return subject.toLowerCase().replace(/-(\w)/g, (match, char) => char.toUpperCase());
        }
        function walk(el, callback) {
          if (callback(el) === false)
            return;
          let node = el.firstElementChild;
          while (node) {
            walk(node, callback);
            node = node.nextElementSibling;
          }
        }
        function debounce(func, wait) {
          var timeout;
          return function() {
            var context = this, args = arguments;
            var later = function later2() {
              timeout = null;
              func.apply(context, args);
            };
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
          };
        }
        const handleError = (el, expression, error) => {
          console.warn(`Alpine Error: "${error}"

Expression: "${expression}"
Element:`, el);
          if (!isTesting()) {
            Object.assign(error, {
              el,
              expression
            });
            throw error;
          }
        };
        function tryCatch(cb, {
          el,
          expression
        }) {
          try {
            const value = cb();
            return value instanceof Promise ? value.catch((e) => handleError(el, expression, e)) : value;
          } catch (e) {
            handleError(el, expression, e);
          }
        }
        function saferEval(el, expression, dataContext, additionalHelperVariables = {}) {
          return tryCatch(() => {
            if (typeof expression === "function") {
              return expression.call(dataContext);
            }
            return new Function(["$data", ...Object.keys(additionalHelperVariables)], `var __alpine_result; with($data) { __alpine_result = ${expression} }; return __alpine_result`)(dataContext, ...Object.values(additionalHelperVariables));
          }, {
            el,
            expression
          });
        }
        function saferEvalNoReturn(el, expression, dataContext, additionalHelperVariables = {}) {
          return tryCatch(() => {
            if (typeof expression === "function") {
              return Promise.resolve(expression.call(dataContext, additionalHelperVariables["$event"]));
            }
            let AsyncFunction = Function;
            AsyncFunction = Object.getPrototypeOf(function() {
              return __async(this, null, function* () {
              });
            }).constructor;
            if (Object.keys(dataContext).includes(expression)) {
              let methodReference = new Function(["dataContext", ...Object.keys(additionalHelperVariables)], `with(dataContext) { return ${expression} }`)(dataContext, ...Object.values(additionalHelperVariables));
              if (typeof methodReference === "function") {
                return Promise.resolve(methodReference.call(dataContext, additionalHelperVariables["$event"]));
              } else {
                return Promise.resolve();
              }
            }
            return Promise.resolve(new AsyncFunction(["dataContext", ...Object.keys(additionalHelperVariables)], `with(dataContext) { ${expression} }`)(dataContext, ...Object.values(additionalHelperVariables)));
          }, {
            el,
            expression
          });
        }
        const xAttrRE = /^x-(on|bind|data|text|html|model|if|for|show|cloak|transition|ref|spread)\b/;
        function isXAttr(attr) {
          const name = replaceAtAndColonWithStandardSyntax(attr.name);
          return xAttrRE.test(name);
        }
        function getXAttrs(el, component, type) {
          let directives = Array.from(el.attributes).filter(isXAttr).map(parseHtmlAttribute);
          let spreadDirective = directives.filter((directive) => directive.type === "spread")[0];
          if (spreadDirective) {
            let spreadObject = saferEval(el, spreadDirective.expression, component.$data);
            directives = directives.concat(Object.entries(spreadObject).map(([name, value]) => parseHtmlAttribute({
              name,
              value
            })));
          }
          if (type)
            return directives.filter((i) => i.type === type);
          return sortDirectives(directives);
        }
        function sortDirectives(directives) {
          let directiveOrder = ["bind", "model", "show", "catch-all"];
          return directives.sort((a, b) => {
            let typeA = directiveOrder.indexOf(a.type) === -1 ? "catch-all" : a.type;
            let typeB = directiveOrder.indexOf(b.type) === -1 ? "catch-all" : b.type;
            return directiveOrder.indexOf(typeA) - directiveOrder.indexOf(typeB);
          });
        }
        function parseHtmlAttribute({
          name,
          value
        }) {
          const normalizedName = replaceAtAndColonWithStandardSyntax(name);
          const typeMatch = normalizedName.match(xAttrRE);
          const valueMatch = normalizedName.match(/:([a-zA-Z0-9\-:]+)/);
          const modifiers = normalizedName.match(/\.[^.\]]+(?=[^\]]*$)/g) || [];
          return {
            type: typeMatch ? typeMatch[1] : null,
            value: valueMatch ? valueMatch[1] : null,
            modifiers: modifiers.map((i) => i.replace(".", "")),
            expression: value
          };
        }
        function isBooleanAttr(attrName) {
          const booleanAttributes = ["disabled", "checked", "required", "readonly", "hidden", "open", "selected", "autofocus", "itemscope", "multiple", "novalidate", "allowfullscreen", "allowpaymentrequest", "formnovalidate", "autoplay", "controls", "loop", "muted", "playsinline", "default", "ismap", "reversed", "async", "defer", "nomodule"];
          return booleanAttributes.includes(attrName);
        }
        function replaceAtAndColonWithStandardSyntax(name) {
          if (name.startsWith("@")) {
            return name.replace("@", "x-on:");
          } else if (name.startsWith(":")) {
            return name.replace(":", "x-bind:");
          }
          return name;
        }
        function convertClassStringToArray(classList, filterFn = Boolean) {
          return classList.split(" ").filter(filterFn);
        }
        const TRANSITION_TYPE_IN = "in";
        const TRANSITION_TYPE_OUT = "out";
        const TRANSITION_CANCELLED = "cancelled";
        function transitionIn(el, show, reject, component, forceSkip = false) {
          if (forceSkip)
            return show();
          if (el.__x_transition && el.__x_transition.type === TRANSITION_TYPE_IN) {
            return;
          }
          const attrs = getXAttrs(el, component, "transition");
          const showAttr = getXAttrs(el, component, "show")[0];
          if (showAttr && showAttr.modifiers.includes("transition")) {
            let modifiers = showAttr.modifiers;
            if (modifiers.includes("out") && !modifiers.includes("in"))
              return show();
            const settingBothSidesOfTransition = modifiers.includes("in") && modifiers.includes("out");
            modifiers = settingBothSidesOfTransition ? modifiers.filter((i, index) => index < modifiers.indexOf("out")) : modifiers;
            transitionHelperIn(el, modifiers, show, reject);
          } else if (attrs.some((attr) => ["enter", "enter-start", "enter-end"].includes(attr.value))) {
            transitionClassesIn(el, component, attrs, show, reject);
          } else {
            show();
          }
        }
        function transitionOut(el, hide, reject, component, forceSkip = false) {
          if (forceSkip)
            return hide();
          if (el.__x_transition && el.__x_transition.type === TRANSITION_TYPE_OUT) {
            return;
          }
          const attrs = getXAttrs(el, component, "transition");
          const showAttr = getXAttrs(el, component, "show")[0];
          if (showAttr && showAttr.modifiers.includes("transition")) {
            let modifiers = showAttr.modifiers;
            if (modifiers.includes("in") && !modifiers.includes("out"))
              return hide();
            const settingBothSidesOfTransition = modifiers.includes("in") && modifiers.includes("out");
            modifiers = settingBothSidesOfTransition ? modifiers.filter((i, index) => index > modifiers.indexOf("out")) : modifiers;
            transitionHelperOut(el, modifiers, settingBothSidesOfTransition, hide, reject);
          } else if (attrs.some((attr) => ["leave", "leave-start", "leave-end"].includes(attr.value))) {
            transitionClassesOut(el, component, attrs, hide, reject);
          } else {
            hide();
          }
        }
        function transitionHelperIn(el, modifiers, showCallback, reject) {
          const styleValues = {
            duration: modifierValue(modifiers, "duration", 150),
            origin: modifierValue(modifiers, "origin", "center"),
            first: {
              opacity: 0,
              scale: modifierValue(modifiers, "scale", 95)
            },
            second: {
              opacity: 1,
              scale: 100
            }
          };
          transitionHelper(el, modifiers, showCallback, () => {
          }, reject, styleValues, TRANSITION_TYPE_IN);
        }
        function transitionHelperOut(el, modifiers, settingBothSidesOfTransition, hideCallback, reject) {
          const duration = settingBothSidesOfTransition ? modifierValue(modifiers, "duration", 150) : modifierValue(modifiers, "duration", 150) / 2;
          const styleValues = {
            duration,
            origin: modifierValue(modifiers, "origin", "center"),
            first: {
              opacity: 1,
              scale: 100
            },
            second: {
              opacity: 0,
              scale: modifierValue(modifiers, "scale", 95)
            }
          };
          transitionHelper(el, modifiers, () => {
          }, hideCallback, reject, styleValues, TRANSITION_TYPE_OUT);
        }
        function modifierValue(modifiers, key, fallback) {
          if (modifiers.indexOf(key) === -1)
            return fallback;
          const rawValue = modifiers[modifiers.indexOf(key) + 1];
          if (!rawValue)
            return fallback;
          if (key === "scale") {
            if (!isNumeric(rawValue))
              return fallback;
          }
          if (key === "duration") {
            let match = rawValue.match(/([0-9]+)ms/);
            if (match)
              return match[1];
          }
          if (key === "origin") {
            if (["top", "right", "left", "center", "bottom"].includes(modifiers[modifiers.indexOf(key) + 2])) {
              return [rawValue, modifiers[modifiers.indexOf(key) + 2]].join(" ");
            }
          }
          return rawValue;
        }
        function transitionHelper(el, modifiers, hook1, hook2, reject, styleValues, type) {
          if (el.__x_transition) {
            el.__x_transition.cancel && el.__x_transition.cancel();
          }
          const opacityCache = el.style.opacity;
          const transformCache = el.style.transform;
          const transformOriginCache = el.style.transformOrigin;
          const noModifiers = !modifiers.includes("opacity") && !modifiers.includes("scale");
          const transitionOpacity = noModifiers || modifiers.includes("opacity");
          const transitionScale = noModifiers || modifiers.includes("scale");
          const stages = {
            start() {
              if (transitionOpacity)
                el.style.opacity = styleValues.first.opacity;
              if (transitionScale)
                el.style.transform = `scale(${styleValues.first.scale / 100})`;
            },
            during() {
              if (transitionScale)
                el.style.transformOrigin = styleValues.origin;
              el.style.transitionProperty = [transitionOpacity ? `opacity` : ``, transitionScale ? `transform` : ``].join(" ").trim();
              el.style.transitionDuration = `${styleValues.duration / 1e3}s`;
              el.style.transitionTimingFunction = `cubic-bezier(0.4, 0.0, 0.2, 1)`;
            },
            show() {
              hook1();
            },
            end() {
              if (transitionOpacity)
                el.style.opacity = styleValues.second.opacity;
              if (transitionScale)
                el.style.transform = `scale(${styleValues.second.scale / 100})`;
            },
            hide() {
              hook2();
            },
            cleanup() {
              if (transitionOpacity)
                el.style.opacity = opacityCache;
              if (transitionScale)
                el.style.transform = transformCache;
              if (transitionScale)
                el.style.transformOrigin = transformOriginCache;
              el.style.transitionProperty = null;
              el.style.transitionDuration = null;
              el.style.transitionTimingFunction = null;
            }
          };
          transition(el, stages, type, reject);
        }
        const ensureStringExpression = (expression, el, component) => {
          return typeof expression === "function" ? component.evaluateReturnExpression(el, expression) : expression;
        };
        function transitionClassesIn(el, component, directives, showCallback, reject) {
          const enter = convertClassStringToArray(ensureStringExpression((directives.find((i) => i.value === "enter") || {
            expression: ""
          }).expression, el, component));
          const enterStart = convertClassStringToArray(ensureStringExpression((directives.find((i) => i.value === "enter-start") || {
            expression: ""
          }).expression, el, component));
          const enterEnd = convertClassStringToArray(ensureStringExpression((directives.find((i) => i.value === "enter-end") || {
            expression: ""
          }).expression, el, component));
          transitionClasses(el, enter, enterStart, enterEnd, showCallback, () => {
          }, TRANSITION_TYPE_IN, reject);
        }
        function transitionClassesOut(el, component, directives, hideCallback, reject) {
          const leave = convertClassStringToArray(ensureStringExpression((directives.find((i) => i.value === "leave") || {
            expression: ""
          }).expression, el, component));
          const leaveStart = convertClassStringToArray(ensureStringExpression((directives.find((i) => i.value === "leave-start") || {
            expression: ""
          }).expression, el, component));
          const leaveEnd = convertClassStringToArray(ensureStringExpression((directives.find((i) => i.value === "leave-end") || {
            expression: ""
          }).expression, el, component));
          transitionClasses(el, leave, leaveStart, leaveEnd, () => {
          }, hideCallback, TRANSITION_TYPE_OUT, reject);
        }
        function transitionClasses(el, classesDuring, classesStart, classesEnd, hook1, hook2, type, reject) {
          if (el.__x_transition) {
            el.__x_transition.cancel && el.__x_transition.cancel();
          }
          const originalClasses = el.__x_original_classes || [];
          const stages = {
            start() {
              el.classList.add(...classesStart);
            },
            during() {
              el.classList.add(...classesDuring);
            },
            show() {
              hook1();
            },
            end() {
              el.classList.remove(...classesStart.filter((i) => !originalClasses.includes(i)));
              el.classList.add(...classesEnd);
            },
            hide() {
              hook2();
            },
            cleanup() {
              el.classList.remove(...classesDuring.filter((i) => !originalClasses.includes(i)));
              el.classList.remove(...classesEnd.filter((i) => !originalClasses.includes(i)));
            }
          };
          transition(el, stages, type, reject);
        }
        function transition(el, stages, type, reject) {
          const finish = once(() => {
            stages.hide();
            if (el.isConnected) {
              stages.cleanup();
            }
            delete el.__x_transition;
          });
          el.__x_transition = {
            type,
            cancel: once(() => {
              reject(TRANSITION_CANCELLED);
              finish();
            }),
            finish,
            nextFrame: null
          };
          stages.start();
          stages.during();
          el.__x_transition.nextFrame = requestAnimationFrame(() => {
            let duration = Number(getComputedStyle(el).transitionDuration.replace(/,.*/, "").replace("s", "")) * 1e3;
            if (duration === 0) {
              duration = Number(getComputedStyle(el).animationDuration.replace("s", "")) * 1e3;
            }
            stages.show();
            el.__x_transition.nextFrame = requestAnimationFrame(() => {
              stages.end();
              setTimeout(el.__x_transition.finish, duration);
            });
          });
        }
        function isNumeric(subject) {
          return !Array.isArray(subject) && !isNaN(subject);
        }
        function once(callback) {
          let called = false;
          return function() {
            if (!called) {
              called = true;
              callback.apply(this, arguments);
            }
          };
        }
        function handleForDirective(component, templateEl, expression, initialUpdate, extraVars) {
          warnIfMalformedTemplate(templateEl, "x-for");
          let iteratorNames = typeof expression === "function" ? parseForExpression(component.evaluateReturnExpression(templateEl, expression)) : parseForExpression(expression);
          let items = evaluateItemsAndReturnEmptyIfXIfIsPresentAndFalseOnElement(component, templateEl, iteratorNames, extraVars);
          let currentEl = templateEl;
          items.forEach((item, index) => {
            let iterationScopeVariables = getIterationScopeVariables(iteratorNames, item, index, items, extraVars());
            let currentKey = generateKeyForIteration(component, templateEl, index, iterationScopeVariables);
            let nextEl = lookAheadForMatchingKeyedElementAndMoveItIfFound(currentEl.nextElementSibling, currentKey);
            if (!nextEl) {
              nextEl = addElementInLoopAfterCurrentEl(templateEl, currentEl);
              transitionIn(nextEl, () => {
              }, () => {
              }, component, initialUpdate);
              nextEl.__x_for = iterationScopeVariables;
              component.initializeElements(nextEl, () => nextEl.__x_for);
            } else {
              delete nextEl.__x_for_key;
              nextEl.__x_for = iterationScopeVariables;
              component.updateElements(nextEl, () => nextEl.__x_for);
            }
            currentEl = nextEl;
            currentEl.__x_for_key = currentKey;
          });
          removeAnyLeftOverElementsFromPreviousUpdate(currentEl, component);
        }
        function parseForExpression(expression) {
          let forIteratorRE = /,([^,\}\]]*)(?:,([^,\}\]]*))?$/;
          let stripParensRE = /^\(|\)$/g;
          let forAliasRE = /([\s\S]*?)\s+(?:in|of)\s+([\s\S]*)/;
          let inMatch = String(expression).match(forAliasRE);
          if (!inMatch)
            return;
          let res = {};
          res.items = inMatch[2].trim();
          let item = inMatch[1].trim().replace(stripParensRE, "");
          let iteratorMatch = item.match(forIteratorRE);
          if (iteratorMatch) {
            res.item = item.replace(forIteratorRE, "").trim();
            res.index = iteratorMatch[1].trim();
            if (iteratorMatch[2]) {
              res.collection = iteratorMatch[2].trim();
            }
          } else {
            res.item = item;
          }
          return res;
        }
        function getIterationScopeVariables(iteratorNames, item, index, items, extraVars) {
          let scopeVariables = extraVars ? _objectSpread2({}, extraVars) : {};
          scopeVariables[iteratorNames.item] = item;
          if (iteratorNames.index)
            scopeVariables[iteratorNames.index] = index;
          if (iteratorNames.collection)
            scopeVariables[iteratorNames.collection] = items;
          return scopeVariables;
        }
        function generateKeyForIteration(component, el, index, iterationScopeVariables) {
          let bindKeyAttribute = getXAttrs(el, component, "bind").filter((attr) => attr.value === "key")[0];
          if (!bindKeyAttribute)
            return index;
          return component.evaluateReturnExpression(el, bindKeyAttribute.expression, () => iterationScopeVariables);
        }
        function evaluateItemsAndReturnEmptyIfXIfIsPresentAndFalseOnElement(component, el, iteratorNames, extraVars) {
          let ifAttribute = getXAttrs(el, component, "if")[0];
          if (ifAttribute && !component.evaluateReturnExpression(el, ifAttribute.expression)) {
            return [];
          }
          let items = component.evaluateReturnExpression(el, iteratorNames.items, extraVars);
          if (isNumeric(items) && items >= 0) {
            items = Array.from(Array(items).keys(), (i) => i + 1);
          }
          return items;
        }
        function addElementInLoopAfterCurrentEl(templateEl, currentEl) {
          let clone = document.importNode(templateEl.content, true);
          currentEl.parentElement.insertBefore(clone, currentEl.nextElementSibling);
          return currentEl.nextElementSibling;
        }
        function lookAheadForMatchingKeyedElementAndMoveItIfFound(nextEl, currentKey) {
          if (!nextEl)
            return;
          if (nextEl.__x_for_key === void 0)
            return;
          if (nextEl.__x_for_key === currentKey)
            return nextEl;
          let tmpNextEl = nextEl;
          while (tmpNextEl) {
            if (tmpNextEl.__x_for_key === currentKey) {
              return tmpNextEl.parentElement.insertBefore(tmpNextEl, nextEl);
            }
            tmpNextEl = tmpNextEl.nextElementSibling && tmpNextEl.nextElementSibling.__x_for_key !== void 0 ? tmpNextEl.nextElementSibling : false;
          }
        }
        function removeAnyLeftOverElementsFromPreviousUpdate(currentEl, component) {
          var nextElementFromOldLoop = currentEl.nextElementSibling && currentEl.nextElementSibling.__x_for_key !== void 0 ? currentEl.nextElementSibling : false;
          while (nextElementFromOldLoop) {
            let nextElementFromOldLoopImmutable = nextElementFromOldLoop;
            let nextSibling = nextElementFromOldLoop.nextElementSibling;
            transitionOut(nextElementFromOldLoop, () => {
              nextElementFromOldLoopImmutable.remove();
            }, () => {
            }, component);
            nextElementFromOldLoop = nextSibling && nextSibling.__x_for_key !== void 0 ? nextSibling : false;
          }
        }
        function handleAttributeBindingDirective(component, el, attrName, expression, extraVars, attrType, modifiers) {
          var value = component.evaluateReturnExpression(el, expression, extraVars);
          if (attrName === "value") {
            if (Alpine.ignoreFocusedForValueBinding && document.activeElement.isSameNode(el))
              return;
            if (value === void 0 && String(expression).match(/\./)) {
              value = "";
            }
            if (el.type === "radio") {
              if (el.attributes.value === void 0 && attrType === "bind") {
                el.value = value;
              } else if (attrType !== "bind") {
                el.checked = checkedAttrLooseCompare(el.value, value);
              }
            } else if (el.type === "checkbox") {
              if (typeof value !== "boolean" && ![null, void 0].includes(value) && attrType === "bind") {
                el.value = String(value);
              } else if (attrType !== "bind") {
                if (Array.isArray(value)) {
                  el.checked = value.some((val) => checkedAttrLooseCompare(val, el.value));
                } else {
                  el.checked = !!value;
                }
              }
            } else if (el.tagName === "SELECT") {
              updateSelect(el, value);
            } else {
              if (el.value === value)
                return;
              el.value = value;
            }
          } else if (attrName === "class") {
            if (Array.isArray(value)) {
              const originalClasses = el.__x_original_classes || [];
              el.setAttribute("class", arrayUnique(originalClasses.concat(value)).join(" "));
            } else if (typeof value === "object") {
              const keysSortedByBooleanValue = Object.keys(value).sort((a, b) => value[a] - value[b]);
              keysSortedByBooleanValue.forEach((classNames) => {
                if (value[classNames]) {
                  convertClassStringToArray(classNames).forEach((className) => el.classList.add(className));
                } else {
                  convertClassStringToArray(classNames).forEach((className) => el.classList.remove(className));
                }
              });
            } else {
              const originalClasses = el.__x_original_classes || [];
              const newClasses = value ? convertClassStringToArray(value) : [];
              el.setAttribute("class", arrayUnique(originalClasses.concat(newClasses)).join(" "));
            }
          } else {
            attrName = modifiers.includes("camel") ? camelCase(attrName) : attrName;
            if ([null, void 0, false].includes(value)) {
              el.removeAttribute(attrName);
            } else {
              isBooleanAttr(attrName) ? setIfChanged(el, attrName, attrName) : setIfChanged(el, attrName, value);
            }
          }
        }
        function setIfChanged(el, attrName, value) {
          if (el.getAttribute(attrName) != value) {
            el.setAttribute(attrName, value);
          }
        }
        function updateSelect(el, value) {
          const arrayWrappedValue = [].concat(value).map((value2) => {
            return value2 + "";
          });
          Array.from(el.options).forEach((option) => {
            option.selected = arrayWrappedValue.includes(option.value || option.text);
          });
        }
        function handleTextDirective(el, output, expression) {
          if (output === void 0 && String(expression).match(/\./)) {
            output = "";
          }
          el.textContent = output;
        }
        function handleHtmlDirective(component, el, expression, extraVars) {
          el.innerHTML = component.evaluateReturnExpression(el, expression, extraVars);
        }
        function handleShowDirective(component, el, value, modifiers, initialUpdate = false) {
          const hide = () => {
            el.style.display = "none";
            el.__x_is_shown = false;
          };
          const show = () => {
            if (el.style.length === 1 && el.style.display === "none") {
              el.removeAttribute("style");
            } else {
              el.style.removeProperty("display");
            }
            el.__x_is_shown = true;
          };
          if (initialUpdate === true) {
            if (value) {
              show();
            } else {
              hide();
            }
            return;
          }
          const handle = (resolve, reject) => {
            if (value) {
              if (el.style.display === "none" || el.__x_transition) {
                transitionIn(el, () => {
                  show();
                }, reject, component);
              }
              resolve(() => {
              });
            } else {
              if (el.style.display !== "none") {
                transitionOut(el, () => {
                  resolve(() => {
                    hide();
                  });
                }, reject, component);
              } else {
                resolve(() => {
                });
              }
            }
          };
          if (modifiers.includes("immediate")) {
            handle((finish) => finish(), () => {
            });
            return;
          }
          if (component.showDirectiveLastElement && !component.showDirectiveLastElement.contains(el)) {
            component.executeAndClearRemainingShowDirectiveStack();
          }
          component.showDirectiveStack.push(handle);
          component.showDirectiveLastElement = el;
        }
        function handleIfDirective(component, el, expressionResult, initialUpdate, extraVars) {
          warnIfMalformedTemplate(el, "x-if");
          const elementHasAlreadyBeenAdded = el.nextElementSibling && el.nextElementSibling.__x_inserted_me === true;
          if (expressionResult && (!elementHasAlreadyBeenAdded || el.__x_transition)) {
            const clone = document.importNode(el.content, true);
            el.parentElement.insertBefore(clone, el.nextElementSibling);
            transitionIn(el.nextElementSibling, () => {
            }, () => {
            }, component, initialUpdate);
            component.initializeElements(el.nextElementSibling, extraVars);
            el.nextElementSibling.__x_inserted_me = true;
          } else if (!expressionResult && elementHasAlreadyBeenAdded) {
            transitionOut(el.nextElementSibling, () => {
              el.nextElementSibling.remove();
            }, () => {
            }, component, initialUpdate);
          }
        }
        function registerListener(component, el, event, modifiers, expression, extraVars = {}) {
          const options = {
            passive: modifiers.includes("passive")
          };
          if (modifiers.includes("camel")) {
            event = camelCase(event);
          }
          let handler, listenerTarget;
          if (modifiers.includes("away")) {
            listenerTarget = document;
            handler = (e) => {
              if (el.contains(e.target))
                return;
              if (el.offsetWidth < 1 && el.offsetHeight < 1)
                return;
              runListenerHandler(component, expression, e, extraVars);
              if (modifiers.includes("once")) {
                document.removeEventListener(event, handler, options);
              }
            };
          } else {
            listenerTarget = modifiers.includes("window") ? window : modifiers.includes("document") ? document : el;
            handler = (e) => {
              if (listenerTarget === window || listenerTarget === document) {
                if (!document.body.contains(el)) {
                  listenerTarget.removeEventListener(event, handler, options);
                  return;
                }
              }
              if (isKeyEvent(event)) {
                if (isListeningForASpecificKeyThatHasntBeenPressed(e, modifiers)) {
                  return;
                }
              }
              if (modifiers.includes("prevent"))
                e.preventDefault();
              if (modifiers.includes("stop"))
                e.stopPropagation();
              if (!modifiers.includes("self") || e.target === el) {
                const returnValue = runListenerHandler(component, expression, e, extraVars);
                returnValue.then((value) => {
                  if (value === false) {
                    e.preventDefault();
                  } else {
                    if (modifiers.includes("once")) {
                      listenerTarget.removeEventListener(event, handler, options);
                    }
                  }
                });
              }
            };
          }
          if (modifiers.includes("debounce")) {
            let nextModifier = modifiers[modifiers.indexOf("debounce") + 1] || "invalid-wait";
            let wait = isNumeric(nextModifier.split("ms")[0]) ? Number(nextModifier.split("ms")[0]) : 250;
            handler = debounce(handler, wait);
          }
          listenerTarget.addEventListener(event, handler, options);
        }
        function runListenerHandler(component, expression, e, extraVars) {
          return component.evaluateCommandExpression(e.target, expression, () => {
            return _objectSpread2(_objectSpread2({}, extraVars()), {}, {
              "$event": e
            });
          });
        }
        function isKeyEvent(event) {
          return ["keydown", "keyup"].includes(event);
        }
        function isListeningForASpecificKeyThatHasntBeenPressed(e, modifiers) {
          let keyModifiers = modifiers.filter((i) => {
            return !["window", "document", "prevent", "stop"].includes(i);
          });
          if (keyModifiers.includes("debounce")) {
            let debounceIndex = keyModifiers.indexOf("debounce");
            keyModifiers.splice(debounceIndex, isNumeric((keyModifiers[debounceIndex + 1] || "invalid-wait").split("ms")[0]) ? 2 : 1);
          }
          if (keyModifiers.length === 0)
            return false;
          if (keyModifiers.length === 1 && keyModifiers[0] === keyToModifier(e.key))
            return false;
          const systemKeyModifiers = ["ctrl", "shift", "alt", "meta", "cmd", "super"];
          const selectedSystemKeyModifiers = systemKeyModifiers.filter((modifier) => keyModifiers.includes(modifier));
          keyModifiers = keyModifiers.filter((i) => !selectedSystemKeyModifiers.includes(i));
          if (selectedSystemKeyModifiers.length > 0) {
            const activelyPressedKeyModifiers = selectedSystemKeyModifiers.filter((modifier) => {
              if (modifier === "cmd" || modifier === "super")
                modifier = "meta";
              return e[`${modifier}Key`];
            });
            if (activelyPressedKeyModifiers.length === selectedSystemKeyModifiers.length) {
              if (keyModifiers[0] === keyToModifier(e.key))
                return false;
            }
          }
          return true;
        }
        function keyToModifier(key) {
          switch (key) {
            case "/":
              return "slash";
            case " ":
            case "Spacebar":
              return "space";
            default:
              return key && kebabCase(key);
          }
        }
        function registerModelListener(component, el, modifiers, expression, extraVars) {
          var event = el.tagName.toLowerCase() === "select" || ["checkbox", "radio"].includes(el.type) || modifiers.includes("lazy") ? "change" : "input";
          const listenerExpression = `${expression} = rightSideOfExpression($event, ${expression})`;
          registerListener(component, el, event, modifiers, listenerExpression, () => {
            return _objectSpread2(_objectSpread2({}, extraVars()), {}, {
              rightSideOfExpression: generateModelAssignmentFunction(el, modifiers, expression)
            });
          });
        }
        function generateModelAssignmentFunction(el, modifiers, expression) {
          if (el.type === "radio") {
            if (!el.hasAttribute("name"))
              el.setAttribute("name", expression);
          }
          return (event, currentValue) => {
            if (event instanceof CustomEvent && event.detail) {
              return event.detail;
            } else if (el.type === "checkbox") {
              if (Array.isArray(currentValue)) {
                const newValue = modifiers.includes("number") ? safeParseNumber(event.target.value) : event.target.value;
                return event.target.checked ? currentValue.concat([newValue]) : currentValue.filter((el2) => !checkedAttrLooseCompare(el2, newValue));
              } else {
                return event.target.checked;
              }
            } else if (el.tagName.toLowerCase() === "select" && el.multiple) {
              return modifiers.includes("number") ? Array.from(event.target.selectedOptions).map((option) => {
                const rawValue = option.value || option.text;
                return safeParseNumber(rawValue);
              }) : Array.from(event.target.selectedOptions).map((option) => {
                return option.value || option.text;
              });
            } else {
              const rawValue = event.target.value;
              return modifiers.includes("number") ? safeParseNumber(rawValue) : modifiers.includes("trim") ? rawValue.trim() : rawValue;
            }
          };
        }
        function safeParseNumber(rawValue) {
          const number = rawValue ? parseFloat(rawValue) : null;
          return isNumeric(number) ? number : rawValue;
        }
        const { isArray } = Array;
        const { getPrototypeOf, create: ObjectCreate, defineProperty: ObjectDefineProperty, defineProperties: ObjectDefineProperties, isExtensible, getOwnPropertyDescriptor, getOwnPropertyNames, getOwnPropertySymbols, preventExtensions, hasOwnProperty } = Object;
        const { push: ArrayPush, concat: ArrayConcat, map: ArrayMap } = Array.prototype;
        function isUndefined(obj) {
          return obj === void 0;
        }
        function isFunction(obj) {
          return typeof obj === "function";
        }
        function isObject(obj) {
          return typeof obj === "object";
        }
        const proxyToValueMap = new WeakMap();
        function registerProxy(proxy, value) {
          proxyToValueMap.set(proxy, value);
        }
        const unwrap = (replicaOrAny) => proxyToValueMap.get(replicaOrAny) || replicaOrAny;
        function wrapValue(membrane, value) {
          return membrane.valueIsObservable(value) ? membrane.getProxy(value) : value;
        }
        function unwrapDescriptor(descriptor) {
          if (hasOwnProperty.call(descriptor, "value")) {
            descriptor.value = unwrap(descriptor.value);
          }
          return descriptor;
        }
        function lockShadowTarget(membrane, shadowTarget, originalTarget) {
          const targetKeys = ArrayConcat.call(getOwnPropertyNames(originalTarget), getOwnPropertySymbols(originalTarget));
          targetKeys.forEach((key) => {
            let descriptor = getOwnPropertyDescriptor(originalTarget, key);
            if (!descriptor.configurable) {
              descriptor = wrapDescriptor(membrane, descriptor, wrapValue);
            }
            ObjectDefineProperty(shadowTarget, key, descriptor);
          });
          preventExtensions(shadowTarget);
        }
        class ReactiveProxyHandler {
          constructor(membrane, value) {
            this.originalTarget = value;
            this.membrane = membrane;
          }
          get(shadowTarget, key) {
            const { originalTarget, membrane } = this;
            const value = originalTarget[key];
            const { valueObserved } = membrane;
            valueObserved(originalTarget, key);
            return membrane.getProxy(value);
          }
          set(shadowTarget, key, value) {
            const { originalTarget, membrane: { valueMutated } } = this;
            const oldValue = originalTarget[key];
            if (oldValue !== value) {
              originalTarget[key] = value;
              valueMutated(originalTarget, key);
            } else if (key === "length" && isArray(originalTarget)) {
              valueMutated(originalTarget, key);
            }
            return true;
          }
          deleteProperty(shadowTarget, key) {
            const { originalTarget, membrane: { valueMutated } } = this;
            delete originalTarget[key];
            valueMutated(originalTarget, key);
            return true;
          }
          apply(shadowTarget, thisArg, argArray) {
          }
          construct(target, argArray, newTarget) {
          }
          has(shadowTarget, key) {
            const { originalTarget, membrane: { valueObserved } } = this;
            valueObserved(originalTarget, key);
            return key in originalTarget;
          }
          ownKeys(shadowTarget) {
            const { originalTarget } = this;
            return ArrayConcat.call(getOwnPropertyNames(originalTarget), getOwnPropertySymbols(originalTarget));
          }
          isExtensible(shadowTarget) {
            const shadowIsExtensible = isExtensible(shadowTarget);
            if (!shadowIsExtensible) {
              return shadowIsExtensible;
            }
            const { originalTarget, membrane } = this;
            const targetIsExtensible = isExtensible(originalTarget);
            if (!targetIsExtensible) {
              lockShadowTarget(membrane, shadowTarget, originalTarget);
            }
            return targetIsExtensible;
          }
          setPrototypeOf(shadowTarget, prototype) {
          }
          getPrototypeOf(shadowTarget) {
            const { originalTarget } = this;
            return getPrototypeOf(originalTarget);
          }
          getOwnPropertyDescriptor(shadowTarget, key) {
            const { originalTarget, membrane } = this;
            const { valueObserved } = this.membrane;
            valueObserved(originalTarget, key);
            let desc = getOwnPropertyDescriptor(originalTarget, key);
            if (isUndefined(desc)) {
              return desc;
            }
            const shadowDescriptor = getOwnPropertyDescriptor(shadowTarget, key);
            if (!isUndefined(shadowDescriptor)) {
              return shadowDescriptor;
            }
            desc = wrapDescriptor(membrane, desc, wrapValue);
            if (!desc.configurable) {
              ObjectDefineProperty(shadowTarget, key, desc);
            }
            return desc;
          }
          preventExtensions(shadowTarget) {
            const { originalTarget, membrane } = this;
            lockShadowTarget(membrane, shadowTarget, originalTarget);
            preventExtensions(originalTarget);
            return true;
          }
          defineProperty(shadowTarget, key, descriptor) {
            const { originalTarget, membrane } = this;
            const { valueMutated } = membrane;
            const { configurable } = descriptor;
            if (hasOwnProperty.call(descriptor, "writable") && !hasOwnProperty.call(descriptor, "value")) {
              const originalDescriptor = getOwnPropertyDescriptor(originalTarget, key);
              descriptor.value = originalDescriptor.value;
            }
            ObjectDefineProperty(originalTarget, key, unwrapDescriptor(descriptor));
            if (configurable === false) {
              ObjectDefineProperty(shadowTarget, key, wrapDescriptor(membrane, descriptor, wrapValue));
            }
            valueMutated(originalTarget, key);
            return true;
          }
        }
        function wrapReadOnlyValue(membrane, value) {
          return membrane.valueIsObservable(value) ? membrane.getReadOnlyProxy(value) : value;
        }
        class ReadOnlyHandler {
          constructor(membrane, value) {
            this.originalTarget = value;
            this.membrane = membrane;
          }
          get(shadowTarget, key) {
            const { membrane, originalTarget } = this;
            const value = originalTarget[key];
            const { valueObserved } = membrane;
            valueObserved(originalTarget, key);
            return membrane.getReadOnlyProxy(value);
          }
          set(shadowTarget, key, value) {
            return false;
          }
          deleteProperty(shadowTarget, key) {
            return false;
          }
          apply(shadowTarget, thisArg, argArray) {
          }
          construct(target, argArray, newTarget) {
          }
          has(shadowTarget, key) {
            const { originalTarget, membrane: { valueObserved } } = this;
            valueObserved(originalTarget, key);
            return key in originalTarget;
          }
          ownKeys(shadowTarget) {
            const { originalTarget } = this;
            return ArrayConcat.call(getOwnPropertyNames(originalTarget), getOwnPropertySymbols(originalTarget));
          }
          setPrototypeOf(shadowTarget, prototype) {
          }
          getOwnPropertyDescriptor(shadowTarget, key) {
            const { originalTarget, membrane } = this;
            const { valueObserved } = membrane;
            valueObserved(originalTarget, key);
            let desc = getOwnPropertyDescriptor(originalTarget, key);
            if (isUndefined(desc)) {
              return desc;
            }
            const shadowDescriptor = getOwnPropertyDescriptor(shadowTarget, key);
            if (!isUndefined(shadowDescriptor)) {
              return shadowDescriptor;
            }
            desc = wrapDescriptor(membrane, desc, wrapReadOnlyValue);
            if (hasOwnProperty.call(desc, "set")) {
              desc.set = void 0;
            }
            if (!desc.configurable) {
              ObjectDefineProperty(shadowTarget, key, desc);
            }
            return desc;
          }
          preventExtensions(shadowTarget) {
            return false;
          }
          defineProperty(shadowTarget, key, descriptor) {
            return false;
          }
        }
        function createShadowTarget(value) {
          let shadowTarget = void 0;
          if (isArray(value)) {
            shadowTarget = [];
          } else if (isObject(value)) {
            shadowTarget = {};
          }
          return shadowTarget;
        }
        const ObjectDotPrototype = Object.prototype;
        function defaultValueIsObservable(value) {
          if (value === null) {
            return false;
          }
          if (typeof value !== "object") {
            return false;
          }
          if (isArray(value)) {
            return true;
          }
          const proto = getPrototypeOf(value);
          return proto === ObjectDotPrototype || proto === null || getPrototypeOf(proto) === null;
        }
        const defaultValueObserved = (obj, key) => {
        };
        const defaultValueMutated = (obj, key) => {
        };
        const defaultValueDistortion = (value) => value;
        function wrapDescriptor(membrane, descriptor, getValue) {
          const { set, get } = descriptor;
          if (hasOwnProperty.call(descriptor, "value")) {
            descriptor.value = getValue(membrane, descriptor.value);
          } else {
            if (!isUndefined(get)) {
              descriptor.get = function() {
                return getValue(membrane, get.call(unwrap(this)));
              };
            }
            if (!isUndefined(set)) {
              descriptor.set = function(value) {
                set.call(unwrap(this), membrane.unwrapProxy(value));
              };
            }
          }
          return descriptor;
        }
        class ReactiveMembrane {
          constructor(options) {
            this.valueDistortion = defaultValueDistortion;
            this.valueMutated = defaultValueMutated;
            this.valueObserved = defaultValueObserved;
            this.valueIsObservable = defaultValueIsObservable;
            this.objectGraph = new WeakMap();
            if (!isUndefined(options)) {
              const { valueDistortion, valueMutated, valueObserved, valueIsObservable } = options;
              this.valueDistortion = isFunction(valueDistortion) ? valueDistortion : defaultValueDistortion;
              this.valueMutated = isFunction(valueMutated) ? valueMutated : defaultValueMutated;
              this.valueObserved = isFunction(valueObserved) ? valueObserved : defaultValueObserved;
              this.valueIsObservable = isFunction(valueIsObservable) ? valueIsObservable : defaultValueIsObservable;
            }
          }
          getProxy(value) {
            const unwrappedValue = unwrap(value);
            const distorted = this.valueDistortion(unwrappedValue);
            if (this.valueIsObservable(distorted)) {
              const o = this.getReactiveState(unwrappedValue, distorted);
              return o.readOnly === value ? value : o.reactive;
            }
            return distorted;
          }
          getReadOnlyProxy(value) {
            value = unwrap(value);
            const distorted = this.valueDistortion(value);
            if (this.valueIsObservable(distorted)) {
              return this.getReactiveState(value, distorted).readOnly;
            }
            return distorted;
          }
          unwrapProxy(p) {
            return unwrap(p);
          }
          getReactiveState(value, distortedValue) {
            const { objectGraph } = this;
            let reactiveState = objectGraph.get(distortedValue);
            if (reactiveState) {
              return reactiveState;
            }
            const membrane = this;
            reactiveState = {
              get reactive() {
                const reactiveHandler = new ReactiveProxyHandler(membrane, distortedValue);
                const proxy = new Proxy(createShadowTarget(distortedValue), reactiveHandler);
                registerProxy(proxy, value);
                ObjectDefineProperty(this, "reactive", { value: proxy });
                return proxy;
              },
              get readOnly() {
                const readOnlyHandler = new ReadOnlyHandler(membrane, distortedValue);
                const proxy = new Proxy(createShadowTarget(distortedValue), readOnlyHandler);
                registerProxy(proxy, value);
                ObjectDefineProperty(this, "readOnly", { value: proxy });
                return proxy;
              }
            };
            objectGraph.set(distortedValue, reactiveState);
            return reactiveState;
          }
        }
        function wrap(data, mutationCallback) {
          let membrane = new ReactiveMembrane({
            valueMutated(target, key) {
              mutationCallback(target, key);
            }
          });
          return {
            data: membrane.getProxy(data),
            membrane
          };
        }
        function unwrap$1(membrane, observable) {
          let unwrappedData = membrane.unwrapProxy(observable);
          let copy = {};
          Object.keys(unwrappedData).forEach((key) => {
            if (["$el", "$refs", "$nextTick", "$watch"].includes(key))
              return;
            copy[key] = unwrappedData[key];
          });
          return copy;
        }
        class Component {
          constructor(el, componentForClone = null) {
            this.$el = el;
            const dataAttr = this.$el.getAttribute("x-data");
            const dataExpression = dataAttr === "" ? "{}" : dataAttr;
            const initExpression = this.$el.getAttribute("x-init");
            let dataExtras = {
              $el: this.$el
            };
            let canonicalComponentElementReference = componentForClone ? componentForClone.$el : this.$el;
            Object.entries(Alpine.magicProperties).forEach(([name, callback]) => {
              Object.defineProperty(dataExtras, `$${name}`, {
                get: function get() {
                  return callback(canonicalComponentElementReference);
                }
              });
            });
            this.unobservedData = componentForClone ? componentForClone.getUnobservedData() : saferEval(el, dataExpression, dataExtras);
            let {
              membrane,
              data
            } = this.wrapDataInObservable(this.unobservedData);
            this.$data = data;
            this.membrane = membrane;
            this.unobservedData.$el = this.$el;
            this.unobservedData.$refs = this.getRefsProxy();
            this.nextTickStack = [];
            this.unobservedData.$nextTick = (callback) => {
              this.nextTickStack.push(callback);
            };
            this.watchers = {};
            this.unobservedData.$watch = (property, callback) => {
              if (!this.watchers[property])
                this.watchers[property] = [];
              this.watchers[property].push(callback);
            };
            Object.entries(Alpine.magicProperties).forEach(([name, callback]) => {
              Object.defineProperty(this.unobservedData, `$${name}`, {
                get: function get() {
                  return callback(canonicalComponentElementReference, this.$el);
                }
              });
            });
            this.showDirectiveStack = [];
            this.showDirectiveLastElement;
            componentForClone || Alpine.onBeforeComponentInitializeds.forEach((callback) => callback(this));
            var initReturnedCallback;
            if (initExpression && !componentForClone) {
              this.pauseReactivity = true;
              initReturnedCallback = this.evaluateReturnExpression(this.$el, initExpression);
              this.pauseReactivity = false;
            }
            this.initializeElements(this.$el, () => {
            }, componentForClone);
            this.listenForNewElementsToInitialize();
            if (typeof initReturnedCallback === "function") {
              initReturnedCallback.call(this.$data);
            }
            componentForClone || setTimeout(() => {
              Alpine.onComponentInitializeds.forEach((callback) => callback(this));
            }, 0);
          }
          getUnobservedData() {
            return unwrap$1(this.membrane, this.$data);
          }
          wrapDataInObservable(data) {
            var self2 = this;
            let updateDom = debounce(function() {
              self2.updateElements(self2.$el);
            }, 0);
            return wrap(data, (target, key) => {
              if (self2.watchers[key]) {
                self2.watchers[key].forEach((callback) => callback(target[key]));
              } else if (Array.isArray(target)) {
                Object.keys(self2.watchers).forEach((fullDotNotationKey) => {
                  let dotNotationParts = fullDotNotationKey.split(".");
                  if (key === "length")
                    return;
                  dotNotationParts.reduce((comparisonData, part) => {
                    if (Object.is(target, comparisonData[part])) {
                      self2.watchers[fullDotNotationKey].forEach((callback) => callback(target));
                    }
                    return comparisonData[part];
                  }, self2.unobservedData);
                });
              } else {
                Object.keys(self2.watchers).filter((i) => i.includes(".")).forEach((fullDotNotationKey) => {
                  let dotNotationParts = fullDotNotationKey.split(".");
                  if (key !== dotNotationParts[dotNotationParts.length - 1])
                    return;
                  dotNotationParts.reduce((comparisonData, part) => {
                    if (Object.is(target, comparisonData)) {
                      self2.watchers[fullDotNotationKey].forEach((callback) => callback(target[key]));
                    }
                    return comparisonData[part];
                  }, self2.unobservedData);
                });
              }
              if (self2.pauseReactivity)
                return;
              updateDom();
            });
          }
          walkAndSkipNestedComponents(el, callback, initializeComponentCallback = () => {
          }) {
            walk(el, (el2) => {
              if (el2.hasAttribute("x-data")) {
                if (!el2.isSameNode(this.$el)) {
                  if (!el2.__x)
                    initializeComponentCallback(el2);
                  return false;
                }
              }
              return callback(el2);
            });
          }
          initializeElements(rootEl, extraVars = () => {
          }, componentForClone = false) {
            this.walkAndSkipNestedComponents(rootEl, (el) => {
              if (el.__x_for_key !== void 0)
                return false;
              if (el.__x_inserted_me !== void 0)
                return false;
              this.initializeElement(el, extraVars, componentForClone ? false : true);
            }, (el) => {
              if (!componentForClone)
                el.__x = new Component(el);
            });
            this.executeAndClearRemainingShowDirectiveStack();
            this.executeAndClearNextTickStack(rootEl);
          }
          initializeElement(el, extraVars, shouldRegisterListeners = true) {
            if (el.hasAttribute("class") && getXAttrs(el, this).length > 0) {
              el.__x_original_classes = convertClassStringToArray(el.getAttribute("class"));
            }
            shouldRegisterListeners && this.registerListeners(el, extraVars);
            this.resolveBoundAttributes(el, true, extraVars);
          }
          updateElements(rootEl, extraVars = () => {
          }) {
            this.walkAndSkipNestedComponents(rootEl, (el) => {
              if (el.__x_for_key !== void 0 && !el.isSameNode(this.$el))
                return false;
              this.updateElement(el, extraVars);
            }, (el) => {
              el.__x = new Component(el);
            });
            this.executeAndClearRemainingShowDirectiveStack();
            this.executeAndClearNextTickStack(rootEl);
          }
          executeAndClearNextTickStack(el) {
            if (el === this.$el && this.nextTickStack.length > 0) {
              requestAnimationFrame(() => {
                while (this.nextTickStack.length > 0) {
                  this.nextTickStack.shift()();
                }
              });
            }
          }
          executeAndClearRemainingShowDirectiveStack() {
            this.showDirectiveStack.reverse().map((handler) => {
              return new Promise((resolve, reject) => {
                handler(resolve, reject);
              });
            }).reduce((promiseChain, promise) => {
              return promiseChain.then(() => {
                return promise.then((finishElement) => {
                  finishElement();
                });
              });
            }, Promise.resolve(() => {
            })).catch((e) => {
              if (e !== TRANSITION_CANCELLED)
                throw e;
            });
            this.showDirectiveStack = [];
            this.showDirectiveLastElement = void 0;
          }
          updateElement(el, extraVars) {
            this.resolveBoundAttributes(el, false, extraVars);
          }
          registerListeners(el, extraVars) {
            getXAttrs(el, this).forEach(({
              type,
              value,
              modifiers,
              expression
            }) => {
              switch (type) {
                case "on":
                  registerListener(this, el, value, modifiers, expression, extraVars);
                  break;
                case "model":
                  registerModelListener(this, el, modifiers, expression, extraVars);
                  break;
              }
            });
          }
          resolveBoundAttributes(el, initialUpdate = false, extraVars) {
            let attrs = getXAttrs(el, this);
            attrs.forEach(({
              type,
              value,
              modifiers,
              expression
            }) => {
              switch (type) {
                case "model":
                  handleAttributeBindingDirective(this, el, "value", expression, extraVars, type, modifiers);
                  break;
                case "bind":
                  if (el.tagName.toLowerCase() === "template" && value === "key")
                    return;
                  handleAttributeBindingDirective(this, el, value, expression, extraVars, type, modifiers);
                  break;
                case "text":
                  var output = this.evaluateReturnExpression(el, expression, extraVars);
                  handleTextDirective(el, output, expression);
                  break;
                case "html":
                  handleHtmlDirective(this, el, expression, extraVars);
                  break;
                case "show":
                  var output = this.evaluateReturnExpression(el, expression, extraVars);
                  handleShowDirective(this, el, output, modifiers, initialUpdate);
                  break;
                case "if":
                  if (attrs.some((i) => i.type === "for"))
                    return;
                  var output = this.evaluateReturnExpression(el, expression, extraVars);
                  handleIfDirective(this, el, output, initialUpdate, extraVars);
                  break;
                case "for":
                  handleForDirective(this, el, expression, initialUpdate, extraVars);
                  break;
                case "cloak":
                  el.removeAttribute("x-cloak");
                  break;
              }
            });
          }
          evaluateReturnExpression(el, expression, extraVars = () => {
          }) {
            return saferEval(el, expression, this.$data, _objectSpread2(_objectSpread2({}, extraVars()), {}, {
              $dispatch: this.getDispatchFunction(el)
            }));
          }
          evaluateCommandExpression(el, expression, extraVars = () => {
          }) {
            return saferEvalNoReturn(el, expression, this.$data, _objectSpread2(_objectSpread2({}, extraVars()), {}, {
              $dispatch: this.getDispatchFunction(el)
            }));
          }
          getDispatchFunction(el) {
            return (event, detail = {}) => {
              el.dispatchEvent(new CustomEvent(event, {
                detail,
                bubbles: true
              }));
            };
          }
          listenForNewElementsToInitialize() {
            const targetNode = this.$el;
            const observerOptions = {
              childList: true,
              attributes: true,
              subtree: true
            };
            const observer = new MutationObserver((mutations) => {
              for (let i = 0; i < mutations.length; i++) {
                const closestParentComponent = mutations[i].target.closest("[x-data]");
                if (!(closestParentComponent && closestParentComponent.isSameNode(this.$el)))
                  continue;
                if (mutations[i].type === "attributes" && mutations[i].attributeName === "x-data") {
                  const xAttr = mutations[i].target.getAttribute("x-data") || "{}";
                  const rawData = saferEval(this.$el, xAttr, {
                    $el: this.$el
                  });
                  Object.keys(rawData).forEach((key) => {
                    if (this.$data[key] !== rawData[key]) {
                      this.$data[key] = rawData[key];
                    }
                  });
                }
                if (mutations[i].addedNodes.length > 0) {
                  mutations[i].addedNodes.forEach((node) => {
                    if (node.nodeType !== 1 || node.__x_inserted_me)
                      return;
                    if (node.matches("[x-data]") && !node.__x) {
                      node.__x = new Component(node);
                      return;
                    }
                    this.initializeElements(node);
                  });
                }
              }
            });
            observer.observe(targetNode, observerOptions);
          }
          getRefsProxy() {
            var self2 = this;
            var refObj = {};
            return new Proxy(refObj, {
              get(object, property) {
                if (property === "$isAlpineProxy")
                  return true;
                var ref;
                self2.walkAndSkipNestedComponents(self2.$el, (el) => {
                  if (el.hasAttribute("x-ref") && el.getAttribute("x-ref") === property) {
                    ref = el;
                  }
                });
                return ref;
              }
            });
          }
        }
        const Alpine = {
          version: "2.8.2",
          pauseMutationObserver: false,
          magicProperties: {},
          onComponentInitializeds: [],
          onBeforeComponentInitializeds: [],
          ignoreFocusedForValueBinding: false,
          start: function start() {
            return __async(this, null, function* () {
              if (!isTesting()) {
                yield domReady();
              }
              this.discoverComponents((el) => {
                this.initializeComponent(el);
              });
              document.addEventListener("turbolinks:load", () => {
                this.discoverUninitializedComponents((el) => {
                  this.initializeComponent(el);
                });
              });
              this.listenForNewUninitializedComponentsAtRunTime();
            });
          },
          discoverComponents: function discoverComponents(callback) {
            const rootEls = document.querySelectorAll("[x-data]");
            rootEls.forEach((rootEl) => {
              callback(rootEl);
            });
          },
          discoverUninitializedComponents: function discoverUninitializedComponents(callback, el = null) {
            const rootEls = (el || document).querySelectorAll("[x-data]");
            Array.from(rootEls).filter((el2) => el2.__x === void 0).forEach((rootEl) => {
              callback(rootEl);
            });
          },
          listenForNewUninitializedComponentsAtRunTime: function listenForNewUninitializedComponentsAtRunTime() {
            const targetNode = document.querySelector("body");
            const observerOptions = {
              childList: true,
              attributes: true,
              subtree: true
            };
            const observer = new MutationObserver((mutations) => {
              if (this.pauseMutationObserver)
                return;
              for (let i = 0; i < mutations.length; i++) {
                if (mutations[i].addedNodes.length > 0) {
                  mutations[i].addedNodes.forEach((node) => {
                    if (node.nodeType !== 1)
                      return;
                    if (node.parentElement && node.parentElement.closest("[x-data]"))
                      return;
                    this.discoverUninitializedComponents((el) => {
                      this.initializeComponent(el);
                    }, node.parentElement);
                  });
                }
              }
            });
            observer.observe(targetNode, observerOptions);
          },
          initializeComponent: function initializeComponent(el) {
            if (!el.__x) {
              try {
                el.__x = new Component(el);
              } catch (error) {
                setTimeout(() => {
                  throw error;
                }, 0);
              }
            }
          },
          clone: function clone(component, newEl) {
            if (!newEl.__x) {
              newEl.__x = new Component(newEl, component);
            }
          },
          addMagicProperty: function addMagicProperty(name, callback) {
            this.magicProperties[name] = callback;
          },
          onComponentInitialized: function onComponentInitialized(callback) {
            this.onComponentInitializeds.push(callback);
          },
          onBeforeComponentInitialized: function onBeforeComponentInitialized(callback) {
            this.onBeforeComponentInitializeds.push(callback);
          }
        };
        if (!isTesting()) {
          window.Alpine = Alpine;
          if (window.deferLoadingAlpine) {
            window.deferLoadingAlpine(function() {
              window.Alpine.start();
            });
          } else {
            window.Alpine.start();
          }
        }
        return Alpine;
      });
    }
  });

  // src/main.ts
  var import_alpinejs = __toModule(require_alpine());
  var openFullscreen = function() {
    const elem = document.documentElement;
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    }
    if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    }
    if (elem.msRequestFullscreen) {
      elem.msRequestFullscreen();
    }
  };
  var closeFullscreen = function() {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    }
    if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
    if (document.msExitFullscreen) {
      document.msExitFullscreen();
    }
  };
  window.openFullscreen = openFullscreen;
  window.closeFullscreen = closeFullscreen;
})();
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsiLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2FscGluZWpzL2Rpc3QvYWxwaW5lLmpzIiwgIi4uLy4uLy4uL3NyYy9tYWluLnRzIl0sCiAgInNvdXJjZXNDb250ZW50IjogWyIoZnVuY3Rpb24gKGdsb2JhbCwgZmFjdG9yeSkge1xuICB0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSAhPT0gJ3VuZGVmaW5lZCcgPyBtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKSA6XG4gIHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCA/IGRlZmluZShmYWN0b3J5KSA6XG4gIChnbG9iYWwgPSBnbG9iYWwgfHwgc2VsZiwgZ2xvYmFsLkFscGluZSA9IGZhY3RvcnkoKSk7XG59KHRoaXMsIChmdW5jdGlvbiAoKSB7ICd1c2Ugc3RyaWN0JztcblxuICBmdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7XG4gICAgaWYgKGtleSBpbiBvYmopIHtcbiAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwge1xuICAgICAgICB2YWx1ZTogdmFsdWUsXG4gICAgICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZSxcbiAgICAgICAgd3JpdGFibGU6IHRydWVcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBvYmpba2V5XSA9IHZhbHVlO1xuICAgIH1cblxuICAgIHJldHVybiBvYmo7XG4gIH1cblxuICBmdW5jdGlvbiBvd25LZXlzKG9iamVjdCwgZW51bWVyYWJsZU9ubHkpIHtcbiAgICB2YXIga2V5cyA9IE9iamVjdC5rZXlzKG9iamVjdCk7XG5cbiAgICBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykge1xuICAgICAgdmFyIHN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iamVjdCk7XG4gICAgICBpZiAoZW51bWVyYWJsZU9ubHkpIHN5bWJvbHMgPSBzeW1ib2xzLmZpbHRlcihmdW5jdGlvbiAoc3ltKSB7XG4gICAgICAgIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9iamVjdCwgc3ltKS5lbnVtZXJhYmxlO1xuICAgICAgfSk7XG4gICAgICBrZXlzLnB1c2guYXBwbHkoa2V5cywgc3ltYm9scyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGtleXM7XG4gIH1cblxuICBmdW5jdGlvbiBfb2JqZWN0U3ByZWFkMih0YXJnZXQpIHtcbiAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307XG5cbiAgICAgIGlmIChpICUgMikge1xuICAgICAgICBvd25LZXlzKE9iamVjdChzb3VyY2UpLCB0cnVlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICBfZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHNvdXJjZVtrZXldKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKSB7XG4gICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMoc291cmNlKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBvd25LZXlzKE9iamVjdChzb3VyY2UpKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc291cmNlLCBrZXkpKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHRhcmdldDtcbiAgfVxuXG4gIC8vIFRoYW5rcyBAc3RpbXVsdXM6XG4gIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9zdGltdWx1c2pzL3N0aW11bHVzL2Jsb2IvbWFzdGVyL3BhY2thZ2VzLyU0MHN0aW11bHVzL2NvcmUvc3JjL2FwcGxpY2F0aW9uLnRzXG4gIGZ1bmN0aW9uIGRvbVJlYWR5KCkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcbiAgICAgIGlmIChkb2N1bWVudC5yZWFkeVN0YXRlID09IFwibG9hZGluZ1wiKSB7XG4gICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsIHJlc29sdmUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIGZ1bmN0aW9uIGFycmF5VW5pcXVlKGFycmF5KSB7XG4gICAgcmV0dXJuIEFycmF5LmZyb20obmV3IFNldChhcnJheSkpO1xuICB9XG4gIGZ1bmN0aW9uIGlzVGVzdGluZygpIHtcbiAgICByZXR1cm4gbmF2aWdhdG9yLnVzZXJBZ2VudC5pbmNsdWRlcyhcIk5vZGUuanNcIikgfHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5pbmNsdWRlcyhcImpzZG9tXCIpO1xuICB9XG4gIGZ1bmN0aW9uIGNoZWNrZWRBdHRyTG9vc2VDb21wYXJlKHZhbHVlQSwgdmFsdWVCKSB7XG4gICAgcmV0dXJuIHZhbHVlQSA9PSB2YWx1ZUI7XG4gIH1cbiAgZnVuY3Rpb24gd2FybklmTWFsZm9ybWVkVGVtcGxhdGUoZWwsIGRpcmVjdGl2ZSkge1xuICAgIGlmIChlbC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09ICd0ZW1wbGF0ZScpIHtcbiAgICAgIGNvbnNvbGUud2FybihgQWxwaW5lOiBbJHtkaXJlY3RpdmV9XSBkaXJlY3RpdmUgc2hvdWxkIG9ubHkgYmUgYWRkZWQgdG8gPHRlbXBsYXRlPiB0YWdzLiBTZWUgaHR0cHM6Ly9naXRodWIuY29tL2FscGluZWpzL2FscGluZSMke2RpcmVjdGl2ZX1gKTtcbiAgICB9IGVsc2UgaWYgKGVsLmNvbnRlbnQuY2hpbGRFbGVtZW50Q291bnQgIT09IDEpIHtcbiAgICAgIGNvbnNvbGUud2FybihgQWxwaW5lOiA8dGVtcGxhdGU+IHRhZyB3aXRoIFske2RpcmVjdGl2ZX1dIGVuY291bnRlcmVkIHdpdGggYW4gdW5leHBlY3RlZCBudW1iZXIgb2Ygcm9vdCBlbGVtZW50cy4gTWFrZSBzdXJlIDx0ZW1wbGF0ZT4gaGFzIGEgc2luZ2xlIHJvb3QgZWxlbWVudC4gYCk7XG4gICAgfVxuICB9XG4gIGZ1bmN0aW9uIGtlYmFiQ2FzZShzdWJqZWN0KSB7XG4gICAgcmV0dXJuIHN1YmplY3QucmVwbGFjZSgvKFthLXpdKShbQS1aXSkvZywgJyQxLSQyJykucmVwbGFjZSgvW19cXHNdLywgJy0nKS50b0xvd2VyQ2FzZSgpO1xuICB9XG4gIGZ1bmN0aW9uIGNhbWVsQ2FzZShzdWJqZWN0KSB7XG4gICAgcmV0dXJuIHN1YmplY3QudG9Mb3dlckNhc2UoKS5yZXBsYWNlKC8tKFxcdykvZywgKG1hdGNoLCBjaGFyKSA9PiBjaGFyLnRvVXBwZXJDYXNlKCkpO1xuICB9XG4gIGZ1bmN0aW9uIHdhbGsoZWwsIGNhbGxiYWNrKSB7XG4gICAgaWYgKGNhbGxiYWNrKGVsKSA9PT0gZmFsc2UpIHJldHVybjtcbiAgICBsZXQgbm9kZSA9IGVsLmZpcnN0RWxlbWVudENoaWxkO1xuXG4gICAgd2hpbGUgKG5vZGUpIHtcbiAgICAgIHdhbGsobm9kZSwgY2FsbGJhY2spO1xuICAgICAgbm9kZSA9IG5vZGUubmV4dEVsZW1lbnRTaWJsaW5nO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiBkZWJvdW5jZShmdW5jLCB3YWl0KSB7XG4gICAgdmFyIHRpbWVvdXQ7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBjb250ZXh0ID0gdGhpcyxcbiAgICAgICAgICBhcmdzID0gYXJndW1lbnRzO1xuXG4gICAgICB2YXIgbGF0ZXIgPSBmdW5jdGlvbiBsYXRlcigpIHtcbiAgICAgICAgdGltZW91dCA9IG51bGw7XG4gICAgICAgIGZ1bmMuYXBwbHkoY29udGV4dCwgYXJncyk7XG4gICAgICB9O1xuXG4gICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XG4gICAgICB0aW1lb3V0ID0gc2V0VGltZW91dChsYXRlciwgd2FpdCk7XG4gICAgfTtcbiAgfVxuXG4gIGNvbnN0IGhhbmRsZUVycm9yID0gKGVsLCBleHByZXNzaW9uLCBlcnJvcikgPT4ge1xuICAgIGNvbnNvbGUud2FybihgQWxwaW5lIEVycm9yOiBcIiR7ZXJyb3J9XCJcXG5cXG5FeHByZXNzaW9uOiBcIiR7ZXhwcmVzc2lvbn1cIlxcbkVsZW1lbnQ6YCwgZWwpO1xuXG4gICAgaWYgKCFpc1Rlc3RpbmcoKSkge1xuICAgICAgT2JqZWN0LmFzc2lnbihlcnJvciwge1xuICAgICAgICBlbCxcbiAgICAgICAgZXhwcmVzc2lvblxuICAgICAgfSk7XG4gICAgICB0aHJvdyBlcnJvcjtcbiAgICB9XG4gIH07XG5cbiAgZnVuY3Rpb24gdHJ5Q2F0Y2goY2IsIHtcbiAgICBlbCxcbiAgICBleHByZXNzaW9uXG4gIH0pIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgdmFsdWUgPSBjYigpO1xuICAgICAgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUHJvbWlzZSA/IHZhbHVlLmNhdGNoKGUgPT4gaGFuZGxlRXJyb3IoZWwsIGV4cHJlc3Npb24sIGUpKSA6IHZhbHVlO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIGhhbmRsZUVycm9yKGVsLCBleHByZXNzaW9uLCBlKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBzYWZlckV2YWwoZWwsIGV4cHJlc3Npb24sIGRhdGFDb250ZXh0LCBhZGRpdGlvbmFsSGVscGVyVmFyaWFibGVzID0ge30pIHtcbiAgICByZXR1cm4gdHJ5Q2F0Y2goKCkgPT4ge1xuICAgICAgaWYgKHR5cGVvZiBleHByZXNzaW9uID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHJldHVybiBleHByZXNzaW9uLmNhbGwoZGF0YUNvbnRleHQpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbmV3IEZ1bmN0aW9uKFsnJGRhdGEnLCAuLi5PYmplY3Qua2V5cyhhZGRpdGlvbmFsSGVscGVyVmFyaWFibGVzKV0sIGB2YXIgX19hbHBpbmVfcmVzdWx0OyB3aXRoKCRkYXRhKSB7IF9fYWxwaW5lX3Jlc3VsdCA9ICR7ZXhwcmVzc2lvbn0gfTsgcmV0dXJuIF9fYWxwaW5lX3Jlc3VsdGApKGRhdGFDb250ZXh0LCAuLi5PYmplY3QudmFsdWVzKGFkZGl0aW9uYWxIZWxwZXJWYXJpYWJsZXMpKTtcbiAgICB9LCB7XG4gICAgICBlbCxcbiAgICAgIGV4cHJlc3Npb25cbiAgICB9KTtcbiAgfVxuICBmdW5jdGlvbiBzYWZlckV2YWxOb1JldHVybihlbCwgZXhwcmVzc2lvbiwgZGF0YUNvbnRleHQsIGFkZGl0aW9uYWxIZWxwZXJWYXJpYWJsZXMgPSB7fSkge1xuICAgIHJldHVybiB0cnlDYXRjaCgoKSA9PiB7XG4gICAgICBpZiAodHlwZW9mIGV4cHJlc3Npb24gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShleHByZXNzaW9uLmNhbGwoZGF0YUNvbnRleHQsIGFkZGl0aW9uYWxIZWxwZXJWYXJpYWJsZXNbJyRldmVudCddKSk7XG4gICAgICB9XG5cbiAgICAgIGxldCBBc3luY0Z1bmN0aW9uID0gRnVuY3Rpb247XG4gICAgICAvKiBNT0RFUk4tT05MWTpTVEFSVCAqL1xuXG4gICAgICBBc3luY0Z1bmN0aW9uID0gT2JqZWN0LmdldFByb3RvdHlwZU9mKGFzeW5jIGZ1bmN0aW9uICgpIHt9KS5jb25zdHJ1Y3RvcjtcbiAgICAgIC8qIE1PREVSTi1PTkxZOkVORCAqL1xuICAgICAgLy8gRm9yIHRoZSBjYXNlcyB3aGVuIHVzZXJzIHBhc3Mgb25seSBhIGZ1bmN0aW9uIHJlZmVyZW5jZSB0byB0aGUgY2FsbGVyOiBgeC1vbjpjbGljaz1cImZvb1wiYFxuICAgICAgLy8gV2hlcmUgXCJmb29cIiBpcyBhIGZ1bmN0aW9uLiBBbHNvLCB3ZSdsbCBwYXNzIHRoZSBmdW5jdGlvbiB0aGUgZXZlbnQgaW5zdGFuY2Ugd2hlbiB3ZSBjYWxsIGl0LlxuXG4gICAgICBpZiAoT2JqZWN0LmtleXMoZGF0YUNvbnRleHQpLmluY2x1ZGVzKGV4cHJlc3Npb24pKSB7XG4gICAgICAgIGxldCBtZXRob2RSZWZlcmVuY2UgPSBuZXcgRnVuY3Rpb24oWydkYXRhQ29udGV4dCcsIC4uLk9iamVjdC5rZXlzKGFkZGl0aW9uYWxIZWxwZXJWYXJpYWJsZXMpXSwgYHdpdGgoZGF0YUNvbnRleHQpIHsgcmV0dXJuICR7ZXhwcmVzc2lvbn0gfWApKGRhdGFDb250ZXh0LCAuLi5PYmplY3QudmFsdWVzKGFkZGl0aW9uYWxIZWxwZXJWYXJpYWJsZXMpKTtcblxuICAgICAgICBpZiAodHlwZW9mIG1ldGhvZFJlZmVyZW5jZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWV0aG9kUmVmZXJlbmNlLmNhbGwoZGF0YUNvbnRleHQsIGFkZGl0aW9uYWxIZWxwZXJWYXJpYWJsZXNbJyRldmVudCddKSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSgpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobmV3IEFzeW5jRnVuY3Rpb24oWydkYXRhQ29udGV4dCcsIC4uLk9iamVjdC5rZXlzKGFkZGl0aW9uYWxIZWxwZXJWYXJpYWJsZXMpXSwgYHdpdGgoZGF0YUNvbnRleHQpIHsgJHtleHByZXNzaW9ufSB9YCkoZGF0YUNvbnRleHQsIC4uLk9iamVjdC52YWx1ZXMoYWRkaXRpb25hbEhlbHBlclZhcmlhYmxlcykpKTtcbiAgICB9LCB7XG4gICAgICBlbCxcbiAgICAgIGV4cHJlc3Npb25cbiAgICB9KTtcbiAgfVxuICBjb25zdCB4QXR0clJFID0gL154LShvbnxiaW5kfGRhdGF8dGV4dHxodG1sfG1vZGVsfGlmfGZvcnxzaG93fGNsb2FrfHRyYW5zaXRpb258cmVmfHNwcmVhZClcXGIvO1xuICBmdW5jdGlvbiBpc1hBdHRyKGF0dHIpIHtcbiAgICBjb25zdCBuYW1lID0gcmVwbGFjZUF0QW5kQ29sb25XaXRoU3RhbmRhcmRTeW50YXgoYXR0ci5uYW1lKTtcbiAgICByZXR1cm4geEF0dHJSRS50ZXN0KG5hbWUpO1xuICB9XG4gIGZ1bmN0aW9uIGdldFhBdHRycyhlbCwgY29tcG9uZW50LCB0eXBlKSB7XG4gICAgbGV0IGRpcmVjdGl2ZXMgPSBBcnJheS5mcm9tKGVsLmF0dHJpYnV0ZXMpLmZpbHRlcihpc1hBdHRyKS5tYXAocGFyc2VIdG1sQXR0cmlidXRlKTsgLy8gR2V0IGFuIG9iamVjdCBvZiBkaXJlY3RpdmVzIGZyb20geC1zcHJlYWQuXG5cbiAgICBsZXQgc3ByZWFkRGlyZWN0aXZlID0gZGlyZWN0aXZlcy5maWx0ZXIoZGlyZWN0aXZlID0+IGRpcmVjdGl2ZS50eXBlID09PSAnc3ByZWFkJylbMF07XG5cbiAgICBpZiAoc3ByZWFkRGlyZWN0aXZlKSB7XG4gICAgICBsZXQgc3ByZWFkT2JqZWN0ID0gc2FmZXJFdmFsKGVsLCBzcHJlYWREaXJlY3RpdmUuZXhwcmVzc2lvbiwgY29tcG9uZW50LiRkYXRhKTsgLy8gQWRkIHgtc3ByZWFkIGRpcmVjdGl2ZXMgdG8gdGhlIHBpbGUgb2YgZXhpc3RpbmcgZGlyZWN0aXZlcy5cblxuICAgICAgZGlyZWN0aXZlcyA9IGRpcmVjdGl2ZXMuY29uY2F0KE9iamVjdC5lbnRyaWVzKHNwcmVhZE9iamVjdCkubWFwKChbbmFtZSwgdmFsdWVdKSA9PiBwYXJzZUh0bWxBdHRyaWJ1dGUoe1xuICAgICAgICBuYW1lLFxuICAgICAgICB2YWx1ZVxuICAgICAgfSkpKTtcbiAgICB9XG5cbiAgICBpZiAodHlwZSkgcmV0dXJuIGRpcmVjdGl2ZXMuZmlsdGVyKGkgPT4gaS50eXBlID09PSB0eXBlKTtcbiAgICByZXR1cm4gc29ydERpcmVjdGl2ZXMoZGlyZWN0aXZlcyk7XG4gIH1cblxuICBmdW5jdGlvbiBzb3J0RGlyZWN0aXZlcyhkaXJlY3RpdmVzKSB7XG4gICAgbGV0IGRpcmVjdGl2ZU9yZGVyID0gWydiaW5kJywgJ21vZGVsJywgJ3Nob3cnLCAnY2F0Y2gtYWxsJ107XG4gICAgcmV0dXJuIGRpcmVjdGl2ZXMuc29ydCgoYSwgYikgPT4ge1xuICAgICAgbGV0IHR5cGVBID0gZGlyZWN0aXZlT3JkZXIuaW5kZXhPZihhLnR5cGUpID09PSAtMSA/ICdjYXRjaC1hbGwnIDogYS50eXBlO1xuICAgICAgbGV0IHR5cGVCID0gZGlyZWN0aXZlT3JkZXIuaW5kZXhPZihiLnR5cGUpID09PSAtMSA/ICdjYXRjaC1hbGwnIDogYi50eXBlO1xuICAgICAgcmV0dXJuIGRpcmVjdGl2ZU9yZGVyLmluZGV4T2YodHlwZUEpIC0gZGlyZWN0aXZlT3JkZXIuaW5kZXhPZih0eXBlQik7XG4gICAgfSk7XG4gIH1cblxuICBmdW5jdGlvbiBwYXJzZUh0bWxBdHRyaWJ1dGUoe1xuICAgIG5hbWUsXG4gICAgdmFsdWVcbiAgfSkge1xuICAgIGNvbnN0IG5vcm1hbGl6ZWROYW1lID0gcmVwbGFjZUF0QW5kQ29sb25XaXRoU3RhbmRhcmRTeW50YXgobmFtZSk7XG4gICAgY29uc3QgdHlwZU1hdGNoID0gbm9ybWFsaXplZE5hbWUubWF0Y2goeEF0dHJSRSk7XG4gICAgY29uc3QgdmFsdWVNYXRjaCA9IG5vcm1hbGl6ZWROYW1lLm1hdGNoKC86KFthLXpBLVowLTlcXC06XSspLyk7XG4gICAgY29uc3QgbW9kaWZpZXJzID0gbm9ybWFsaXplZE5hbWUubWF0Y2goL1xcLlteLlxcXV0rKD89W15cXF1dKiQpL2cpIHx8IFtdO1xuICAgIHJldHVybiB7XG4gICAgICB0eXBlOiB0eXBlTWF0Y2ggPyB0eXBlTWF0Y2hbMV0gOiBudWxsLFxuICAgICAgdmFsdWU6IHZhbHVlTWF0Y2ggPyB2YWx1ZU1hdGNoWzFdIDogbnVsbCxcbiAgICAgIG1vZGlmaWVyczogbW9kaWZpZXJzLm1hcChpID0+IGkucmVwbGFjZSgnLicsICcnKSksXG4gICAgICBleHByZXNzaW9uOiB2YWx1ZVxuICAgIH07XG4gIH1cbiAgZnVuY3Rpb24gaXNCb29sZWFuQXR0cihhdHRyTmFtZSkge1xuICAgIC8vIEFzIHBlciBIVE1MIHNwZWMgdGFibGUgaHR0cHM6Ly9odG1sLnNwZWMud2hhdHdnLm9yZy9tdWx0aXBhZ2UvaW5kaWNlcy5odG1sI2F0dHJpYnV0ZXMtMzpib29sZWFuLWF0dHJpYnV0ZVxuICAgIC8vIEFycmF5IHJvdWdobHkgb3JkZXJlZCBieSBlc3RpbWF0ZWQgdXNhZ2VcbiAgICBjb25zdCBib29sZWFuQXR0cmlidXRlcyA9IFsnZGlzYWJsZWQnLCAnY2hlY2tlZCcsICdyZXF1aXJlZCcsICdyZWFkb25seScsICdoaWRkZW4nLCAnb3BlbicsICdzZWxlY3RlZCcsICdhdXRvZm9jdXMnLCAnaXRlbXNjb3BlJywgJ211bHRpcGxlJywgJ25vdmFsaWRhdGUnLCAnYWxsb3dmdWxsc2NyZWVuJywgJ2FsbG93cGF5bWVudHJlcXVlc3QnLCAnZm9ybW5vdmFsaWRhdGUnLCAnYXV0b3BsYXknLCAnY29udHJvbHMnLCAnbG9vcCcsICdtdXRlZCcsICdwbGF5c2lubGluZScsICdkZWZhdWx0JywgJ2lzbWFwJywgJ3JldmVyc2VkJywgJ2FzeW5jJywgJ2RlZmVyJywgJ25vbW9kdWxlJ107XG4gICAgcmV0dXJuIGJvb2xlYW5BdHRyaWJ1dGVzLmluY2x1ZGVzKGF0dHJOYW1lKTtcbiAgfVxuICBmdW5jdGlvbiByZXBsYWNlQXRBbmRDb2xvbldpdGhTdGFuZGFyZFN5bnRheChuYW1lKSB7XG4gICAgaWYgKG5hbWUuc3RhcnRzV2l0aCgnQCcpKSB7XG4gICAgICByZXR1cm4gbmFtZS5yZXBsYWNlKCdAJywgJ3gtb246Jyk7XG4gICAgfSBlbHNlIGlmIChuYW1lLnN0YXJ0c1dpdGgoJzonKSkge1xuICAgICAgcmV0dXJuIG5hbWUucmVwbGFjZSgnOicsICd4LWJpbmQ6Jyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5hbWU7XG4gIH1cbiAgZnVuY3Rpb24gY29udmVydENsYXNzU3RyaW5nVG9BcnJheShjbGFzc0xpc3QsIGZpbHRlckZuID0gQm9vbGVhbikge1xuICAgIHJldHVybiBjbGFzc0xpc3Quc3BsaXQoJyAnKS5maWx0ZXIoZmlsdGVyRm4pO1xuICB9XG4gIGNvbnN0IFRSQU5TSVRJT05fVFlQRV9JTiA9ICdpbic7XG4gIGNvbnN0IFRSQU5TSVRJT05fVFlQRV9PVVQgPSAnb3V0JztcbiAgY29uc3QgVFJBTlNJVElPTl9DQU5DRUxMRUQgPSAnY2FuY2VsbGVkJztcbiAgZnVuY3Rpb24gdHJhbnNpdGlvbkluKGVsLCBzaG93LCByZWplY3QsIGNvbXBvbmVudCwgZm9yY2VTa2lwID0gZmFsc2UpIHtcbiAgICAvLyBXZSBkb24ndCB3YW50IHRvIHRyYW5zaXRpb24gb24gdGhlIGluaXRpYWwgcGFnZSBsb2FkLlxuICAgIGlmIChmb3JjZVNraXApIHJldHVybiBzaG93KCk7XG5cbiAgICBpZiAoZWwuX194X3RyYW5zaXRpb24gJiYgZWwuX194X3RyYW5zaXRpb24udHlwZSA9PT0gVFJBTlNJVElPTl9UWVBFX0lOKSB7XG4gICAgICAvLyB0aGVyZSBpcyBhbHJlYWR5IGEgc2ltaWxhciB0cmFuc2l0aW9uIGdvaW5nIG9uLCB0aGlzIHdhcyBwcm9iYWJseSB0cmlnZ2VyZWQgYnlcbiAgICAgIC8vIGEgY2hhbmdlIGluIGEgZGlmZmVyZW50IHByb3BlcnR5LCBsZXQncyBqdXN0IGxlYXZlIHRoZSBwcmV2aW91cyBvbmUgZG9pbmcgaXRzIGpvYlxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IGF0dHJzID0gZ2V0WEF0dHJzKGVsLCBjb21wb25lbnQsICd0cmFuc2l0aW9uJyk7XG4gICAgY29uc3Qgc2hvd0F0dHIgPSBnZXRYQXR0cnMoZWwsIGNvbXBvbmVudCwgJ3Nob3cnKVswXTsgLy8gSWYgdGhpcyBpcyB0cmlnZ2VyZWQgYnkgYSB4LXNob3cudHJhbnNpdGlvbi5cblxuICAgIGlmIChzaG93QXR0ciAmJiBzaG93QXR0ci5tb2RpZmllcnMuaW5jbHVkZXMoJ3RyYW5zaXRpb24nKSkge1xuICAgICAgbGV0IG1vZGlmaWVycyA9IHNob3dBdHRyLm1vZGlmaWVyczsgLy8gSWYgeC1zaG93LnRyYW5zaXRpb24ub3V0LCB3ZSdsbCBza2lwIHRoZSBcImluXCIgdHJhbnNpdGlvbi5cblxuICAgICAgaWYgKG1vZGlmaWVycy5pbmNsdWRlcygnb3V0JykgJiYgIW1vZGlmaWVycy5pbmNsdWRlcygnaW4nKSkgcmV0dXJuIHNob3coKTtcbiAgICAgIGNvbnN0IHNldHRpbmdCb3RoU2lkZXNPZlRyYW5zaXRpb24gPSBtb2RpZmllcnMuaW5jbHVkZXMoJ2luJykgJiYgbW9kaWZpZXJzLmluY2x1ZGVzKCdvdXQnKTsgLy8gSWYgeC1zaG93LnRyYW5zaXRpb24uaW4uLi5vdXQuLi4gb25seSB1c2UgXCJpblwiIHJlbGF0ZWQgbW9kaWZpZXJzIGZvciB0aGlzIHRyYW5zaXRpb24uXG5cbiAgICAgIG1vZGlmaWVycyA9IHNldHRpbmdCb3RoU2lkZXNPZlRyYW5zaXRpb24gPyBtb2RpZmllcnMuZmlsdGVyKChpLCBpbmRleCkgPT4gaW5kZXggPCBtb2RpZmllcnMuaW5kZXhPZignb3V0JykpIDogbW9kaWZpZXJzO1xuICAgICAgdHJhbnNpdGlvbkhlbHBlckluKGVsLCBtb2RpZmllcnMsIHNob3csIHJlamVjdCk7IC8vIE90aGVyd2lzZSwgd2UgY2FuIGFzc3VtZSB4LXRyYW5zaXRpb246ZW50ZXIuXG4gICAgfSBlbHNlIGlmIChhdHRycy5zb21lKGF0dHIgPT4gWydlbnRlcicsICdlbnRlci1zdGFydCcsICdlbnRlci1lbmQnXS5pbmNsdWRlcyhhdHRyLnZhbHVlKSkpIHtcbiAgICAgIHRyYW5zaXRpb25DbGFzc2VzSW4oZWwsIGNvbXBvbmVudCwgYXR0cnMsIHNob3csIHJlamVjdCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIElmIG5laXRoZXIsIGp1c3Qgc2hvdyB0aGF0IGRhbW4gdGhpbmcuXG4gICAgICBzaG93KCk7XG4gICAgfVxuICB9XG4gIGZ1bmN0aW9uIHRyYW5zaXRpb25PdXQoZWwsIGhpZGUsIHJlamVjdCwgY29tcG9uZW50LCBmb3JjZVNraXAgPSBmYWxzZSkge1xuICAgIC8vIFdlIGRvbid0IHdhbnQgdG8gdHJhbnNpdGlvbiBvbiB0aGUgaW5pdGlhbCBwYWdlIGxvYWQuXG4gICAgaWYgKGZvcmNlU2tpcCkgcmV0dXJuIGhpZGUoKTtcblxuICAgIGlmIChlbC5fX3hfdHJhbnNpdGlvbiAmJiBlbC5fX3hfdHJhbnNpdGlvbi50eXBlID09PSBUUkFOU0lUSU9OX1RZUEVfT1VUKSB7XG4gICAgICAvLyB0aGVyZSBpcyBhbHJlYWR5IGEgc2ltaWxhciB0cmFuc2l0aW9uIGdvaW5nIG9uLCB0aGlzIHdhcyBwcm9iYWJseSB0cmlnZ2VyZWQgYnlcbiAgICAgIC8vIGEgY2hhbmdlIGluIGEgZGlmZmVyZW50IHByb3BlcnR5LCBsZXQncyBqdXN0IGxlYXZlIHRoZSBwcmV2aW91cyBvbmUgZG9pbmcgaXRzIGpvYlxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IGF0dHJzID0gZ2V0WEF0dHJzKGVsLCBjb21wb25lbnQsICd0cmFuc2l0aW9uJyk7XG4gICAgY29uc3Qgc2hvd0F0dHIgPSBnZXRYQXR0cnMoZWwsIGNvbXBvbmVudCwgJ3Nob3cnKVswXTtcblxuICAgIGlmIChzaG93QXR0ciAmJiBzaG93QXR0ci5tb2RpZmllcnMuaW5jbHVkZXMoJ3RyYW5zaXRpb24nKSkge1xuICAgICAgbGV0IG1vZGlmaWVycyA9IHNob3dBdHRyLm1vZGlmaWVycztcbiAgICAgIGlmIChtb2RpZmllcnMuaW5jbHVkZXMoJ2luJykgJiYgIW1vZGlmaWVycy5pbmNsdWRlcygnb3V0JykpIHJldHVybiBoaWRlKCk7XG4gICAgICBjb25zdCBzZXR0aW5nQm90aFNpZGVzT2ZUcmFuc2l0aW9uID0gbW9kaWZpZXJzLmluY2x1ZGVzKCdpbicpICYmIG1vZGlmaWVycy5pbmNsdWRlcygnb3V0Jyk7XG4gICAgICBtb2RpZmllcnMgPSBzZXR0aW5nQm90aFNpZGVzT2ZUcmFuc2l0aW9uID8gbW9kaWZpZXJzLmZpbHRlcigoaSwgaW5kZXgpID0+IGluZGV4ID4gbW9kaWZpZXJzLmluZGV4T2YoJ291dCcpKSA6IG1vZGlmaWVycztcbiAgICAgIHRyYW5zaXRpb25IZWxwZXJPdXQoZWwsIG1vZGlmaWVycywgc2V0dGluZ0JvdGhTaWRlc09mVHJhbnNpdGlvbiwgaGlkZSwgcmVqZWN0KTtcbiAgICB9IGVsc2UgaWYgKGF0dHJzLnNvbWUoYXR0ciA9PiBbJ2xlYXZlJywgJ2xlYXZlLXN0YXJ0JywgJ2xlYXZlLWVuZCddLmluY2x1ZGVzKGF0dHIudmFsdWUpKSkge1xuICAgICAgdHJhbnNpdGlvbkNsYXNzZXNPdXQoZWwsIGNvbXBvbmVudCwgYXR0cnMsIGhpZGUsIHJlamVjdCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGhpZGUoKTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gdHJhbnNpdGlvbkhlbHBlckluKGVsLCBtb2RpZmllcnMsIHNob3dDYWxsYmFjaywgcmVqZWN0KSB7XG4gICAgLy8gRGVmYXVsdCB2YWx1ZXMgaW5zcGlyZWQgYnk6IGh0dHBzOi8vbWF0ZXJpYWwuaW8vZGVzaWduL21vdGlvbi9zcGVlZC5odG1sI2R1cmF0aW9uXG4gICAgY29uc3Qgc3R5bGVWYWx1ZXMgPSB7XG4gICAgICBkdXJhdGlvbjogbW9kaWZpZXJWYWx1ZShtb2RpZmllcnMsICdkdXJhdGlvbicsIDE1MCksXG4gICAgICBvcmlnaW46IG1vZGlmaWVyVmFsdWUobW9kaWZpZXJzLCAnb3JpZ2luJywgJ2NlbnRlcicpLFxuICAgICAgZmlyc3Q6IHtcbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgc2NhbGU6IG1vZGlmaWVyVmFsdWUobW9kaWZpZXJzLCAnc2NhbGUnLCA5NSlcbiAgICAgIH0sXG4gICAgICBzZWNvbmQ6IHtcbiAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgc2NhbGU6IDEwMFxuICAgICAgfVxuICAgIH07XG4gICAgdHJhbnNpdGlvbkhlbHBlcihlbCwgbW9kaWZpZXJzLCBzaG93Q2FsbGJhY2ssICgpID0+IHt9LCByZWplY3QsIHN0eWxlVmFsdWVzLCBUUkFOU0lUSU9OX1RZUEVfSU4pO1xuICB9XG4gIGZ1bmN0aW9uIHRyYW5zaXRpb25IZWxwZXJPdXQoZWwsIG1vZGlmaWVycywgc2V0dGluZ0JvdGhTaWRlc09mVHJhbnNpdGlvbiwgaGlkZUNhbGxiYWNrLCByZWplY3QpIHtcbiAgICAvLyBNYWtlIHRoZSBcIm91dFwiIHRyYW5zaXRpb24gLjV4IHNsb3dlciB0aGFuIHRoZSBcImluXCIuIChWaXN1YWxseSBiZXR0ZXIpXG4gICAgLy8gSE9XRVZFUiwgaWYgdGhleSBleHBsaWNpdGx5IHNldCBhIGR1cmF0aW9uIGZvciB0aGUgXCJvdXRcIiB0cmFuc2l0aW9uLFxuICAgIC8vIHVzZSB0aGF0LlxuICAgIGNvbnN0IGR1cmF0aW9uID0gc2V0dGluZ0JvdGhTaWRlc09mVHJhbnNpdGlvbiA/IG1vZGlmaWVyVmFsdWUobW9kaWZpZXJzLCAnZHVyYXRpb24nLCAxNTApIDogbW9kaWZpZXJWYWx1ZShtb2RpZmllcnMsICdkdXJhdGlvbicsIDE1MCkgLyAyO1xuICAgIGNvbnN0IHN0eWxlVmFsdWVzID0ge1xuICAgICAgZHVyYXRpb246IGR1cmF0aW9uLFxuICAgICAgb3JpZ2luOiBtb2RpZmllclZhbHVlKG1vZGlmaWVycywgJ29yaWdpbicsICdjZW50ZXInKSxcbiAgICAgIGZpcnN0OiB7XG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIHNjYWxlOiAxMDBcbiAgICAgIH0sXG4gICAgICBzZWNvbmQ6IHtcbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgc2NhbGU6IG1vZGlmaWVyVmFsdWUobW9kaWZpZXJzLCAnc2NhbGUnLCA5NSlcbiAgICAgIH1cbiAgICB9O1xuICAgIHRyYW5zaXRpb25IZWxwZXIoZWwsIG1vZGlmaWVycywgKCkgPT4ge30sIGhpZGVDYWxsYmFjaywgcmVqZWN0LCBzdHlsZVZhbHVlcywgVFJBTlNJVElPTl9UWVBFX09VVCk7XG4gIH1cblxuICBmdW5jdGlvbiBtb2RpZmllclZhbHVlKG1vZGlmaWVycywga2V5LCBmYWxsYmFjaykge1xuICAgIC8vIElmIHRoZSBtb2RpZmllciBpc24ndCBwcmVzZW50LCB1c2UgdGhlIGRlZmF1bHQuXG4gICAgaWYgKG1vZGlmaWVycy5pbmRleE9mKGtleSkgPT09IC0xKSByZXR1cm4gZmFsbGJhY2s7IC8vIElmIGl0IElTIHByZXNlbnQsIGdyYWIgdGhlIHZhbHVlIGFmdGVyIGl0OiB4LXNob3cudHJhbnNpdGlvbi5kdXJhdGlvbi41MDBtc1xuXG4gICAgY29uc3QgcmF3VmFsdWUgPSBtb2RpZmllcnNbbW9kaWZpZXJzLmluZGV4T2Yoa2V5KSArIDFdO1xuICAgIGlmICghcmF3VmFsdWUpIHJldHVybiBmYWxsYmFjaztcblxuICAgIGlmIChrZXkgPT09ICdzY2FsZScpIHtcbiAgICAgIC8vIENoZWNrIGlmIHRoZSB2ZXJ5IG5leHQgdmFsdWUgaXMgTk9UIGEgbnVtYmVyIGFuZCByZXR1cm4gdGhlIGZhbGxiYWNrLlxuICAgICAgLy8gSWYgeC1zaG93LnRyYW5zaXRpb24uc2NhbGUsIHdlJ2xsIHVzZSB0aGUgZGVmYXVsdCBzY2FsZSB2YWx1ZS5cbiAgICAgIC8vIFRoYXQgaXMgaG93IGEgdXNlciBvcHRzIG91dCBvZiB0aGUgb3BhY2l0eSB0cmFuc2l0aW9uLlxuICAgICAgaWYgKCFpc051bWVyaWMocmF3VmFsdWUpKSByZXR1cm4gZmFsbGJhY2s7XG4gICAgfVxuXG4gICAgaWYgKGtleSA9PT0gJ2R1cmF0aW9uJykge1xuICAgICAgLy8gU3VwcG9ydCB4LXNob3cudHJhbnNpdGlvbi5kdXJhdGlvbi41MDBtcyAmJiBkdXJhdGlvbi41MDBcbiAgICAgIGxldCBtYXRjaCA9IHJhd1ZhbHVlLm1hdGNoKC8oWzAtOV0rKW1zLyk7XG4gICAgICBpZiAobWF0Y2gpIHJldHVybiBtYXRjaFsxXTtcbiAgICB9XG5cbiAgICBpZiAoa2V5ID09PSAnb3JpZ2luJykge1xuICAgICAgLy8gU3VwcG9ydCBjaGFpbmluZyBvcmlnaW4gZGlyZWN0aW9uczogeC1zaG93LnRyYW5zaXRpb24udG9wLnJpZ2h0XG4gICAgICBpZiAoWyd0b3AnLCAncmlnaHQnLCAnbGVmdCcsICdjZW50ZXInLCAnYm90dG9tJ10uaW5jbHVkZXMobW9kaWZpZXJzW21vZGlmaWVycy5pbmRleE9mKGtleSkgKyAyXSkpIHtcbiAgICAgICAgcmV0dXJuIFtyYXdWYWx1ZSwgbW9kaWZpZXJzW21vZGlmaWVycy5pbmRleE9mKGtleSkgKyAyXV0uam9pbignICcpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiByYXdWYWx1ZTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHRyYW5zaXRpb25IZWxwZXIoZWwsIG1vZGlmaWVycywgaG9vazEsIGhvb2syLCByZWplY3QsIHN0eWxlVmFsdWVzLCB0eXBlKSB7XG4gICAgLy8gY2xlYXIgdGhlIHByZXZpb3VzIHRyYW5zaXRpb24gaWYgZXhpc3RzIHRvIGF2b2lkIGNhY2hpbmcgdGhlIHdyb25nIHN0eWxlc1xuICAgIGlmIChlbC5fX3hfdHJhbnNpdGlvbikge1xuICAgICAgZWwuX194X3RyYW5zaXRpb24uY2FuY2VsICYmIGVsLl9feF90cmFuc2l0aW9uLmNhbmNlbCgpO1xuICAgIH0gLy8gSWYgdGhlIHVzZXIgc2V0IHRoZXNlIHN0eWxlIHZhbHVlcywgd2UnbGwgcHV0IHRoZW0gYmFjayB3aGVuIHdlJ3JlIGRvbmUgd2l0aCB0aGVtLlxuXG5cbiAgICBjb25zdCBvcGFjaXR5Q2FjaGUgPSBlbC5zdHlsZS5vcGFjaXR5O1xuICAgIGNvbnN0IHRyYW5zZm9ybUNhY2hlID0gZWwuc3R5bGUudHJhbnNmb3JtO1xuICAgIGNvbnN0IHRyYW5zZm9ybU9yaWdpbkNhY2hlID0gZWwuc3R5bGUudHJhbnNmb3JtT3JpZ2luOyAvLyBJZiBubyBtb2RpZmllcnMgYXJlIHByZXNlbnQ6IHgtc2hvdy50cmFuc2l0aW9uLCB3ZSdsbCBkZWZhdWx0IHRvIGJvdGggb3BhY2l0eSBhbmQgc2NhbGUuXG5cbiAgICBjb25zdCBub01vZGlmaWVycyA9ICFtb2RpZmllcnMuaW5jbHVkZXMoJ29wYWNpdHknKSAmJiAhbW9kaWZpZXJzLmluY2x1ZGVzKCdzY2FsZScpO1xuICAgIGNvbnN0IHRyYW5zaXRpb25PcGFjaXR5ID0gbm9Nb2RpZmllcnMgfHwgbW9kaWZpZXJzLmluY2x1ZGVzKCdvcGFjaXR5Jyk7XG4gICAgY29uc3QgdHJhbnNpdGlvblNjYWxlID0gbm9Nb2RpZmllcnMgfHwgbW9kaWZpZXJzLmluY2x1ZGVzKCdzY2FsZScpOyAvLyBUaGVzZSBhcmUgdGhlIGV4cGxpY2l0IHN0YWdlcyBvZiBhIHRyYW5zaXRpb24gKHNhbWUgc3RhZ2VzIGZvciBpbiBhbmQgZm9yIG91dCkuXG4gICAgLy8gVGhpcyB3YXkgeW91IGNhbiBnZXQgYSBiaXJkcyBleWUgdmlldyBvZiB0aGUgaG9va3MsIGFuZCB0aGUgZGlmZmVyZW5jZXNcbiAgICAvLyBiZXR3ZWVuIHRoZW0uXG5cbiAgICBjb25zdCBzdGFnZXMgPSB7XG4gICAgICBzdGFydCgpIHtcbiAgICAgICAgaWYgKHRyYW5zaXRpb25PcGFjaXR5KSBlbC5zdHlsZS5vcGFjaXR5ID0gc3R5bGVWYWx1ZXMuZmlyc3Qub3BhY2l0eTtcbiAgICAgICAgaWYgKHRyYW5zaXRpb25TY2FsZSkgZWwuc3R5bGUudHJhbnNmb3JtID0gYHNjYWxlKCR7c3R5bGVWYWx1ZXMuZmlyc3Quc2NhbGUgLyAxMDB9KWA7XG4gICAgICB9LFxuXG4gICAgICBkdXJpbmcoKSB7XG4gICAgICAgIGlmICh0cmFuc2l0aW9uU2NhbGUpIGVsLnN0eWxlLnRyYW5zZm9ybU9yaWdpbiA9IHN0eWxlVmFsdWVzLm9yaWdpbjtcbiAgICAgICAgZWwuc3R5bGUudHJhbnNpdGlvblByb3BlcnR5ID0gW3RyYW5zaXRpb25PcGFjaXR5ID8gYG9wYWNpdHlgIDogYGAsIHRyYW5zaXRpb25TY2FsZSA/IGB0cmFuc2Zvcm1gIDogYGBdLmpvaW4oJyAnKS50cmltKCk7XG4gICAgICAgIGVsLnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IGAke3N0eWxlVmFsdWVzLmR1cmF0aW9uIC8gMTAwMH1zYDtcbiAgICAgICAgZWwuc3R5bGUudHJhbnNpdGlvblRpbWluZ0Z1bmN0aW9uID0gYGN1YmljLWJlemllcigwLjQsIDAuMCwgMC4yLCAxKWA7XG4gICAgICB9LFxuXG4gICAgICBzaG93KCkge1xuICAgICAgICBob29rMSgpO1xuICAgICAgfSxcblxuICAgICAgZW5kKCkge1xuICAgICAgICBpZiAodHJhbnNpdGlvbk9wYWNpdHkpIGVsLnN0eWxlLm9wYWNpdHkgPSBzdHlsZVZhbHVlcy5zZWNvbmQub3BhY2l0eTtcbiAgICAgICAgaWYgKHRyYW5zaXRpb25TY2FsZSkgZWwuc3R5bGUudHJhbnNmb3JtID0gYHNjYWxlKCR7c3R5bGVWYWx1ZXMuc2Vjb25kLnNjYWxlIC8gMTAwfSlgO1xuICAgICAgfSxcblxuICAgICAgaGlkZSgpIHtcbiAgICAgICAgaG9vazIoKTtcbiAgICAgIH0sXG5cbiAgICAgIGNsZWFudXAoKSB7XG4gICAgICAgIGlmICh0cmFuc2l0aW9uT3BhY2l0eSkgZWwuc3R5bGUub3BhY2l0eSA9IG9wYWNpdHlDYWNoZTtcbiAgICAgICAgaWYgKHRyYW5zaXRpb25TY2FsZSkgZWwuc3R5bGUudHJhbnNmb3JtID0gdHJhbnNmb3JtQ2FjaGU7XG4gICAgICAgIGlmICh0cmFuc2l0aW9uU2NhbGUpIGVsLnN0eWxlLnRyYW5zZm9ybU9yaWdpbiA9IHRyYW5zZm9ybU9yaWdpbkNhY2hlO1xuICAgICAgICBlbC5zdHlsZS50cmFuc2l0aW9uUHJvcGVydHkgPSBudWxsO1xuICAgICAgICBlbC5zdHlsZS50cmFuc2l0aW9uRHVyYXRpb24gPSBudWxsO1xuICAgICAgICBlbC5zdHlsZS50cmFuc2l0aW9uVGltaW5nRnVuY3Rpb24gPSBudWxsO1xuICAgICAgfVxuXG4gICAgfTtcbiAgICB0cmFuc2l0aW9uKGVsLCBzdGFnZXMsIHR5cGUsIHJlamVjdCk7XG4gIH1cblxuICBjb25zdCBlbnN1cmVTdHJpbmdFeHByZXNzaW9uID0gKGV4cHJlc3Npb24sIGVsLCBjb21wb25lbnQpID0+IHtcbiAgICByZXR1cm4gdHlwZW9mIGV4cHJlc3Npb24gPT09ICdmdW5jdGlvbicgPyBjb21wb25lbnQuZXZhbHVhdGVSZXR1cm5FeHByZXNzaW9uKGVsLCBleHByZXNzaW9uKSA6IGV4cHJlc3Npb247XG4gIH07XG5cbiAgZnVuY3Rpb24gdHJhbnNpdGlvbkNsYXNzZXNJbihlbCwgY29tcG9uZW50LCBkaXJlY3RpdmVzLCBzaG93Q2FsbGJhY2ssIHJlamVjdCkge1xuICAgIGNvbnN0IGVudGVyID0gY29udmVydENsYXNzU3RyaW5nVG9BcnJheShlbnN1cmVTdHJpbmdFeHByZXNzaW9uKChkaXJlY3RpdmVzLmZpbmQoaSA9PiBpLnZhbHVlID09PSAnZW50ZXInKSB8fCB7XG4gICAgICBleHByZXNzaW9uOiAnJ1xuICAgIH0pLmV4cHJlc3Npb24sIGVsLCBjb21wb25lbnQpKTtcbiAgICBjb25zdCBlbnRlclN0YXJ0ID0gY29udmVydENsYXNzU3RyaW5nVG9BcnJheShlbnN1cmVTdHJpbmdFeHByZXNzaW9uKChkaXJlY3RpdmVzLmZpbmQoaSA9PiBpLnZhbHVlID09PSAnZW50ZXItc3RhcnQnKSB8fCB7XG4gICAgICBleHByZXNzaW9uOiAnJ1xuICAgIH0pLmV4cHJlc3Npb24sIGVsLCBjb21wb25lbnQpKTtcbiAgICBjb25zdCBlbnRlckVuZCA9IGNvbnZlcnRDbGFzc1N0cmluZ1RvQXJyYXkoZW5zdXJlU3RyaW5nRXhwcmVzc2lvbigoZGlyZWN0aXZlcy5maW5kKGkgPT4gaS52YWx1ZSA9PT0gJ2VudGVyLWVuZCcpIHx8IHtcbiAgICAgIGV4cHJlc3Npb246ICcnXG4gICAgfSkuZXhwcmVzc2lvbiwgZWwsIGNvbXBvbmVudCkpO1xuICAgIHRyYW5zaXRpb25DbGFzc2VzKGVsLCBlbnRlciwgZW50ZXJTdGFydCwgZW50ZXJFbmQsIHNob3dDYWxsYmFjaywgKCkgPT4ge30sIFRSQU5TSVRJT05fVFlQRV9JTiwgcmVqZWN0KTtcbiAgfVxuICBmdW5jdGlvbiB0cmFuc2l0aW9uQ2xhc3Nlc091dChlbCwgY29tcG9uZW50LCBkaXJlY3RpdmVzLCBoaWRlQ2FsbGJhY2ssIHJlamVjdCkge1xuICAgIGNvbnN0IGxlYXZlID0gY29udmVydENsYXNzU3RyaW5nVG9BcnJheShlbnN1cmVTdHJpbmdFeHByZXNzaW9uKChkaXJlY3RpdmVzLmZpbmQoaSA9PiBpLnZhbHVlID09PSAnbGVhdmUnKSB8fCB7XG4gICAgICBleHByZXNzaW9uOiAnJ1xuICAgIH0pLmV4cHJlc3Npb24sIGVsLCBjb21wb25lbnQpKTtcbiAgICBjb25zdCBsZWF2ZVN0YXJ0ID0gY29udmVydENsYXNzU3RyaW5nVG9BcnJheShlbnN1cmVTdHJpbmdFeHByZXNzaW9uKChkaXJlY3RpdmVzLmZpbmQoaSA9PiBpLnZhbHVlID09PSAnbGVhdmUtc3RhcnQnKSB8fCB7XG4gICAgICBleHByZXNzaW9uOiAnJ1xuICAgIH0pLmV4cHJlc3Npb24sIGVsLCBjb21wb25lbnQpKTtcbiAgICBjb25zdCBsZWF2ZUVuZCA9IGNvbnZlcnRDbGFzc1N0cmluZ1RvQXJyYXkoZW5zdXJlU3RyaW5nRXhwcmVzc2lvbigoZGlyZWN0aXZlcy5maW5kKGkgPT4gaS52YWx1ZSA9PT0gJ2xlYXZlLWVuZCcpIHx8IHtcbiAgICAgIGV4cHJlc3Npb246ICcnXG4gICAgfSkuZXhwcmVzc2lvbiwgZWwsIGNvbXBvbmVudCkpO1xuICAgIHRyYW5zaXRpb25DbGFzc2VzKGVsLCBsZWF2ZSwgbGVhdmVTdGFydCwgbGVhdmVFbmQsICgpID0+IHt9LCBoaWRlQ2FsbGJhY2ssIFRSQU5TSVRJT05fVFlQRV9PVVQsIHJlamVjdCk7XG4gIH1cbiAgZnVuY3Rpb24gdHJhbnNpdGlvbkNsYXNzZXMoZWwsIGNsYXNzZXNEdXJpbmcsIGNsYXNzZXNTdGFydCwgY2xhc3Nlc0VuZCwgaG9vazEsIGhvb2syLCB0eXBlLCByZWplY3QpIHtcbiAgICAvLyBjbGVhciB0aGUgcHJldmlvdXMgdHJhbnNpdGlvbiBpZiBleGlzdHMgdG8gYXZvaWQgY2FjaGluZyB0aGUgd3JvbmcgY2xhc3Nlc1xuICAgIGlmIChlbC5fX3hfdHJhbnNpdGlvbikge1xuICAgICAgZWwuX194X3RyYW5zaXRpb24uY2FuY2VsICYmIGVsLl9feF90cmFuc2l0aW9uLmNhbmNlbCgpO1xuICAgIH1cblxuICAgIGNvbnN0IG9yaWdpbmFsQ2xhc3NlcyA9IGVsLl9feF9vcmlnaW5hbF9jbGFzc2VzIHx8IFtdO1xuICAgIGNvbnN0IHN0YWdlcyA9IHtcbiAgICAgIHN0YXJ0KCkge1xuICAgICAgICBlbC5jbGFzc0xpc3QuYWRkKC4uLmNsYXNzZXNTdGFydCk7XG4gICAgICB9LFxuXG4gICAgICBkdXJpbmcoKSB7XG4gICAgICAgIGVsLmNsYXNzTGlzdC5hZGQoLi4uY2xhc3Nlc0R1cmluZyk7XG4gICAgICB9LFxuXG4gICAgICBzaG93KCkge1xuICAgICAgICBob29rMSgpO1xuICAgICAgfSxcblxuICAgICAgZW5kKCkge1xuICAgICAgICAvLyBEb24ndCByZW1vdmUgY2xhc3NlcyB0aGF0IHdlcmUgaW4gdGhlIG9yaWdpbmFsIGNsYXNzIGF0dHJpYnV0ZS5cbiAgICAgICAgZWwuY2xhc3NMaXN0LnJlbW92ZSguLi5jbGFzc2VzU3RhcnQuZmlsdGVyKGkgPT4gIW9yaWdpbmFsQ2xhc3Nlcy5pbmNsdWRlcyhpKSkpO1xuICAgICAgICBlbC5jbGFzc0xpc3QuYWRkKC4uLmNsYXNzZXNFbmQpO1xuICAgICAgfSxcblxuICAgICAgaGlkZSgpIHtcbiAgICAgICAgaG9vazIoKTtcbiAgICAgIH0sXG5cbiAgICAgIGNsZWFudXAoKSB7XG4gICAgICAgIGVsLmNsYXNzTGlzdC5yZW1vdmUoLi4uY2xhc3Nlc0R1cmluZy5maWx0ZXIoaSA9PiAhb3JpZ2luYWxDbGFzc2VzLmluY2x1ZGVzKGkpKSk7XG4gICAgICAgIGVsLmNsYXNzTGlzdC5yZW1vdmUoLi4uY2xhc3Nlc0VuZC5maWx0ZXIoaSA9PiAhb3JpZ2luYWxDbGFzc2VzLmluY2x1ZGVzKGkpKSk7XG4gICAgICB9XG5cbiAgICB9O1xuICAgIHRyYW5zaXRpb24oZWwsIHN0YWdlcywgdHlwZSwgcmVqZWN0KTtcbiAgfVxuICBmdW5jdGlvbiB0cmFuc2l0aW9uKGVsLCBzdGFnZXMsIHR5cGUsIHJlamVjdCkge1xuICAgIGNvbnN0IGZpbmlzaCA9IG9uY2UoKCkgPT4ge1xuICAgICAgc3RhZ2VzLmhpZGUoKTsgLy8gQWRkaW5nIGFuIFwiaXNDb25uZWN0ZWRcIiBjaGVjaywgaW4gY2FzZSB0aGUgY2FsbGJhY2tcbiAgICAgIC8vIHJlbW92ZWQgdGhlIGVsZW1lbnQgZnJvbSB0aGUgRE9NLlxuXG4gICAgICBpZiAoZWwuaXNDb25uZWN0ZWQpIHtcbiAgICAgICAgc3RhZ2VzLmNsZWFudXAoKTtcbiAgICAgIH1cblxuICAgICAgZGVsZXRlIGVsLl9feF90cmFuc2l0aW9uO1xuICAgIH0pO1xuICAgIGVsLl9feF90cmFuc2l0aW9uID0ge1xuICAgICAgLy8gU2V0IHRyYW5zaXRpb24gdHlwZSBzbyB3ZSBjYW4gYXZvaWQgY2xlYXJpbmcgdHJhbnNpdGlvbiBpZiB0aGUgZGlyZWN0aW9uIGlzIHRoZSBzYW1lXG4gICAgICB0eXBlOiB0eXBlLFxuICAgICAgLy8gY3JlYXRlIGEgY2FsbGJhY2sgZm9yIHRoZSBsYXN0IHN0YWdlcyBvZiB0aGUgdHJhbnNpdGlvbiBzbyB3ZSBjYW4gY2FsbCBpdFxuICAgICAgLy8gZnJvbSBkaWZmZXJlbnQgcG9pbnQgYW5kIGVhcmx5IHRlcm1pbmF0ZSBpdC4gT25jZSB3aWxsIGVuc3VyZSB0aGF0IGZ1bmN0aW9uXG4gICAgICAvLyBpcyBvbmx5IGNhbGxlZCBvbmUgdGltZS5cbiAgICAgIGNhbmNlbDogb25jZSgoKSA9PiB7XG4gICAgICAgIHJlamVjdChUUkFOU0lUSU9OX0NBTkNFTExFRCk7XG4gICAgICAgIGZpbmlzaCgpO1xuICAgICAgfSksXG4gICAgICBmaW5pc2gsXG4gICAgICAvLyBUaGlzIHN0b3JlIHRoZSBuZXh0IGFuaW1hdGlvbiBmcmFtZSBzbyB3ZSBjYW4gY2FuY2VsIGl0XG4gICAgICBuZXh0RnJhbWU6IG51bGxcbiAgICB9O1xuICAgIHN0YWdlcy5zdGFydCgpO1xuICAgIHN0YWdlcy5kdXJpbmcoKTtcbiAgICBlbC5fX3hfdHJhbnNpdGlvbi5uZXh0RnJhbWUgPSByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoKCkgPT4ge1xuICAgICAgLy8gTm90ZTogU2FmYXJpJ3MgdHJhbnNpdGlvbkR1cmF0aW9uIHByb3BlcnR5IHdpbGwgbGlzdCBvdXQgY29tbWEgc2VwYXJhdGVkIHRyYW5zaXRpb24gZHVyYXRpb25zXG4gICAgICAvLyBmb3IgZXZlcnkgc2luZ2xlIHRyYW5zaXRpb24gcHJvcGVydHkuIExldCdzIGdyYWIgdGhlIGZpcnN0IG9uZSBhbmQgY2FsbCBpdCBhIGRheS5cbiAgICAgIGxldCBkdXJhdGlvbiA9IE51bWJlcihnZXRDb21wdXRlZFN0eWxlKGVsKS50cmFuc2l0aW9uRHVyYXRpb24ucmVwbGFjZSgvLC4qLywgJycpLnJlcGxhY2UoJ3MnLCAnJykpICogMTAwMDtcblxuICAgICAgaWYgKGR1cmF0aW9uID09PSAwKSB7XG4gICAgICAgIGR1cmF0aW9uID0gTnVtYmVyKGdldENvbXB1dGVkU3R5bGUoZWwpLmFuaW1hdGlvbkR1cmF0aW9uLnJlcGxhY2UoJ3MnLCAnJykpICogMTAwMDtcbiAgICAgIH1cblxuICAgICAgc3RhZ2VzLnNob3coKTtcbiAgICAgIGVsLl9feF90cmFuc2l0aW9uLm5leHRGcmFtZSA9IHJlcXVlc3RBbmltYXRpb25GcmFtZSgoKSA9PiB7XG4gICAgICAgIHN0YWdlcy5lbmQoKTtcbiAgICAgICAgc2V0VGltZW91dChlbC5fX3hfdHJhbnNpdGlvbi5maW5pc2gsIGR1cmF0aW9uKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG4gIGZ1bmN0aW9uIGlzTnVtZXJpYyhzdWJqZWN0KSB7XG4gICAgcmV0dXJuICFBcnJheS5pc0FycmF5KHN1YmplY3QpICYmICFpc05hTihzdWJqZWN0KTtcbiAgfSAvLyBUaGFua3MgQHZ1ZWpzXG4gIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS92dWVqcy92dWUvYmxvYi80ZGU0NjQ5ZDk2MzcyNjJhOWIwMDc3MjBiNTlmODBhYzcyYTU2MjBjL3NyYy9zaGFyZWQvdXRpbC5qc1xuXG4gIGZ1bmN0aW9uIG9uY2UoY2FsbGJhY2spIHtcbiAgICBsZXQgY2FsbGVkID0gZmFsc2U7XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICghY2FsbGVkKSB7XG4gICAgICAgIGNhbGxlZCA9IHRydWU7XG4gICAgICAgIGNhbGxiYWNrLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgICB9XG4gICAgfTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGhhbmRsZUZvckRpcmVjdGl2ZShjb21wb25lbnQsIHRlbXBsYXRlRWwsIGV4cHJlc3Npb24sIGluaXRpYWxVcGRhdGUsIGV4dHJhVmFycykge1xuICAgIHdhcm5JZk1hbGZvcm1lZFRlbXBsYXRlKHRlbXBsYXRlRWwsICd4LWZvcicpO1xuICAgIGxldCBpdGVyYXRvck5hbWVzID0gdHlwZW9mIGV4cHJlc3Npb24gPT09ICdmdW5jdGlvbicgPyBwYXJzZUZvckV4cHJlc3Npb24oY29tcG9uZW50LmV2YWx1YXRlUmV0dXJuRXhwcmVzc2lvbih0ZW1wbGF0ZUVsLCBleHByZXNzaW9uKSkgOiBwYXJzZUZvckV4cHJlc3Npb24oZXhwcmVzc2lvbik7XG4gICAgbGV0IGl0ZW1zID0gZXZhbHVhdGVJdGVtc0FuZFJldHVybkVtcHR5SWZYSWZJc1ByZXNlbnRBbmRGYWxzZU9uRWxlbWVudChjb21wb25lbnQsIHRlbXBsYXRlRWwsIGl0ZXJhdG9yTmFtZXMsIGV4dHJhVmFycyk7IC8vIEFzIHdlIHdhbGsgdGhlIGFycmF5LCB3ZSdsbCBhbHNvIHdhbGsgdGhlIERPTSAodXBkYXRpbmcvY3JlYXRpbmcgYXMgd2UgZ28pLlxuXG4gICAgbGV0IGN1cnJlbnRFbCA9IHRlbXBsYXRlRWw7XG4gICAgaXRlbXMuZm9yRWFjaCgoaXRlbSwgaW5kZXgpID0+IHtcbiAgICAgIGxldCBpdGVyYXRpb25TY29wZVZhcmlhYmxlcyA9IGdldEl0ZXJhdGlvblNjb3BlVmFyaWFibGVzKGl0ZXJhdG9yTmFtZXMsIGl0ZW0sIGluZGV4LCBpdGVtcywgZXh0cmFWYXJzKCkpO1xuICAgICAgbGV0IGN1cnJlbnRLZXkgPSBnZW5lcmF0ZUtleUZvckl0ZXJhdGlvbihjb21wb25lbnQsIHRlbXBsYXRlRWwsIGluZGV4LCBpdGVyYXRpb25TY29wZVZhcmlhYmxlcyk7XG4gICAgICBsZXQgbmV4dEVsID0gbG9va0FoZWFkRm9yTWF0Y2hpbmdLZXllZEVsZW1lbnRBbmRNb3ZlSXRJZkZvdW5kKGN1cnJlbnRFbC5uZXh0RWxlbWVudFNpYmxpbmcsIGN1cnJlbnRLZXkpOyAvLyBJZiB3ZSBoYXZlbid0IGZvdW5kIGEgbWF0Y2hpbmcga2V5LCBpbnNlcnQgdGhlIGVsZW1lbnQgYXQgdGhlIGN1cnJlbnQgcG9zaXRpb24uXG5cbiAgICAgIGlmICghbmV4dEVsKSB7XG4gICAgICAgIG5leHRFbCA9IGFkZEVsZW1lbnRJbkxvb3BBZnRlckN1cnJlbnRFbCh0ZW1wbGF0ZUVsLCBjdXJyZW50RWwpOyAvLyBBbmQgdHJhbnNpdGlvbiBpdCBpbiBpZiBpdCdzIG5vdCB0aGUgZmlyc3QgcGFnZSBsb2FkLlxuXG4gICAgICAgIHRyYW5zaXRpb25JbihuZXh0RWwsICgpID0+IHt9LCAoKSA9PiB7fSwgY29tcG9uZW50LCBpbml0aWFsVXBkYXRlKTtcbiAgICAgICAgbmV4dEVsLl9feF9mb3IgPSBpdGVyYXRpb25TY29wZVZhcmlhYmxlcztcbiAgICAgICAgY29tcG9uZW50LmluaXRpYWxpemVFbGVtZW50cyhuZXh0RWwsICgpID0+IG5leHRFbC5fX3hfZm9yKTsgLy8gT3RoZXJ3aXNlIHVwZGF0ZSB0aGUgZWxlbWVudCB3ZSBmb3VuZC5cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIFRlbXBvcmFyaWx5IHJlbW92ZSB0aGUga2V5IGluZGljYXRvciB0byBhbGxvdyB0aGUgbm9ybWFsIFwidXBkYXRlRWxlbWVudHNcIiB0byB3b3JrLlxuICAgICAgICBkZWxldGUgbmV4dEVsLl9feF9mb3Jfa2V5O1xuICAgICAgICBuZXh0RWwuX194X2ZvciA9IGl0ZXJhdGlvblNjb3BlVmFyaWFibGVzO1xuICAgICAgICBjb21wb25lbnQudXBkYXRlRWxlbWVudHMobmV4dEVsLCAoKSA9PiBuZXh0RWwuX194X2Zvcik7XG4gICAgICB9XG5cbiAgICAgIGN1cnJlbnRFbCA9IG5leHRFbDtcbiAgICAgIGN1cnJlbnRFbC5fX3hfZm9yX2tleSA9IGN1cnJlbnRLZXk7XG4gICAgfSk7XG4gICAgcmVtb3ZlQW55TGVmdE92ZXJFbGVtZW50c0Zyb21QcmV2aW91c1VwZGF0ZShjdXJyZW50RWwsIGNvbXBvbmVudCk7XG4gIH0gLy8gVGhpcyB3YXMgdGFrZW4gZnJvbSBWdWVKUyAyLiogY29yZS4gVGhhbmtzIFZ1ZSFcblxuICBmdW5jdGlvbiBwYXJzZUZvckV4cHJlc3Npb24oZXhwcmVzc2lvbikge1xuICAgIGxldCBmb3JJdGVyYXRvclJFID0gLywoW14sXFx9XFxdXSopKD86LChbXixcXH1cXF1dKikpPyQvO1xuICAgIGxldCBzdHJpcFBhcmVuc1JFID0gL15cXCh8XFwpJC9nO1xuICAgIGxldCBmb3JBbGlhc1JFID0gLyhbXFxzXFxTXSo/KVxccysoPzppbnxvZilcXHMrKFtcXHNcXFNdKikvO1xuICAgIGxldCBpbk1hdGNoID0gU3RyaW5nKGV4cHJlc3Npb24pLm1hdGNoKGZvckFsaWFzUkUpO1xuICAgIGlmICghaW5NYXRjaCkgcmV0dXJuO1xuICAgIGxldCByZXMgPSB7fTtcbiAgICByZXMuaXRlbXMgPSBpbk1hdGNoWzJdLnRyaW0oKTtcbiAgICBsZXQgaXRlbSA9IGluTWF0Y2hbMV0udHJpbSgpLnJlcGxhY2Uoc3RyaXBQYXJlbnNSRSwgJycpO1xuICAgIGxldCBpdGVyYXRvck1hdGNoID0gaXRlbS5tYXRjaChmb3JJdGVyYXRvclJFKTtcblxuICAgIGlmIChpdGVyYXRvck1hdGNoKSB7XG4gICAgICByZXMuaXRlbSA9IGl0ZW0ucmVwbGFjZShmb3JJdGVyYXRvclJFLCAnJykudHJpbSgpO1xuICAgICAgcmVzLmluZGV4ID0gaXRlcmF0b3JNYXRjaFsxXS50cmltKCk7XG5cbiAgICAgIGlmIChpdGVyYXRvck1hdGNoWzJdKSB7XG4gICAgICAgIHJlcy5jb2xsZWN0aW9uID0gaXRlcmF0b3JNYXRjaFsyXS50cmltKCk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlcy5pdGVtID0gaXRlbTtcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzO1xuICB9XG5cbiAgZnVuY3Rpb24gZ2V0SXRlcmF0aW9uU2NvcGVWYXJpYWJsZXMoaXRlcmF0b3JOYW1lcywgaXRlbSwgaW5kZXgsIGl0ZW1zLCBleHRyYVZhcnMpIHtcbiAgICAvLyBXZSBtdXN0IGNyZWF0ZSBhIG5ldyBvYmplY3QsIHNvIGVhY2ggaXRlcmF0aW9uIGhhcyBhIG5ldyBzY29wZVxuICAgIGxldCBzY29wZVZhcmlhYmxlcyA9IGV4dHJhVmFycyA/IF9vYmplY3RTcHJlYWQyKHt9LCBleHRyYVZhcnMpIDoge307XG4gICAgc2NvcGVWYXJpYWJsZXNbaXRlcmF0b3JOYW1lcy5pdGVtXSA9IGl0ZW07XG4gICAgaWYgKGl0ZXJhdG9yTmFtZXMuaW5kZXgpIHNjb3BlVmFyaWFibGVzW2l0ZXJhdG9yTmFtZXMuaW5kZXhdID0gaW5kZXg7XG4gICAgaWYgKGl0ZXJhdG9yTmFtZXMuY29sbGVjdGlvbikgc2NvcGVWYXJpYWJsZXNbaXRlcmF0b3JOYW1lcy5jb2xsZWN0aW9uXSA9IGl0ZW1zO1xuICAgIHJldHVybiBzY29wZVZhcmlhYmxlcztcbiAgfVxuXG4gIGZ1bmN0aW9uIGdlbmVyYXRlS2V5Rm9ySXRlcmF0aW9uKGNvbXBvbmVudCwgZWwsIGluZGV4LCBpdGVyYXRpb25TY29wZVZhcmlhYmxlcykge1xuICAgIGxldCBiaW5kS2V5QXR0cmlidXRlID0gZ2V0WEF0dHJzKGVsLCBjb21wb25lbnQsICdiaW5kJykuZmlsdGVyKGF0dHIgPT4gYXR0ci52YWx1ZSA9PT0gJ2tleScpWzBdOyAvLyBJZiB0aGUgZGV2IGhhc24ndCBzcGVjaWZpZWQgYSBrZXksIGp1c3QgcmV0dXJuIHRoZSBpbmRleCBvZiB0aGUgaXRlcmF0aW9uLlxuXG4gICAgaWYgKCFiaW5kS2V5QXR0cmlidXRlKSByZXR1cm4gaW5kZXg7XG4gICAgcmV0dXJuIGNvbXBvbmVudC5ldmFsdWF0ZVJldHVybkV4cHJlc3Npb24oZWwsIGJpbmRLZXlBdHRyaWJ1dGUuZXhwcmVzc2lvbiwgKCkgPT4gaXRlcmF0aW9uU2NvcGVWYXJpYWJsZXMpO1xuICB9XG5cbiAgZnVuY3Rpb24gZXZhbHVhdGVJdGVtc0FuZFJldHVybkVtcHR5SWZYSWZJc1ByZXNlbnRBbmRGYWxzZU9uRWxlbWVudChjb21wb25lbnQsIGVsLCBpdGVyYXRvck5hbWVzLCBleHRyYVZhcnMpIHtcbiAgICBsZXQgaWZBdHRyaWJ1dGUgPSBnZXRYQXR0cnMoZWwsIGNvbXBvbmVudCwgJ2lmJylbMF07XG5cbiAgICBpZiAoaWZBdHRyaWJ1dGUgJiYgIWNvbXBvbmVudC5ldmFsdWF0ZVJldHVybkV4cHJlc3Npb24oZWwsIGlmQXR0cmlidXRlLmV4cHJlc3Npb24pKSB7XG4gICAgICByZXR1cm4gW107XG4gICAgfVxuXG4gICAgbGV0IGl0ZW1zID0gY29tcG9uZW50LmV2YWx1YXRlUmV0dXJuRXhwcmVzc2lvbihlbCwgaXRlcmF0b3JOYW1lcy5pdGVtcywgZXh0cmFWYXJzKTsgLy8gVGhpcyBhZGRzIHN1cHBvcnQgZm9yIHRoZSBgaSBpbiBuYCBzeW50YXguXG5cbiAgICBpZiAoaXNOdW1lcmljKGl0ZW1zKSAmJiBpdGVtcyA+PSAwKSB7XG4gICAgICBpdGVtcyA9IEFycmF5LmZyb20oQXJyYXkoaXRlbXMpLmtleXMoKSwgaSA9PiBpICsgMSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGl0ZW1zO1xuICB9XG5cbiAgZnVuY3Rpb24gYWRkRWxlbWVudEluTG9vcEFmdGVyQ3VycmVudEVsKHRlbXBsYXRlRWwsIGN1cnJlbnRFbCkge1xuICAgIGxldCBjbG9uZSA9IGRvY3VtZW50LmltcG9ydE5vZGUodGVtcGxhdGVFbC5jb250ZW50LCB0cnVlKTtcbiAgICBjdXJyZW50RWwucGFyZW50RWxlbWVudC5pbnNlcnRCZWZvcmUoY2xvbmUsIGN1cnJlbnRFbC5uZXh0RWxlbWVudFNpYmxpbmcpO1xuICAgIHJldHVybiBjdXJyZW50RWwubmV4dEVsZW1lbnRTaWJsaW5nO1xuICB9XG5cbiAgZnVuY3Rpb24gbG9va0FoZWFkRm9yTWF0Y2hpbmdLZXllZEVsZW1lbnRBbmRNb3ZlSXRJZkZvdW5kKG5leHRFbCwgY3VycmVudEtleSkge1xuICAgIGlmICghbmV4dEVsKSByZXR1cm47IC8vIElmIHdlIGFyZSBhbHJlYWR5IHBhc3QgdGhlIHgtZm9yIGdlbmVyYXRlZCBlbGVtZW50cywgd2UgZG9uJ3QgbmVlZCB0byBsb29rIGFoZWFkLlxuXG4gICAgaWYgKG5leHRFbC5fX3hfZm9yX2tleSA9PT0gdW5kZWZpbmVkKSByZXR1cm47IC8vIElmIHRoZSB0aGUga2V5J3MgRE8gbWF0Y2gsIG5vIG5lZWQgdG8gbG9vayBhaGVhZC5cblxuICAgIGlmIChuZXh0RWwuX194X2Zvcl9rZXkgPT09IGN1cnJlbnRLZXkpIHJldHVybiBuZXh0RWw7IC8vIElmIHRoZXkgZG9uJ3QsIHdlJ2xsIGxvb2sgYWhlYWQgZm9yIGEgbWF0Y2guXG4gICAgLy8gSWYgd2UgZmluZCBpdCwgd2UnbGwgbW92ZSBpdCB0byB0aGUgY3VycmVudCBwb3NpdGlvbiBpbiB0aGUgbG9vcC5cblxuICAgIGxldCB0bXBOZXh0RWwgPSBuZXh0RWw7XG5cbiAgICB3aGlsZSAodG1wTmV4dEVsKSB7XG4gICAgICBpZiAodG1wTmV4dEVsLl9feF9mb3Jfa2V5ID09PSBjdXJyZW50S2V5KSB7XG4gICAgICAgIHJldHVybiB0bXBOZXh0RWwucGFyZW50RWxlbWVudC5pbnNlcnRCZWZvcmUodG1wTmV4dEVsLCBuZXh0RWwpO1xuICAgICAgfVxuXG4gICAgICB0bXBOZXh0RWwgPSB0bXBOZXh0RWwubmV4dEVsZW1lbnRTaWJsaW5nICYmIHRtcE5leHRFbC5uZXh0RWxlbWVudFNpYmxpbmcuX194X2Zvcl9rZXkgIT09IHVuZGVmaW5lZCA/IHRtcE5leHRFbC5uZXh0RWxlbWVudFNpYmxpbmcgOiBmYWxzZTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiByZW1vdmVBbnlMZWZ0T3ZlckVsZW1lbnRzRnJvbVByZXZpb3VzVXBkYXRlKGN1cnJlbnRFbCwgY29tcG9uZW50KSB7XG4gICAgdmFyIG5leHRFbGVtZW50RnJvbU9sZExvb3AgPSBjdXJyZW50RWwubmV4dEVsZW1lbnRTaWJsaW5nICYmIGN1cnJlbnRFbC5uZXh0RWxlbWVudFNpYmxpbmcuX194X2Zvcl9rZXkgIT09IHVuZGVmaW5lZCA/IGN1cnJlbnRFbC5uZXh0RWxlbWVudFNpYmxpbmcgOiBmYWxzZTtcblxuICAgIHdoaWxlIChuZXh0RWxlbWVudEZyb21PbGRMb29wKSB7XG4gICAgICBsZXQgbmV4dEVsZW1lbnRGcm9tT2xkTG9vcEltbXV0YWJsZSA9IG5leHRFbGVtZW50RnJvbU9sZExvb3A7XG4gICAgICBsZXQgbmV4dFNpYmxpbmcgPSBuZXh0RWxlbWVudEZyb21PbGRMb29wLm5leHRFbGVtZW50U2libGluZztcbiAgICAgIHRyYW5zaXRpb25PdXQobmV4dEVsZW1lbnRGcm9tT2xkTG9vcCwgKCkgPT4ge1xuICAgICAgICBuZXh0RWxlbWVudEZyb21PbGRMb29wSW1tdXRhYmxlLnJlbW92ZSgpO1xuICAgICAgfSwgKCkgPT4ge30sIGNvbXBvbmVudCk7XG4gICAgICBuZXh0RWxlbWVudEZyb21PbGRMb29wID0gbmV4dFNpYmxpbmcgJiYgbmV4dFNpYmxpbmcuX194X2Zvcl9rZXkgIT09IHVuZGVmaW5lZCA/IG5leHRTaWJsaW5nIDogZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gaGFuZGxlQXR0cmlidXRlQmluZGluZ0RpcmVjdGl2ZShjb21wb25lbnQsIGVsLCBhdHRyTmFtZSwgZXhwcmVzc2lvbiwgZXh0cmFWYXJzLCBhdHRyVHlwZSwgbW9kaWZpZXJzKSB7XG4gICAgdmFyIHZhbHVlID0gY29tcG9uZW50LmV2YWx1YXRlUmV0dXJuRXhwcmVzc2lvbihlbCwgZXhwcmVzc2lvbiwgZXh0cmFWYXJzKTtcblxuICAgIGlmIChhdHRyTmFtZSA9PT0gJ3ZhbHVlJykge1xuICAgICAgaWYgKEFscGluZS5pZ25vcmVGb2N1c2VkRm9yVmFsdWVCaW5kaW5nICYmIGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQuaXNTYW1lTm9kZShlbCkpIHJldHVybjsgLy8gSWYgbmVzdGVkIG1vZGVsIGtleSBpcyB1bmRlZmluZWQsIHNldCB0aGUgZGVmYXVsdCB2YWx1ZSB0byBlbXB0eSBzdHJpbmcuXG5cbiAgICAgIGlmICh2YWx1ZSA9PT0gdW5kZWZpbmVkICYmIFN0cmluZyhleHByZXNzaW9uKS5tYXRjaCgvXFwuLykpIHtcbiAgICAgICAgdmFsdWUgPSAnJztcbiAgICAgIH1cblxuICAgICAgaWYgKGVsLnR5cGUgPT09ICdyYWRpbycpIHtcbiAgICAgICAgLy8gU2V0IHJhZGlvIHZhbHVlIGZyb20geC1iaW5kOnZhbHVlLCBpZiBubyBcInZhbHVlXCIgYXR0cmlidXRlIGV4aXN0cy5cbiAgICAgICAgLy8gSWYgdGhlcmUgYXJlIGFueSBpbml0aWFsIHN0YXRlIHZhbHVlcywgcmFkaW8gd2lsbCBoYXZlIGEgY29ycmVjdFxuICAgICAgICAvLyBcImNoZWNrZWRcIiB2YWx1ZSBzaW5jZSB4LWJpbmQ6dmFsdWUgaXMgcHJvY2Vzc2VkIGJlZm9yZSB4LW1vZGVsLlxuICAgICAgICBpZiAoZWwuYXR0cmlidXRlcy52YWx1ZSA9PT0gdW5kZWZpbmVkICYmIGF0dHJUeXBlID09PSAnYmluZCcpIHtcbiAgICAgICAgICBlbC52YWx1ZSA9IHZhbHVlO1xuICAgICAgICB9IGVsc2UgaWYgKGF0dHJUeXBlICE9PSAnYmluZCcpIHtcbiAgICAgICAgICBlbC5jaGVja2VkID0gY2hlY2tlZEF0dHJMb29zZUNvbXBhcmUoZWwudmFsdWUsIHZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChlbC50eXBlID09PSAnY2hlY2tib3gnKSB7XG4gICAgICAgIC8vIElmIHdlIGFyZSBleHBsaWNpdGx5IGJpbmRpbmcgYSBzdHJpbmcgdG8gdGhlIDp2YWx1ZSwgc2V0IHRoZSBzdHJpbmcsXG4gICAgICAgIC8vIElmIHRoZSB2YWx1ZSBpcyBhIGJvb2xlYW4sIGxlYXZlIGl0IGFsb25lLCBpdCB3aWxsIGJlIHNldCB0byBcIm9uXCJcbiAgICAgICAgLy8gYXV0b21hdGljYWxseS5cbiAgICAgICAgaWYgKHR5cGVvZiB2YWx1ZSAhPT0gJ2Jvb2xlYW4nICYmICFbbnVsbCwgdW5kZWZpbmVkXS5pbmNsdWRlcyh2YWx1ZSkgJiYgYXR0clR5cGUgPT09ICdiaW5kJykge1xuICAgICAgICAgIGVsLnZhbHVlID0gU3RyaW5nKHZhbHVlKTtcbiAgICAgICAgfSBlbHNlIGlmIChhdHRyVHlwZSAhPT0gJ2JpbmQnKSB7XG4gICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4gICAgICAgICAgICAvLyBJJ20gcHVycG9zZWx5IG5vdCB1c2luZyBBcnJheS5pbmNsdWRlcyBoZXJlIGJlY2F1c2UgaXQnc1xuICAgICAgICAgICAgLy8gc3RyaWN0LCBhbmQgYmVjYXVzZSBvZiBOdW1lcmljL1N0cmluZyBtaXMtY2FzdGluZywgSVxuICAgICAgICAgICAgLy8gd2FudCB0aGUgXCJpbmNsdWRlc1wiIHRvIGJlIFwiZnV6enlcIi5cbiAgICAgICAgICAgIGVsLmNoZWNrZWQgPSB2YWx1ZS5zb21lKHZhbCA9PiBjaGVja2VkQXR0ckxvb3NlQ29tcGFyZSh2YWwsIGVsLnZhbHVlKSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGVsLmNoZWNrZWQgPSAhIXZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChlbC50YWdOYW1lID09PSAnU0VMRUNUJykge1xuICAgICAgICB1cGRhdGVTZWxlY3QoZWwsIHZhbHVlKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChlbC52YWx1ZSA9PT0gdmFsdWUpIHJldHVybjtcbiAgICAgICAgZWwudmFsdWUgPSB2YWx1ZTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKGF0dHJOYW1lID09PSAnY2xhc3MnKSB7XG4gICAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcbiAgICAgICAgY29uc3Qgb3JpZ2luYWxDbGFzc2VzID0gZWwuX194X29yaWdpbmFsX2NsYXNzZXMgfHwgW107XG4gICAgICAgIGVsLnNldEF0dHJpYnV0ZSgnY2xhc3MnLCBhcnJheVVuaXF1ZShvcmlnaW5hbENsYXNzZXMuY29uY2F0KHZhbHVlKSkuam9pbignICcpKTtcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0Jykge1xuICAgICAgICAvLyBTb3J0aW5nIHRoZSBrZXlzIC8gY2xhc3MgbmFtZXMgYnkgdGhlaXIgYm9vbGVhbiB2YWx1ZSB3aWxsIGVuc3VyZSB0aGF0XG4gICAgICAgIC8vIGFueXRoaW5nIHRoYXQgZXZhbHVhdGVzIHRvIGBmYWxzZWAgYW5kIG5lZWRzIHRvIHJlbW92ZSBjbGFzc2VzIGlzIHJ1biBmaXJzdC5cbiAgICAgICAgY29uc3Qga2V5c1NvcnRlZEJ5Qm9vbGVhblZhbHVlID0gT2JqZWN0LmtleXModmFsdWUpLnNvcnQoKGEsIGIpID0+IHZhbHVlW2FdIC0gdmFsdWVbYl0pO1xuICAgICAgICBrZXlzU29ydGVkQnlCb29sZWFuVmFsdWUuZm9yRWFjaChjbGFzc05hbWVzID0+IHtcbiAgICAgICAgICBpZiAodmFsdWVbY2xhc3NOYW1lc10pIHtcbiAgICAgICAgICAgIGNvbnZlcnRDbGFzc1N0cmluZ1RvQXJyYXkoY2xhc3NOYW1lcykuZm9yRWFjaChjbGFzc05hbWUgPT4gZWwuY2xhc3NMaXN0LmFkZChjbGFzc05hbWUpKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29udmVydENsYXNzU3RyaW5nVG9BcnJheShjbGFzc05hbWVzKS5mb3JFYWNoKGNsYXNzTmFtZSA9PiBlbC5jbGFzc0xpc3QucmVtb3ZlKGNsYXNzTmFtZSkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCBvcmlnaW5hbENsYXNzZXMgPSBlbC5fX3hfb3JpZ2luYWxfY2xhc3NlcyB8fCBbXTtcbiAgICAgICAgY29uc3QgbmV3Q2xhc3NlcyA9IHZhbHVlID8gY29udmVydENsYXNzU3RyaW5nVG9BcnJheSh2YWx1ZSkgOiBbXTtcbiAgICAgICAgZWwuc2V0QXR0cmlidXRlKCdjbGFzcycsIGFycmF5VW5pcXVlKG9yaWdpbmFsQ2xhc3Nlcy5jb25jYXQobmV3Q2xhc3NlcykpLmpvaW4oJyAnKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGF0dHJOYW1lID0gbW9kaWZpZXJzLmluY2x1ZGVzKCdjYW1lbCcpID8gY2FtZWxDYXNlKGF0dHJOYW1lKSA6IGF0dHJOYW1lOyAvLyBJZiBhbiBhdHRyaWJ1dGUncyBib3VuZCB2YWx1ZSBpcyBudWxsLCB1bmRlZmluZWQgb3IgZmFsc2UsIHJlbW92ZSB0aGUgYXR0cmlidXRlXG5cbiAgICAgIGlmIChbbnVsbCwgdW5kZWZpbmVkLCBmYWxzZV0uaW5jbHVkZXModmFsdWUpKSB7XG4gICAgICAgIGVsLnJlbW92ZUF0dHJpYnV0ZShhdHRyTmFtZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpc0Jvb2xlYW5BdHRyKGF0dHJOYW1lKSA/IHNldElmQ2hhbmdlZChlbCwgYXR0ck5hbWUsIGF0dHJOYW1lKSA6IHNldElmQ2hhbmdlZChlbCwgYXR0ck5hbWUsIHZhbHVlKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBzZXRJZkNoYW5nZWQoZWwsIGF0dHJOYW1lLCB2YWx1ZSkge1xuICAgIGlmIChlbC5nZXRBdHRyaWJ1dGUoYXR0ck5hbWUpICE9IHZhbHVlKSB7XG4gICAgICBlbC5zZXRBdHRyaWJ1dGUoYXR0ck5hbWUsIHZhbHVlKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiB1cGRhdGVTZWxlY3QoZWwsIHZhbHVlKSB7XG4gICAgY29uc3QgYXJyYXlXcmFwcGVkVmFsdWUgPSBbXS5jb25jYXQodmFsdWUpLm1hcCh2YWx1ZSA9PiB7XG4gICAgICByZXR1cm4gdmFsdWUgKyAnJztcbiAgICB9KTtcbiAgICBBcnJheS5mcm9tKGVsLm9wdGlvbnMpLmZvckVhY2gob3B0aW9uID0+IHtcbiAgICAgIG9wdGlvbi5zZWxlY3RlZCA9IGFycmF5V3JhcHBlZFZhbHVlLmluY2x1ZGVzKG9wdGlvbi52YWx1ZSB8fCBvcHRpb24udGV4dCk7XG4gICAgfSk7XG4gIH1cblxuICBmdW5jdGlvbiBoYW5kbGVUZXh0RGlyZWN0aXZlKGVsLCBvdXRwdXQsIGV4cHJlc3Npb24pIHtcbiAgICAvLyBJZiBuZXN0ZWQgbW9kZWwga2V5IGlzIHVuZGVmaW5lZCwgc2V0IHRoZSBkZWZhdWx0IHZhbHVlIHRvIGVtcHR5IHN0cmluZy5cbiAgICBpZiAob3V0cHV0ID09PSB1bmRlZmluZWQgJiYgU3RyaW5nKGV4cHJlc3Npb24pLm1hdGNoKC9cXC4vKSkge1xuICAgICAgb3V0cHV0ID0gJyc7XG4gICAgfVxuXG4gICAgZWwudGV4dENvbnRlbnQgPSBvdXRwdXQ7XG4gIH1cblxuICBmdW5jdGlvbiBoYW5kbGVIdG1sRGlyZWN0aXZlKGNvbXBvbmVudCwgZWwsIGV4cHJlc3Npb24sIGV4dHJhVmFycykge1xuICAgIGVsLmlubmVySFRNTCA9IGNvbXBvbmVudC5ldmFsdWF0ZVJldHVybkV4cHJlc3Npb24oZWwsIGV4cHJlc3Npb24sIGV4dHJhVmFycyk7XG4gIH1cblxuICBmdW5jdGlvbiBoYW5kbGVTaG93RGlyZWN0aXZlKGNvbXBvbmVudCwgZWwsIHZhbHVlLCBtb2RpZmllcnMsIGluaXRpYWxVcGRhdGUgPSBmYWxzZSkge1xuICAgIGNvbnN0IGhpZGUgPSAoKSA9PiB7XG4gICAgICBlbC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgICAgZWwuX194X2lzX3Nob3duID0gZmFsc2U7XG4gICAgfTtcblxuICAgIGNvbnN0IHNob3cgPSAoKSA9PiB7XG4gICAgICBpZiAoZWwuc3R5bGUubGVuZ3RoID09PSAxICYmIGVsLnN0eWxlLmRpc3BsYXkgPT09ICdub25lJykge1xuICAgICAgICBlbC5yZW1vdmVBdHRyaWJ1dGUoJ3N0eWxlJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBlbC5zdHlsZS5yZW1vdmVQcm9wZXJ0eSgnZGlzcGxheScpO1xuICAgICAgfVxuXG4gICAgICBlbC5fX3hfaXNfc2hvd24gPSB0cnVlO1xuICAgIH07XG5cbiAgICBpZiAoaW5pdGlhbFVwZGF0ZSA9PT0gdHJ1ZSkge1xuICAgICAgaWYgKHZhbHVlKSB7XG4gICAgICAgIHNob3coKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGhpZGUoKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IGhhbmRsZSA9IChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGlmICh2YWx1ZSkge1xuICAgICAgICBpZiAoZWwuc3R5bGUuZGlzcGxheSA9PT0gJ25vbmUnIHx8IGVsLl9feF90cmFuc2l0aW9uKSB7XG4gICAgICAgICAgdHJhbnNpdGlvbkluKGVsLCAoKSA9PiB7XG4gICAgICAgICAgICBzaG93KCk7XG4gICAgICAgICAgfSwgcmVqZWN0LCBjb21wb25lbnQpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVzb2x2ZSgoKSA9PiB7fSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoZWwuc3R5bGUuZGlzcGxheSAhPT0gJ25vbmUnKSB7XG4gICAgICAgICAgdHJhbnNpdGlvbk91dChlbCwgKCkgPT4ge1xuICAgICAgICAgICAgcmVzb2x2ZSgoKSA9PiB7XG4gICAgICAgICAgICAgIGhpZGUoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0sIHJlamVjdCwgY29tcG9uZW50KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXNvbHZlKCgpID0+IHt9KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07IC8vIFRoZSB3b3JraW5nIG9mIHgtc2hvdyBpcyBhIGJpdCBjb21wbGV4IGJlY2F1c2Ugd2UgbmVlZCB0b1xuICAgIC8vIHdhaXQgZm9yIGFueSBjaGlsZCB0cmFuc2l0aW9ucyB0byBmaW5pc2ggYmVmb3JlIGhpZGluZ1xuICAgIC8vIHNvbWUgZWxlbWVudC4gQWxzbywgdGhpcyBoYXMgdG8gYmUgZG9uZSByZWN1cnNpdmVseS5cbiAgICAvLyBJZiB4LXNob3cuaW1tZWRpYXRlLCBmb3JlZ29lIHRoZSB3YWl0aW5nLlxuXG5cbiAgICBpZiAobW9kaWZpZXJzLmluY2x1ZGVzKCdpbW1lZGlhdGUnKSkge1xuICAgICAgaGFuZGxlKGZpbmlzaCA9PiBmaW5pc2goKSwgKCkgPT4ge30pO1xuICAgICAgcmV0dXJuO1xuICAgIH0gLy8geC1zaG93IGlzIGVuY291bnRlcmVkIGR1cmluZyBhIERPTSB0cmVlIHdhbGsuIElmIGFuIGVsZW1lbnRcbiAgICAvLyB3ZSBlbmNvdW50ZXIgaXMgTk9UIGEgY2hpbGQgb2YgYW5vdGhlciB4LXNob3cgZWxlbWVudCB3ZVxuICAgIC8vIGNhbiBleGVjdXRlIHRoZSBwcmV2aW91cyB4LXNob3cgc3RhY2sgKGlmIG9uZSBleGlzdHMpLlxuXG5cbiAgICBpZiAoY29tcG9uZW50LnNob3dEaXJlY3RpdmVMYXN0RWxlbWVudCAmJiAhY29tcG9uZW50LnNob3dEaXJlY3RpdmVMYXN0RWxlbWVudC5jb250YWlucyhlbCkpIHtcbiAgICAgIGNvbXBvbmVudC5leGVjdXRlQW5kQ2xlYXJSZW1haW5pbmdTaG93RGlyZWN0aXZlU3RhY2soKTtcbiAgICB9XG5cbiAgICBjb21wb25lbnQuc2hvd0RpcmVjdGl2ZVN0YWNrLnB1c2goaGFuZGxlKTtcbiAgICBjb21wb25lbnQuc2hvd0RpcmVjdGl2ZUxhc3RFbGVtZW50ID0gZWw7XG4gIH1cblxuICBmdW5jdGlvbiBoYW5kbGVJZkRpcmVjdGl2ZShjb21wb25lbnQsIGVsLCBleHByZXNzaW9uUmVzdWx0LCBpbml0aWFsVXBkYXRlLCBleHRyYVZhcnMpIHtcbiAgICB3YXJuSWZNYWxmb3JtZWRUZW1wbGF0ZShlbCwgJ3gtaWYnKTtcbiAgICBjb25zdCBlbGVtZW50SGFzQWxyZWFkeUJlZW5BZGRlZCA9IGVsLm5leHRFbGVtZW50U2libGluZyAmJiBlbC5uZXh0RWxlbWVudFNpYmxpbmcuX194X2luc2VydGVkX21lID09PSB0cnVlO1xuXG4gICAgaWYgKGV4cHJlc3Npb25SZXN1bHQgJiYgKCFlbGVtZW50SGFzQWxyZWFkeUJlZW5BZGRlZCB8fCBlbC5fX3hfdHJhbnNpdGlvbikpIHtcbiAgICAgIGNvbnN0IGNsb25lID0gZG9jdW1lbnQuaW1wb3J0Tm9kZShlbC5jb250ZW50LCB0cnVlKTtcbiAgICAgIGVsLnBhcmVudEVsZW1lbnQuaW5zZXJ0QmVmb3JlKGNsb25lLCBlbC5uZXh0RWxlbWVudFNpYmxpbmcpO1xuICAgICAgdHJhbnNpdGlvbkluKGVsLm5leHRFbGVtZW50U2libGluZywgKCkgPT4ge30sICgpID0+IHt9LCBjb21wb25lbnQsIGluaXRpYWxVcGRhdGUpO1xuICAgICAgY29tcG9uZW50LmluaXRpYWxpemVFbGVtZW50cyhlbC5uZXh0RWxlbWVudFNpYmxpbmcsIGV4dHJhVmFycyk7XG4gICAgICBlbC5uZXh0RWxlbWVudFNpYmxpbmcuX194X2luc2VydGVkX21lID0gdHJ1ZTtcbiAgICB9IGVsc2UgaWYgKCFleHByZXNzaW9uUmVzdWx0ICYmIGVsZW1lbnRIYXNBbHJlYWR5QmVlbkFkZGVkKSB7XG4gICAgICB0cmFuc2l0aW9uT3V0KGVsLm5leHRFbGVtZW50U2libGluZywgKCkgPT4ge1xuICAgICAgICBlbC5uZXh0RWxlbWVudFNpYmxpbmcucmVtb3ZlKCk7XG4gICAgICB9LCAoKSA9PiB7fSwgY29tcG9uZW50LCBpbml0aWFsVXBkYXRlKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiByZWdpc3Rlckxpc3RlbmVyKGNvbXBvbmVudCwgZWwsIGV2ZW50LCBtb2RpZmllcnMsIGV4cHJlc3Npb24sIGV4dHJhVmFycyA9IHt9KSB7XG4gICAgY29uc3Qgb3B0aW9ucyA9IHtcbiAgICAgIHBhc3NpdmU6IG1vZGlmaWVycy5pbmNsdWRlcygncGFzc2l2ZScpXG4gICAgfTtcblxuICAgIGlmIChtb2RpZmllcnMuaW5jbHVkZXMoJ2NhbWVsJykpIHtcbiAgICAgIGV2ZW50ID0gY2FtZWxDYXNlKGV2ZW50KTtcbiAgICB9XG5cbiAgICBsZXQgaGFuZGxlciwgbGlzdGVuZXJUYXJnZXQ7XG5cbiAgICBpZiAobW9kaWZpZXJzLmluY2x1ZGVzKCdhd2F5JykpIHtcbiAgICAgIGxpc3RlbmVyVGFyZ2V0ID0gZG9jdW1lbnQ7XG5cbiAgICAgIGhhbmRsZXIgPSBlID0+IHtcbiAgICAgICAgLy8gRG9uJ3QgZG8gYW55dGhpbmcgaWYgdGhlIGNsaWNrIGNhbWUgZnJvbSB0aGUgZWxlbWVudCBvciB3aXRoaW4gaXQuXG4gICAgICAgIGlmIChlbC5jb250YWlucyhlLnRhcmdldCkpIHJldHVybjsgLy8gRG9uJ3QgZG8gYW55dGhpbmcgaWYgdGhpcyBlbGVtZW50IGlzbid0IGN1cnJlbnRseSB2aXNpYmxlLlxuXG4gICAgICAgIGlmIChlbC5vZmZzZXRXaWR0aCA8IDEgJiYgZWwub2Zmc2V0SGVpZ2h0IDwgMSkgcmV0dXJuOyAvLyBOb3cgdGhhdCB3ZSBhcmUgc3VyZSB0aGUgZWxlbWVudCBpcyB2aXNpYmxlLCBBTkQgdGhlIGNsaWNrXG4gICAgICAgIC8vIGlzIGZyb20gb3V0c2lkZSBpdCwgbGV0J3MgcnVuIHRoZSBleHByZXNzaW9uLlxuXG4gICAgICAgIHJ1bkxpc3RlbmVySGFuZGxlcihjb21wb25lbnQsIGV4cHJlc3Npb24sIGUsIGV4dHJhVmFycyk7XG5cbiAgICAgICAgaWYgKG1vZGlmaWVycy5pbmNsdWRlcygnb25jZScpKSB7XG4gICAgICAgICAgZG9jdW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihldmVudCwgaGFuZGxlciwgb3B0aW9ucyk7XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgfSBlbHNlIHtcbiAgICAgIGxpc3RlbmVyVGFyZ2V0ID0gbW9kaWZpZXJzLmluY2x1ZGVzKCd3aW5kb3cnKSA/IHdpbmRvdyA6IG1vZGlmaWVycy5pbmNsdWRlcygnZG9jdW1lbnQnKSA/IGRvY3VtZW50IDogZWw7XG5cbiAgICAgIGhhbmRsZXIgPSBlID0+IHtcbiAgICAgICAgLy8gUmVtb3ZlIHRoaXMgZ2xvYmFsIGV2ZW50IGhhbmRsZXIgaWYgdGhlIGVsZW1lbnQgdGhhdCBkZWNsYXJlZCBpdFxuICAgICAgICAvLyBoYXMgYmVlbiByZW1vdmVkLiBJdCdzIG5vdyBzdGFsZS5cbiAgICAgICAgaWYgKGxpc3RlbmVyVGFyZ2V0ID09PSB3aW5kb3cgfHwgbGlzdGVuZXJUYXJnZXQgPT09IGRvY3VtZW50KSB7XG4gICAgICAgICAgaWYgKCFkb2N1bWVudC5ib2R5LmNvbnRhaW5zKGVsKSkge1xuICAgICAgICAgICAgbGlzdGVuZXJUYXJnZXQucmVtb3ZlRXZlbnRMaXN0ZW5lcihldmVudCwgaGFuZGxlciwgb3B0aW9ucyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGlzS2V5RXZlbnQoZXZlbnQpKSB7XG4gICAgICAgICAgaWYgKGlzTGlzdGVuaW5nRm9yQVNwZWNpZmljS2V5VGhhdEhhc250QmVlblByZXNzZWQoZSwgbW9kaWZpZXJzKSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChtb2RpZmllcnMuaW5jbHVkZXMoJ3ByZXZlbnQnKSkgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBpZiAobW9kaWZpZXJzLmluY2x1ZGVzKCdzdG9wJykpIGUuc3RvcFByb3BhZ2F0aW9uKCk7IC8vIElmIHRoZSAuc2VsZiBtb2RpZmllciBpc24ndCBwcmVzZW50LCBvciBpZiBpdCBpcyBwcmVzZW50IGFuZFxuICAgICAgICAvLyB0aGUgdGFyZ2V0IGVsZW1lbnQgbWF0Y2hlcyB0aGUgZWxlbWVudCB3ZSBhcmUgcmVnaXN0ZXJpbmcgdGhlXG4gICAgICAgIC8vIGV2ZW50IG9uLCBydW4gdGhlIGhhbmRsZXJcblxuICAgICAgICBpZiAoIW1vZGlmaWVycy5pbmNsdWRlcygnc2VsZicpIHx8IGUudGFyZ2V0ID09PSBlbCkge1xuICAgICAgICAgIGNvbnN0IHJldHVyblZhbHVlID0gcnVuTGlzdGVuZXJIYW5kbGVyKGNvbXBvbmVudCwgZXhwcmVzc2lvbiwgZSwgZXh0cmFWYXJzKTtcbiAgICAgICAgICByZXR1cm5WYWx1ZS50aGVuKHZhbHVlID0+IHtcbiAgICAgICAgICAgIGlmICh2YWx1ZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgaWYgKG1vZGlmaWVycy5pbmNsdWRlcygnb25jZScpKSB7XG4gICAgICAgICAgICAgICAgbGlzdGVuZXJUYXJnZXQucmVtb3ZlRXZlbnRMaXN0ZW5lcihldmVudCwgaGFuZGxlciwgb3B0aW9ucyk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICB9XG5cbiAgICBpZiAobW9kaWZpZXJzLmluY2x1ZGVzKCdkZWJvdW5jZScpKSB7XG4gICAgICBsZXQgbmV4dE1vZGlmaWVyID0gbW9kaWZpZXJzW21vZGlmaWVycy5pbmRleE9mKCdkZWJvdW5jZScpICsgMV0gfHwgJ2ludmFsaWQtd2FpdCc7XG4gICAgICBsZXQgd2FpdCA9IGlzTnVtZXJpYyhuZXh0TW9kaWZpZXIuc3BsaXQoJ21zJylbMF0pID8gTnVtYmVyKG5leHRNb2RpZmllci5zcGxpdCgnbXMnKVswXSkgOiAyNTA7XG4gICAgICBoYW5kbGVyID0gZGVib3VuY2UoaGFuZGxlciwgd2FpdCk7XG4gICAgfVxuXG4gICAgbGlzdGVuZXJUYXJnZXQuYWRkRXZlbnRMaXN0ZW5lcihldmVudCwgaGFuZGxlciwgb3B0aW9ucyk7XG4gIH1cblxuICBmdW5jdGlvbiBydW5MaXN0ZW5lckhhbmRsZXIoY29tcG9uZW50LCBleHByZXNzaW9uLCBlLCBleHRyYVZhcnMpIHtcbiAgICByZXR1cm4gY29tcG9uZW50LmV2YWx1YXRlQ29tbWFuZEV4cHJlc3Npb24oZS50YXJnZXQsIGV4cHJlc3Npb24sICgpID0+IHtcbiAgICAgIHJldHVybiBfb2JqZWN0U3ByZWFkMihfb2JqZWN0U3ByZWFkMih7fSwgZXh0cmFWYXJzKCkpLCB7fSwge1xuICAgICAgICAnJGV2ZW50JzogZVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBmdW5jdGlvbiBpc0tleUV2ZW50KGV2ZW50KSB7XG4gICAgcmV0dXJuIFsna2V5ZG93bicsICdrZXl1cCddLmluY2x1ZGVzKGV2ZW50KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGlzTGlzdGVuaW5nRm9yQVNwZWNpZmljS2V5VGhhdEhhc250QmVlblByZXNzZWQoZSwgbW9kaWZpZXJzKSB7XG4gICAgbGV0IGtleU1vZGlmaWVycyA9IG1vZGlmaWVycy5maWx0ZXIoaSA9PiB7XG4gICAgICByZXR1cm4gIVsnd2luZG93JywgJ2RvY3VtZW50JywgJ3ByZXZlbnQnLCAnc3RvcCddLmluY2x1ZGVzKGkpO1xuICAgIH0pO1xuXG4gICAgaWYgKGtleU1vZGlmaWVycy5pbmNsdWRlcygnZGVib3VuY2UnKSkge1xuICAgICAgbGV0IGRlYm91bmNlSW5kZXggPSBrZXlNb2RpZmllcnMuaW5kZXhPZignZGVib3VuY2UnKTtcbiAgICAgIGtleU1vZGlmaWVycy5zcGxpY2UoZGVib3VuY2VJbmRleCwgaXNOdW1lcmljKChrZXlNb2RpZmllcnNbZGVib3VuY2VJbmRleCArIDFdIHx8ICdpbnZhbGlkLXdhaXQnKS5zcGxpdCgnbXMnKVswXSkgPyAyIDogMSk7XG4gICAgfSAvLyBJZiBubyBtb2RpZmllciBpcyBzcGVjaWZpZWQsIHdlJ2xsIGNhbGwgaXQgYSBwcmVzcy5cblxuXG4gICAgaWYgKGtleU1vZGlmaWVycy5sZW5ndGggPT09IDApIHJldHVybiBmYWxzZTsgLy8gSWYgb25lIGlzIHBhc3NlZCwgQU5EIGl0IG1hdGNoZXMgdGhlIGtleSBwcmVzc2VkLCB3ZSdsbCBjYWxsIGl0IGEgcHJlc3MuXG5cbiAgICBpZiAoa2V5TW9kaWZpZXJzLmxlbmd0aCA9PT0gMSAmJiBrZXlNb2RpZmllcnNbMF0gPT09IGtleVRvTW9kaWZpZXIoZS5rZXkpKSByZXR1cm4gZmFsc2U7IC8vIFRoZSB1c2VyIGlzIGxpc3RlbmluZyBmb3Iga2V5IGNvbWJpbmF0aW9ucy5cblxuICAgIGNvbnN0IHN5c3RlbUtleU1vZGlmaWVycyA9IFsnY3RybCcsICdzaGlmdCcsICdhbHQnLCAnbWV0YScsICdjbWQnLCAnc3VwZXInXTtcbiAgICBjb25zdCBzZWxlY3RlZFN5c3RlbUtleU1vZGlmaWVycyA9IHN5c3RlbUtleU1vZGlmaWVycy5maWx0ZXIobW9kaWZpZXIgPT4ga2V5TW9kaWZpZXJzLmluY2x1ZGVzKG1vZGlmaWVyKSk7XG4gICAga2V5TW9kaWZpZXJzID0ga2V5TW9kaWZpZXJzLmZpbHRlcihpID0+ICFzZWxlY3RlZFN5c3RlbUtleU1vZGlmaWVycy5pbmNsdWRlcyhpKSk7XG5cbiAgICBpZiAoc2VsZWN0ZWRTeXN0ZW1LZXlNb2RpZmllcnMubGVuZ3RoID4gMCkge1xuICAgICAgY29uc3QgYWN0aXZlbHlQcmVzc2VkS2V5TW9kaWZpZXJzID0gc2VsZWN0ZWRTeXN0ZW1LZXlNb2RpZmllcnMuZmlsdGVyKG1vZGlmaWVyID0+IHtcbiAgICAgICAgLy8gQWxpYXMgXCJjbWRcIiBhbmQgXCJzdXBlclwiIHRvIFwibWV0YVwiXG4gICAgICAgIGlmIChtb2RpZmllciA9PT0gJ2NtZCcgfHwgbW9kaWZpZXIgPT09ICdzdXBlcicpIG1vZGlmaWVyID0gJ21ldGEnO1xuICAgICAgICByZXR1cm4gZVtgJHttb2RpZmllcn1LZXlgXTtcbiAgICAgIH0pOyAvLyBJZiBhbGwgdGhlIG1vZGlmaWVycyBzZWxlY3RlZCBhcmUgcHJlc3NlZCwgLi4uXG5cbiAgICAgIGlmIChhY3RpdmVseVByZXNzZWRLZXlNb2RpZmllcnMubGVuZ3RoID09PSBzZWxlY3RlZFN5c3RlbUtleU1vZGlmaWVycy5sZW5ndGgpIHtcbiAgICAgICAgLy8gQU5EIHRoZSByZW1haW5pbmcga2V5IGlzIHByZXNzZWQgYXMgd2VsbC4gSXQncyBhIHByZXNzLlxuICAgICAgICBpZiAoa2V5TW9kaWZpZXJzWzBdID09PSBrZXlUb01vZGlmaWVyKGUua2V5KSkgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH0gLy8gV2UnbGwgY2FsbCBpdCBOT1QgYSB2YWxpZCBrZXlwcmVzcy5cblxuXG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICBmdW5jdGlvbiBrZXlUb01vZGlmaWVyKGtleSkge1xuICAgIHN3aXRjaCAoa2V5KSB7XG4gICAgICBjYXNlICcvJzpcbiAgICAgICAgcmV0dXJuICdzbGFzaCc7XG5cbiAgICAgIGNhc2UgJyAnOlxuICAgICAgY2FzZSAnU3BhY2ViYXInOlxuICAgICAgICByZXR1cm4gJ3NwYWNlJztcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIGtleSAmJiBrZWJhYkNhc2Uoa2V5KTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiByZWdpc3Rlck1vZGVsTGlzdGVuZXIoY29tcG9uZW50LCBlbCwgbW9kaWZpZXJzLCBleHByZXNzaW9uLCBleHRyYVZhcnMpIHtcbiAgICAvLyBJZiB0aGUgZWxlbWVudCB3ZSBhcmUgYmluZGluZyB0byBpcyBhIHNlbGVjdCwgYSByYWRpbywgb3IgY2hlY2tib3hcbiAgICAvLyB3ZSdsbCBsaXN0ZW4gZm9yIHRoZSBjaGFuZ2UgZXZlbnQgaW5zdGVhZCBvZiB0aGUgXCJpbnB1dFwiIGV2ZW50LlxuICAgIHZhciBldmVudCA9IGVsLnRhZ05hbWUudG9Mb3dlckNhc2UoKSA9PT0gJ3NlbGVjdCcgfHwgWydjaGVja2JveCcsICdyYWRpbyddLmluY2x1ZGVzKGVsLnR5cGUpIHx8IG1vZGlmaWVycy5pbmNsdWRlcygnbGF6eScpID8gJ2NoYW5nZScgOiAnaW5wdXQnO1xuICAgIGNvbnN0IGxpc3RlbmVyRXhwcmVzc2lvbiA9IGAke2V4cHJlc3Npb259ID0gcmlnaHRTaWRlT2ZFeHByZXNzaW9uKCRldmVudCwgJHtleHByZXNzaW9ufSlgO1xuICAgIHJlZ2lzdGVyTGlzdGVuZXIoY29tcG9uZW50LCBlbCwgZXZlbnQsIG1vZGlmaWVycywgbGlzdGVuZXJFeHByZXNzaW9uLCAoKSA9PiB7XG4gICAgICByZXR1cm4gX29iamVjdFNwcmVhZDIoX29iamVjdFNwcmVhZDIoe30sIGV4dHJhVmFycygpKSwge30sIHtcbiAgICAgICAgcmlnaHRTaWRlT2ZFeHByZXNzaW9uOiBnZW5lcmF0ZU1vZGVsQXNzaWdubWVudEZ1bmN0aW9uKGVsLCBtb2RpZmllcnMsIGV4cHJlc3Npb24pXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGdlbmVyYXRlTW9kZWxBc3NpZ25tZW50RnVuY3Rpb24oZWwsIG1vZGlmaWVycywgZXhwcmVzc2lvbikge1xuICAgIGlmIChlbC50eXBlID09PSAncmFkaW8nKSB7XG4gICAgICAvLyBSYWRpbyBidXR0b25zIG9ubHkgd29yayBwcm9wZXJseSB3aGVuIHRoZXkgc2hhcmUgYSBuYW1lIGF0dHJpYnV0ZS5cbiAgICAgIC8vIFBlb3BsZSBtaWdodCBhc3N1bWUgd2UgdGFrZSBjYXJlIG9mIHRoYXQgZm9yIHRoZW0sIGJlY2F1c2VcbiAgICAgIC8vIHRoZXkgYWxyZWFkeSBzZXQgYSBzaGFyZWQgXCJ4LW1vZGVsXCIgYXR0cmlidXRlLlxuICAgICAgaWYgKCFlbC5oYXNBdHRyaWJ1dGUoJ25hbWUnKSkgZWwuc2V0QXR0cmlidXRlKCduYW1lJywgZXhwcmVzc2lvbik7XG4gICAgfVxuXG4gICAgcmV0dXJuIChldmVudCwgY3VycmVudFZhbHVlKSA9PiB7XG4gICAgICAvLyBDaGVjayBmb3IgZXZlbnQuZGV0YWlsIGR1ZSB0byBhbiBpc3N1ZSB3aGVyZSBJRTExIGhhbmRsZXMgb3RoZXIgZXZlbnRzIGFzIGEgQ3VzdG9tRXZlbnQuXG4gICAgICBpZiAoZXZlbnQgaW5zdGFuY2VvZiBDdXN0b21FdmVudCAmJiBldmVudC5kZXRhaWwpIHtcbiAgICAgICAgcmV0dXJuIGV2ZW50LmRldGFpbDtcbiAgICAgIH0gZWxzZSBpZiAoZWwudHlwZSA9PT0gJ2NoZWNrYm94Jykge1xuICAgICAgICAvLyBJZiB0aGUgZGF0YSB3ZSBhcmUgYmluZGluZyB0byBpcyBhbiBhcnJheSwgdG9nZ2xlIGl0cyB2YWx1ZSBpbnNpZGUgdGhlIGFycmF5LlxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShjdXJyZW50VmFsdWUpKSB7XG4gICAgICAgICAgY29uc3QgbmV3VmFsdWUgPSBtb2RpZmllcnMuaW5jbHVkZXMoJ251bWJlcicpID8gc2FmZVBhcnNlTnVtYmVyKGV2ZW50LnRhcmdldC52YWx1ZSkgOiBldmVudC50YXJnZXQudmFsdWU7XG4gICAgICAgICAgcmV0dXJuIGV2ZW50LnRhcmdldC5jaGVja2VkID8gY3VycmVudFZhbHVlLmNvbmNhdChbbmV3VmFsdWVdKSA6IGN1cnJlbnRWYWx1ZS5maWx0ZXIoZWwgPT4gIWNoZWNrZWRBdHRyTG9vc2VDb21wYXJlKGVsLCBuZXdWYWx1ZSkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiBldmVudC50YXJnZXQuY2hlY2tlZDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChlbC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgPT09ICdzZWxlY3QnICYmIGVsLm11bHRpcGxlKSB7XG4gICAgICAgIHJldHVybiBtb2RpZmllcnMuaW5jbHVkZXMoJ251bWJlcicpID8gQXJyYXkuZnJvbShldmVudC50YXJnZXQuc2VsZWN0ZWRPcHRpb25zKS5tYXAob3B0aW9uID0+IHtcbiAgICAgICAgICBjb25zdCByYXdWYWx1ZSA9IG9wdGlvbi52YWx1ZSB8fCBvcHRpb24udGV4dDtcbiAgICAgICAgICByZXR1cm4gc2FmZVBhcnNlTnVtYmVyKHJhd1ZhbHVlKTtcbiAgICAgICAgfSkgOiBBcnJheS5mcm9tKGV2ZW50LnRhcmdldC5zZWxlY3RlZE9wdGlvbnMpLm1hcChvcHRpb24gPT4ge1xuICAgICAgICAgIHJldHVybiBvcHRpb24udmFsdWUgfHwgb3B0aW9uLnRleHQ7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc3QgcmF3VmFsdWUgPSBldmVudC50YXJnZXQudmFsdWU7XG4gICAgICAgIHJldHVybiBtb2RpZmllcnMuaW5jbHVkZXMoJ251bWJlcicpID8gc2FmZVBhcnNlTnVtYmVyKHJhd1ZhbHVlKSA6IG1vZGlmaWVycy5pbmNsdWRlcygndHJpbScpID8gcmF3VmFsdWUudHJpbSgpIDogcmF3VmFsdWU7XG4gICAgICB9XG4gICAgfTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHNhZmVQYXJzZU51bWJlcihyYXdWYWx1ZSkge1xuICAgIGNvbnN0IG51bWJlciA9IHJhd1ZhbHVlID8gcGFyc2VGbG9hdChyYXdWYWx1ZSkgOiBudWxsO1xuICAgIHJldHVybiBpc051bWVyaWMobnVtYmVyKSA/IG51bWJlciA6IHJhd1ZhbHVlO1xuICB9XG5cbiAgLyoqXG4gICAqIENvcHlyaWdodCAoQykgMjAxNyBzYWxlc2ZvcmNlLmNvbSwgaW5jLlxuICAgKi9cbiAgY29uc3QgeyBpc0FycmF5IH0gPSBBcnJheTtcbiAgY29uc3QgeyBnZXRQcm90b3R5cGVPZiwgY3JlYXRlOiBPYmplY3RDcmVhdGUsIGRlZmluZVByb3BlcnR5OiBPYmplY3REZWZpbmVQcm9wZXJ0eSwgZGVmaW5lUHJvcGVydGllczogT2JqZWN0RGVmaW5lUHJvcGVydGllcywgaXNFeHRlbnNpYmxlLCBnZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IsIGdldE93blByb3BlcnR5TmFtZXMsIGdldE93blByb3BlcnR5U3ltYm9scywgcHJldmVudEV4dGVuc2lvbnMsIGhhc093blByb3BlcnR5LCB9ID0gT2JqZWN0O1xuICBjb25zdCB7IHB1c2g6IEFycmF5UHVzaCwgY29uY2F0OiBBcnJheUNvbmNhdCwgbWFwOiBBcnJheU1hcCwgfSA9IEFycmF5LnByb3RvdHlwZTtcbiAgZnVuY3Rpb24gaXNVbmRlZmluZWQob2JqKSB7XG4gICAgICByZXR1cm4gb2JqID09PSB1bmRlZmluZWQ7XG4gIH1cbiAgZnVuY3Rpb24gaXNGdW5jdGlvbihvYmopIHtcbiAgICAgIHJldHVybiB0eXBlb2Ygb2JqID09PSAnZnVuY3Rpb24nO1xuICB9XG4gIGZ1bmN0aW9uIGlzT2JqZWN0KG9iaikge1xuICAgICAgcmV0dXJuIHR5cGVvZiBvYmogPT09ICdvYmplY3QnO1xuICB9XG4gIGNvbnN0IHByb3h5VG9WYWx1ZU1hcCA9IG5ldyBXZWFrTWFwKCk7XG4gIGZ1bmN0aW9uIHJlZ2lzdGVyUHJveHkocHJveHksIHZhbHVlKSB7XG4gICAgICBwcm94eVRvVmFsdWVNYXAuc2V0KHByb3h5LCB2YWx1ZSk7XG4gIH1cbiAgY29uc3QgdW53cmFwID0gKHJlcGxpY2FPckFueSkgPT4gcHJveHlUb1ZhbHVlTWFwLmdldChyZXBsaWNhT3JBbnkpIHx8IHJlcGxpY2FPckFueTtcblxuICBmdW5jdGlvbiB3cmFwVmFsdWUobWVtYnJhbmUsIHZhbHVlKSB7XG4gICAgICByZXR1cm4gbWVtYnJhbmUudmFsdWVJc09ic2VydmFibGUodmFsdWUpID8gbWVtYnJhbmUuZ2V0UHJveHkodmFsdWUpIDogdmFsdWU7XG4gIH1cbiAgLyoqXG4gICAqIFVud3JhcCBwcm9wZXJ0eSBkZXNjcmlwdG9ycyB3aWxsIHNldCB2YWx1ZSBvbiBvcmlnaW5hbCBkZXNjcmlwdG9yXG4gICAqIFdlIG9ubHkgbmVlZCB0byB1bndyYXAgaWYgdmFsdWUgaXMgc3BlY2lmaWVkXG4gICAqIEBwYXJhbSBkZXNjcmlwdG9yIGV4dGVybmFsIGRlc2NycGl0b3IgcHJvdmlkZWQgdG8gZGVmaW5lIG5ldyBwcm9wZXJ0eSBvbiBvcmlnaW5hbCB2YWx1ZVxuICAgKi9cbiAgZnVuY3Rpb24gdW53cmFwRGVzY3JpcHRvcihkZXNjcmlwdG9yKSB7XG4gICAgICBpZiAoaGFzT3duUHJvcGVydHkuY2FsbChkZXNjcmlwdG9yLCAndmFsdWUnKSkge1xuICAgICAgICAgIGRlc2NyaXB0b3IudmFsdWUgPSB1bndyYXAoZGVzY3JpcHRvci52YWx1ZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gZGVzY3JpcHRvcjtcbiAgfVxuICBmdW5jdGlvbiBsb2NrU2hhZG93VGFyZ2V0KG1lbWJyYW5lLCBzaGFkb3dUYXJnZXQsIG9yaWdpbmFsVGFyZ2V0KSB7XG4gICAgICBjb25zdCB0YXJnZXRLZXlzID0gQXJyYXlDb25jYXQuY2FsbChnZXRPd25Qcm9wZXJ0eU5hbWVzKG9yaWdpbmFsVGFyZ2V0KSwgZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9yaWdpbmFsVGFyZ2V0KSk7XG4gICAgICB0YXJnZXRLZXlzLmZvckVhY2goKGtleSkgPT4ge1xuICAgICAgICAgIGxldCBkZXNjcmlwdG9yID0gZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9yaWdpbmFsVGFyZ2V0LCBrZXkpO1xuICAgICAgICAgIC8vIFdlIGRvIG5vdCBuZWVkIHRvIHdyYXAgdGhlIGRlc2NyaXB0b3IgaWYgY29uZmlndXJhYmxlXG4gICAgICAgICAgLy8gQmVjYXVzZSB3ZSBjYW4gZGVhbCB3aXRoIHdyYXBwaW5nIGl0IHdoZW4gdXNlciBnb2VzIHRocm91Z2hcbiAgICAgICAgICAvLyBHZXQgb3duIHByb3BlcnR5IGRlc2NyaXB0b3IuIFRoZXJlIGlzIGFsc28gYSBjaGFuY2UgdGhhdCB0aGlzIGRlc2NyaXB0b3JcbiAgICAgICAgICAvLyBjb3VsZCBjaGFuZ2Ugc29tZXRpbWUgaW4gdGhlIGZ1dHVyZSwgc28gd2UgY2FuIGRlZmVyIHdyYXBwaW5nXG4gICAgICAgICAgLy8gdW50aWwgd2UgbmVlZCB0b1xuICAgICAgICAgIGlmICghZGVzY3JpcHRvci5jb25maWd1cmFibGUpIHtcbiAgICAgICAgICAgICAgZGVzY3JpcHRvciA9IHdyYXBEZXNjcmlwdG9yKG1lbWJyYW5lLCBkZXNjcmlwdG9yLCB3cmFwVmFsdWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBPYmplY3REZWZpbmVQcm9wZXJ0eShzaGFkb3dUYXJnZXQsIGtleSwgZGVzY3JpcHRvcik7XG4gICAgICB9KTtcbiAgICAgIHByZXZlbnRFeHRlbnNpb25zKHNoYWRvd1RhcmdldCk7XG4gIH1cbiAgY2xhc3MgUmVhY3RpdmVQcm94eUhhbmRsZXIge1xuICAgICAgY29uc3RydWN0b3IobWVtYnJhbmUsIHZhbHVlKSB7XG4gICAgICAgICAgdGhpcy5vcmlnaW5hbFRhcmdldCA9IHZhbHVlO1xuICAgICAgICAgIHRoaXMubWVtYnJhbmUgPSBtZW1icmFuZTtcbiAgICAgIH1cbiAgICAgIGdldChzaGFkb3dUYXJnZXQsIGtleSkge1xuICAgICAgICAgIGNvbnN0IHsgb3JpZ2luYWxUYXJnZXQsIG1lbWJyYW5lIH0gPSB0aGlzO1xuICAgICAgICAgIGNvbnN0IHZhbHVlID0gb3JpZ2luYWxUYXJnZXRba2V5XTtcbiAgICAgICAgICBjb25zdCB7IHZhbHVlT2JzZXJ2ZWQgfSA9IG1lbWJyYW5lO1xuICAgICAgICAgIHZhbHVlT2JzZXJ2ZWQob3JpZ2luYWxUYXJnZXQsIGtleSk7XG4gICAgICAgICAgcmV0dXJuIG1lbWJyYW5lLmdldFByb3h5KHZhbHVlKTtcbiAgICAgIH1cbiAgICAgIHNldChzaGFkb3dUYXJnZXQsIGtleSwgdmFsdWUpIHtcbiAgICAgICAgICBjb25zdCB7IG9yaWdpbmFsVGFyZ2V0LCBtZW1icmFuZTogeyB2YWx1ZU11dGF0ZWQgfSB9ID0gdGhpcztcbiAgICAgICAgICBjb25zdCBvbGRWYWx1ZSA9IG9yaWdpbmFsVGFyZ2V0W2tleV07XG4gICAgICAgICAgaWYgKG9sZFZhbHVlICE9PSB2YWx1ZSkge1xuICAgICAgICAgICAgICBvcmlnaW5hbFRhcmdldFtrZXldID0gdmFsdWU7XG4gICAgICAgICAgICAgIHZhbHVlTXV0YXRlZChvcmlnaW5hbFRhcmdldCwga2V5KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAoa2V5ID09PSAnbGVuZ3RoJyAmJiBpc0FycmF5KG9yaWdpbmFsVGFyZ2V0KSkge1xuICAgICAgICAgICAgICAvLyBmaXggZm9yIGlzc3VlICMyMzY6IHB1c2ggd2lsbCBhZGQgdGhlIG5ldyBpbmRleCwgYW5kIGJ5IHRoZSB0aW1lIGxlbmd0aFxuICAgICAgICAgICAgICAvLyBpcyB1cGRhdGVkLCB0aGUgaW50ZXJuYWwgbGVuZ3RoIGlzIGFscmVhZHkgZXF1YWwgdG8gdGhlIG5ldyBsZW5ndGggdmFsdWVcbiAgICAgICAgICAgICAgLy8gdGhlcmVmb3JlLCB0aGUgb2xkVmFsdWUgaXMgZXF1YWwgdG8gdGhlIHZhbHVlLiBUaGlzIGlzIHRoZSBmb3JraW5nIGxvZ2ljXG4gICAgICAgICAgICAgIC8vIHRvIHN1cHBvcnQgdGhpcyB1c2UgY2FzZS5cbiAgICAgICAgICAgICAgdmFsdWVNdXRhdGVkKG9yaWdpbmFsVGFyZ2V0LCBrZXkpO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICAgIGRlbGV0ZVByb3BlcnR5KHNoYWRvd1RhcmdldCwga2V5KSB7XG4gICAgICAgICAgY29uc3QgeyBvcmlnaW5hbFRhcmdldCwgbWVtYnJhbmU6IHsgdmFsdWVNdXRhdGVkIH0gfSA9IHRoaXM7XG4gICAgICAgICAgZGVsZXRlIG9yaWdpbmFsVGFyZ2V0W2tleV07XG4gICAgICAgICAgdmFsdWVNdXRhdGVkKG9yaWdpbmFsVGFyZ2V0LCBrZXkpO1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgICAgYXBwbHkoc2hhZG93VGFyZ2V0LCB0aGlzQXJnLCBhcmdBcnJheSkge1xuICAgICAgICAgIC8qIE5vIG9wICovXG4gICAgICB9XG4gICAgICBjb25zdHJ1Y3QodGFyZ2V0LCBhcmdBcnJheSwgbmV3VGFyZ2V0KSB7XG4gICAgICAgICAgLyogTm8gb3AgKi9cbiAgICAgIH1cbiAgICAgIGhhcyhzaGFkb3dUYXJnZXQsIGtleSkge1xuICAgICAgICAgIGNvbnN0IHsgb3JpZ2luYWxUYXJnZXQsIG1lbWJyYW5lOiB7IHZhbHVlT2JzZXJ2ZWQgfSB9ID0gdGhpcztcbiAgICAgICAgICB2YWx1ZU9ic2VydmVkKG9yaWdpbmFsVGFyZ2V0LCBrZXkpO1xuICAgICAgICAgIHJldHVybiBrZXkgaW4gb3JpZ2luYWxUYXJnZXQ7XG4gICAgICB9XG4gICAgICBvd25LZXlzKHNoYWRvd1RhcmdldCkge1xuICAgICAgICAgIGNvbnN0IHsgb3JpZ2luYWxUYXJnZXQgfSA9IHRoaXM7XG4gICAgICAgICAgcmV0dXJuIEFycmF5Q29uY2F0LmNhbGwoZ2V0T3duUHJvcGVydHlOYW1lcyhvcmlnaW5hbFRhcmdldCksIGdldE93blByb3BlcnR5U3ltYm9scyhvcmlnaW5hbFRhcmdldCkpO1xuICAgICAgfVxuICAgICAgaXNFeHRlbnNpYmxlKHNoYWRvd1RhcmdldCkge1xuICAgICAgICAgIGNvbnN0IHNoYWRvd0lzRXh0ZW5zaWJsZSA9IGlzRXh0ZW5zaWJsZShzaGFkb3dUYXJnZXQpO1xuICAgICAgICAgIGlmICghc2hhZG93SXNFeHRlbnNpYmxlKSB7XG4gICAgICAgICAgICAgIHJldHVybiBzaGFkb3dJc0V4dGVuc2libGU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnN0IHsgb3JpZ2luYWxUYXJnZXQsIG1lbWJyYW5lIH0gPSB0aGlzO1xuICAgICAgICAgIGNvbnN0IHRhcmdldElzRXh0ZW5zaWJsZSA9IGlzRXh0ZW5zaWJsZShvcmlnaW5hbFRhcmdldCk7XG4gICAgICAgICAgaWYgKCF0YXJnZXRJc0V4dGVuc2libGUpIHtcbiAgICAgICAgICAgICAgbG9ja1NoYWRvd1RhcmdldChtZW1icmFuZSwgc2hhZG93VGFyZ2V0LCBvcmlnaW5hbFRhcmdldCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiB0YXJnZXRJc0V4dGVuc2libGU7XG4gICAgICB9XG4gICAgICBzZXRQcm90b3R5cGVPZihzaGFkb3dUYXJnZXQsIHByb3RvdHlwZSkge1xuICAgICAgfVxuICAgICAgZ2V0UHJvdG90eXBlT2Yoc2hhZG93VGFyZ2V0KSB7XG4gICAgICAgICAgY29uc3QgeyBvcmlnaW5hbFRhcmdldCB9ID0gdGhpcztcbiAgICAgICAgICByZXR1cm4gZ2V0UHJvdG90eXBlT2Yob3JpZ2luYWxUYXJnZXQpO1xuICAgICAgfVxuICAgICAgZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNoYWRvd1RhcmdldCwga2V5KSB7XG4gICAgICAgICAgY29uc3QgeyBvcmlnaW5hbFRhcmdldCwgbWVtYnJhbmUgfSA9IHRoaXM7XG4gICAgICAgICAgY29uc3QgeyB2YWx1ZU9ic2VydmVkIH0gPSB0aGlzLm1lbWJyYW5lO1xuICAgICAgICAgIC8vIGtleXMgbG9va2VkIHVwIHZpYSBoYXNPd25Qcm9wZXJ0eSBuZWVkIHRvIGJlIHJlYWN0aXZlXG4gICAgICAgICAgdmFsdWVPYnNlcnZlZChvcmlnaW5hbFRhcmdldCwga2V5KTtcbiAgICAgICAgICBsZXQgZGVzYyA9IGdldE93blByb3BlcnR5RGVzY3JpcHRvcihvcmlnaW5hbFRhcmdldCwga2V5KTtcbiAgICAgICAgICBpZiAoaXNVbmRlZmluZWQoZGVzYykpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGRlc2M7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnN0IHNoYWRvd0Rlc2NyaXB0b3IgPSBnZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc2hhZG93VGFyZ2V0LCBrZXkpO1xuICAgICAgICAgIGlmICghaXNVbmRlZmluZWQoc2hhZG93RGVzY3JpcHRvcikpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHNoYWRvd0Rlc2NyaXB0b3I7XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIE5vdGU6IGJ5IGFjY2Vzc2luZyB0aGUgZGVzY3JpcHRvciwgdGhlIGtleSBpcyBtYXJrZWQgYXMgb2JzZXJ2ZWRcbiAgICAgICAgICAvLyBidXQgYWNjZXNzIHRvIHRoZSB2YWx1ZSwgc2V0dGVyIG9yIGdldHRlciAoaWYgYXZhaWxhYmxlKSBjYW5ub3Qgb2JzZXJ2ZVxuICAgICAgICAgIC8vIG11dGF0aW9ucywganVzdCBsaWtlIHJlZ3VsYXIgbWV0aG9kcywgaW4gd2hpY2ggY2FzZSB3ZSBqdXN0IGRvIG5vdGhpbmcuXG4gICAgICAgICAgZGVzYyA9IHdyYXBEZXNjcmlwdG9yKG1lbWJyYW5lLCBkZXNjLCB3cmFwVmFsdWUpO1xuICAgICAgICAgIGlmICghZGVzYy5jb25maWd1cmFibGUpIHtcbiAgICAgICAgICAgICAgLy8gSWYgZGVzY3JpcHRvciBmcm9tIG9yaWdpbmFsIHRhcmdldCBpcyBub3QgY29uZmlndXJhYmxlLFxuICAgICAgICAgICAgICAvLyBXZSBtdXN0IGNvcHkgdGhlIHdyYXBwZWQgZGVzY3JpcHRvciBvdmVyIHRvIHRoZSBzaGFkb3cgdGFyZ2V0LlxuICAgICAgICAgICAgICAvLyBPdGhlcndpc2UsIHByb3h5IHdpbGwgdGhyb3cgYW4gaW52YXJpYW50IGVycm9yLlxuICAgICAgICAgICAgICAvLyBUaGlzIGlzIG91ciBsYXN0IGNoYW5jZSB0byBsb2NrIHRoZSB2YWx1ZS5cbiAgICAgICAgICAgICAgLy8gaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvSmF2YVNjcmlwdC9SZWZlcmVuY2UvR2xvYmFsX09iamVjdHMvUHJveHkvaGFuZGxlci9nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IjSW52YXJpYW50c1xuICAgICAgICAgICAgICBPYmplY3REZWZpbmVQcm9wZXJ0eShzaGFkb3dUYXJnZXQsIGtleSwgZGVzYyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBkZXNjO1xuICAgICAgfVxuICAgICAgcHJldmVudEV4dGVuc2lvbnMoc2hhZG93VGFyZ2V0KSB7XG4gICAgICAgICAgY29uc3QgeyBvcmlnaW5hbFRhcmdldCwgbWVtYnJhbmUgfSA9IHRoaXM7XG4gICAgICAgICAgbG9ja1NoYWRvd1RhcmdldChtZW1icmFuZSwgc2hhZG93VGFyZ2V0LCBvcmlnaW5hbFRhcmdldCk7XG4gICAgICAgICAgcHJldmVudEV4dGVuc2lvbnMob3JpZ2luYWxUYXJnZXQpO1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgICAgZGVmaW5lUHJvcGVydHkoc2hhZG93VGFyZ2V0LCBrZXksIGRlc2NyaXB0b3IpIHtcbiAgICAgICAgICBjb25zdCB7IG9yaWdpbmFsVGFyZ2V0LCBtZW1icmFuZSB9ID0gdGhpcztcbiAgICAgICAgICBjb25zdCB7IHZhbHVlTXV0YXRlZCB9ID0gbWVtYnJhbmU7XG4gICAgICAgICAgY29uc3QgeyBjb25maWd1cmFibGUgfSA9IGRlc2NyaXB0b3I7XG4gICAgICAgICAgLy8gV2UgaGF2ZSB0byBjaGVjayBmb3IgdmFsdWUgaW4gZGVzY3JpcHRvclxuICAgICAgICAgIC8vIGJlY2F1c2UgT2JqZWN0LmZyZWV6ZShwcm94eSkgY2FsbHMgdGhpcyBtZXRob2RcbiAgICAgICAgICAvLyB3aXRoIG9ubHkgeyBjb25maWd1cmFibGU6IGZhbHNlLCB3cml0ZWFibGU6IGZhbHNlIH1cbiAgICAgICAgICAvLyBBZGRpdGlvbmFsbHksIG1ldGhvZCB3aWxsIG9ubHkgYmUgY2FsbGVkIHdpdGggd3JpdGVhYmxlOmZhbHNlXG4gICAgICAgICAgLy8gaWYgdGhlIGRlc2NyaXB0b3IgaGFzIGEgdmFsdWUsIGFzIG9wcG9zZWQgdG8gZ2V0dGVyL3NldHRlclxuICAgICAgICAgIC8vIFNvIHdlIGNhbiBqdXN0IGNoZWNrIGlmIHdyaXRhYmxlIGlzIHByZXNlbnQgYW5kIHRoZW4gc2VlIGlmXG4gICAgICAgICAgLy8gdmFsdWUgaXMgcHJlc2VudC4gVGhpcyBlbGltaW5hdGVzIGdldHRlciBhbmQgc2V0dGVyIGRlc2NyaXB0b3JzXG4gICAgICAgICAgaWYgKGhhc093blByb3BlcnR5LmNhbGwoZGVzY3JpcHRvciwgJ3dyaXRhYmxlJykgJiYgIWhhc093blByb3BlcnR5LmNhbGwoZGVzY3JpcHRvciwgJ3ZhbHVlJykpIHtcbiAgICAgICAgICAgICAgY29uc3Qgb3JpZ2luYWxEZXNjcmlwdG9yID0gZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9yaWdpbmFsVGFyZ2V0LCBrZXkpO1xuICAgICAgICAgICAgICBkZXNjcmlwdG9yLnZhbHVlID0gb3JpZ2luYWxEZXNjcmlwdG9yLnZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBPYmplY3REZWZpbmVQcm9wZXJ0eShvcmlnaW5hbFRhcmdldCwga2V5LCB1bndyYXBEZXNjcmlwdG9yKGRlc2NyaXB0b3IpKTtcbiAgICAgICAgICBpZiAoY29uZmlndXJhYmxlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICBPYmplY3REZWZpbmVQcm9wZXJ0eShzaGFkb3dUYXJnZXQsIGtleSwgd3JhcERlc2NyaXB0b3IobWVtYnJhbmUsIGRlc2NyaXB0b3IsIHdyYXBWYWx1ZSkpO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YWx1ZU11dGF0ZWQob3JpZ2luYWxUYXJnZXQsIGtleSk7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gIH1cblxuICBmdW5jdGlvbiB3cmFwUmVhZE9ubHlWYWx1ZShtZW1icmFuZSwgdmFsdWUpIHtcbiAgICAgIHJldHVybiBtZW1icmFuZS52YWx1ZUlzT2JzZXJ2YWJsZSh2YWx1ZSkgPyBtZW1icmFuZS5nZXRSZWFkT25seVByb3h5KHZhbHVlKSA6IHZhbHVlO1xuICB9XG4gIGNsYXNzIFJlYWRPbmx5SGFuZGxlciB7XG4gICAgICBjb25zdHJ1Y3RvcihtZW1icmFuZSwgdmFsdWUpIHtcbiAgICAgICAgICB0aGlzLm9yaWdpbmFsVGFyZ2V0ID0gdmFsdWU7XG4gICAgICAgICAgdGhpcy5tZW1icmFuZSA9IG1lbWJyYW5lO1xuICAgICAgfVxuICAgICAgZ2V0KHNoYWRvd1RhcmdldCwga2V5KSB7XG4gICAgICAgICAgY29uc3QgeyBtZW1icmFuZSwgb3JpZ2luYWxUYXJnZXQgfSA9IHRoaXM7XG4gICAgICAgICAgY29uc3QgdmFsdWUgPSBvcmlnaW5hbFRhcmdldFtrZXldO1xuICAgICAgICAgIGNvbnN0IHsgdmFsdWVPYnNlcnZlZCB9ID0gbWVtYnJhbmU7XG4gICAgICAgICAgdmFsdWVPYnNlcnZlZChvcmlnaW5hbFRhcmdldCwga2V5KTtcbiAgICAgICAgICByZXR1cm4gbWVtYnJhbmUuZ2V0UmVhZE9ubHlQcm94eSh2YWx1ZSk7XG4gICAgICB9XG4gICAgICBzZXQoc2hhZG93VGFyZ2V0LCBrZXksIHZhbHVlKSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgICAgZGVsZXRlUHJvcGVydHkoc2hhZG93VGFyZ2V0LCBrZXkpIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICBhcHBseShzaGFkb3dUYXJnZXQsIHRoaXNBcmcsIGFyZ0FycmF5KSB7XG4gICAgICAgICAgLyogTm8gb3AgKi9cbiAgICAgIH1cbiAgICAgIGNvbnN0cnVjdCh0YXJnZXQsIGFyZ0FycmF5LCBuZXdUYXJnZXQpIHtcbiAgICAgICAgICAvKiBObyBvcCAqL1xuICAgICAgfVxuICAgICAgaGFzKHNoYWRvd1RhcmdldCwga2V5KSB7XG4gICAgICAgICAgY29uc3QgeyBvcmlnaW5hbFRhcmdldCwgbWVtYnJhbmU6IHsgdmFsdWVPYnNlcnZlZCB9IH0gPSB0aGlzO1xuICAgICAgICAgIHZhbHVlT2JzZXJ2ZWQob3JpZ2luYWxUYXJnZXQsIGtleSk7XG4gICAgICAgICAgcmV0dXJuIGtleSBpbiBvcmlnaW5hbFRhcmdldDtcbiAgICAgIH1cbiAgICAgIG93bktleXMoc2hhZG93VGFyZ2V0KSB7XG4gICAgICAgICAgY29uc3QgeyBvcmlnaW5hbFRhcmdldCB9ID0gdGhpcztcbiAgICAgICAgICByZXR1cm4gQXJyYXlDb25jYXQuY2FsbChnZXRPd25Qcm9wZXJ0eU5hbWVzKG9yaWdpbmFsVGFyZ2V0KSwgZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9yaWdpbmFsVGFyZ2V0KSk7XG4gICAgICB9XG4gICAgICBzZXRQcm90b3R5cGVPZihzaGFkb3dUYXJnZXQsIHByb3RvdHlwZSkge1xuICAgICAgfVxuICAgICAgZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNoYWRvd1RhcmdldCwga2V5KSB7XG4gICAgICAgICAgY29uc3QgeyBvcmlnaW5hbFRhcmdldCwgbWVtYnJhbmUgfSA9IHRoaXM7XG4gICAgICAgICAgY29uc3QgeyB2YWx1ZU9ic2VydmVkIH0gPSBtZW1icmFuZTtcbiAgICAgICAgICAvLyBrZXlzIGxvb2tlZCB1cCB2aWEgaGFzT3duUHJvcGVydHkgbmVlZCB0byBiZSByZWFjdGl2ZVxuICAgICAgICAgIHZhbHVlT2JzZXJ2ZWQob3JpZ2luYWxUYXJnZXQsIGtleSk7XG4gICAgICAgICAgbGV0IGRlc2MgPSBnZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob3JpZ2luYWxUYXJnZXQsIGtleSk7XG4gICAgICAgICAgaWYgKGlzVW5kZWZpbmVkKGRlc2MpKSB7XG4gICAgICAgICAgICAgIHJldHVybiBkZXNjO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjb25zdCBzaGFkb3dEZXNjcmlwdG9yID0gZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNoYWRvd1RhcmdldCwga2V5KTtcbiAgICAgICAgICBpZiAoIWlzVW5kZWZpbmVkKHNoYWRvd0Rlc2NyaXB0b3IpKSB7XG4gICAgICAgICAgICAgIHJldHVybiBzaGFkb3dEZXNjcmlwdG9yO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBOb3RlOiBieSBhY2Nlc3NpbmcgdGhlIGRlc2NyaXB0b3IsIHRoZSBrZXkgaXMgbWFya2VkIGFzIG9ic2VydmVkXG4gICAgICAgICAgLy8gYnV0IGFjY2VzcyB0byB0aGUgdmFsdWUgb3IgZ2V0dGVyIChpZiBhdmFpbGFibGUpIGNhbm5vdCBiZSBvYnNlcnZlZCxcbiAgICAgICAgICAvLyBqdXN0IGxpa2UgcmVndWxhciBtZXRob2RzLCBpbiB3aGljaCBjYXNlIHdlIGp1c3QgZG8gbm90aGluZy5cbiAgICAgICAgICBkZXNjID0gd3JhcERlc2NyaXB0b3IobWVtYnJhbmUsIGRlc2MsIHdyYXBSZWFkT25seVZhbHVlKTtcbiAgICAgICAgICBpZiAoaGFzT3duUHJvcGVydHkuY2FsbChkZXNjLCAnc2V0JykpIHtcbiAgICAgICAgICAgICAgZGVzYy5zZXQgPSB1bmRlZmluZWQ7IC8vIHJlYWRPbmx5IG1lbWJyYW5lIGRvZXMgbm90IGFsbG93IHNldHRlcnNcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCFkZXNjLmNvbmZpZ3VyYWJsZSkge1xuICAgICAgICAgICAgICAvLyBJZiBkZXNjcmlwdG9yIGZyb20gb3JpZ2luYWwgdGFyZ2V0IGlzIG5vdCBjb25maWd1cmFibGUsXG4gICAgICAgICAgICAgIC8vIFdlIG11c3QgY29weSB0aGUgd3JhcHBlZCBkZXNjcmlwdG9yIG92ZXIgdG8gdGhlIHNoYWRvdyB0YXJnZXQuXG4gICAgICAgICAgICAgIC8vIE90aGVyd2lzZSwgcHJveHkgd2lsbCB0aHJvdyBhbiBpbnZhcmlhbnQgZXJyb3IuXG4gICAgICAgICAgICAgIC8vIFRoaXMgaXMgb3VyIGxhc3QgY2hhbmNlIHRvIGxvY2sgdGhlIHZhbHVlLlxuICAgICAgICAgICAgICAvLyBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9Qcm94eS9oYW5kbGVyL2dldE93blByb3BlcnR5RGVzY3JpcHRvciNJbnZhcmlhbnRzXG4gICAgICAgICAgICAgIE9iamVjdERlZmluZVByb3BlcnR5KHNoYWRvd1RhcmdldCwga2V5LCBkZXNjKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGRlc2M7XG4gICAgICB9XG4gICAgICBwcmV2ZW50RXh0ZW5zaW9ucyhzaGFkb3dUYXJnZXQpIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICBkZWZpbmVQcm9wZXJ0eShzaGFkb3dUYXJnZXQsIGtleSwgZGVzY3JpcHRvcikge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgfVxuICBmdW5jdGlvbiBjcmVhdGVTaGFkb3dUYXJnZXQodmFsdWUpIHtcbiAgICAgIGxldCBzaGFkb3dUYXJnZXQgPSB1bmRlZmluZWQ7XG4gICAgICBpZiAoaXNBcnJheSh2YWx1ZSkpIHtcbiAgICAgICAgICBzaGFkb3dUYXJnZXQgPSBbXTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKGlzT2JqZWN0KHZhbHVlKSkge1xuICAgICAgICAgIHNoYWRvd1RhcmdldCA9IHt9O1xuICAgICAgfVxuICAgICAgcmV0dXJuIHNoYWRvd1RhcmdldDtcbiAgfVxuICBjb25zdCBPYmplY3REb3RQcm90b3R5cGUgPSBPYmplY3QucHJvdG90eXBlO1xuICBmdW5jdGlvbiBkZWZhdWx0VmFsdWVJc09ic2VydmFibGUodmFsdWUpIHtcbiAgICAgIC8vIGludGVudGlvbmFsbHkgY2hlY2tpbmcgZm9yIG51bGxcbiAgICAgIGlmICh2YWx1ZSA9PT0gbnVsbCkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIC8vIHRyZWF0IGFsbCBub24tb2JqZWN0IHR5cGVzLCBpbmNsdWRpbmcgdW5kZWZpbmVkLCBhcyBub24tb2JzZXJ2YWJsZSB2YWx1ZXNcbiAgICAgIGlmICh0eXBlb2YgdmFsdWUgIT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgICAgaWYgKGlzQXJyYXkodmFsdWUpKSB7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgICBjb25zdCBwcm90byA9IGdldFByb3RvdHlwZU9mKHZhbHVlKTtcbiAgICAgIHJldHVybiAocHJvdG8gPT09IE9iamVjdERvdFByb3RvdHlwZSB8fCBwcm90byA9PT0gbnVsbCB8fCBnZXRQcm90b3R5cGVPZihwcm90bykgPT09IG51bGwpO1xuICB9XG4gIGNvbnN0IGRlZmF1bHRWYWx1ZU9ic2VydmVkID0gKG9iaiwga2V5KSA9PiB7XG4gICAgICAvKiBkbyBub3RoaW5nICovXG4gIH07XG4gIGNvbnN0IGRlZmF1bHRWYWx1ZU11dGF0ZWQgPSAob2JqLCBrZXkpID0+IHtcbiAgICAgIC8qIGRvIG5vdGhpbmcgKi9cbiAgfTtcbiAgY29uc3QgZGVmYXVsdFZhbHVlRGlzdG9ydGlvbiA9ICh2YWx1ZSkgPT4gdmFsdWU7XG4gIGZ1bmN0aW9uIHdyYXBEZXNjcmlwdG9yKG1lbWJyYW5lLCBkZXNjcmlwdG9yLCBnZXRWYWx1ZSkge1xuICAgICAgY29uc3QgeyBzZXQsIGdldCB9ID0gZGVzY3JpcHRvcjtcbiAgICAgIGlmIChoYXNPd25Qcm9wZXJ0eS5jYWxsKGRlc2NyaXB0b3IsICd2YWx1ZScpKSB7XG4gICAgICAgICAgZGVzY3JpcHRvci52YWx1ZSA9IGdldFZhbHVlKG1lbWJyYW5lLCBkZXNjcmlwdG9yLnZhbHVlKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICAgIGlmICghaXNVbmRlZmluZWQoZ2V0KSkge1xuICAgICAgICAgICAgICBkZXNjcmlwdG9yLmdldCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIC8vIGludm9raW5nIHRoZSBvcmlnaW5hbCBnZXR0ZXIgd2l0aCB0aGUgb3JpZ2luYWwgdGFyZ2V0XG4gICAgICAgICAgICAgICAgICByZXR1cm4gZ2V0VmFsdWUobWVtYnJhbmUsIGdldC5jYWxsKHVud3JhcCh0aGlzKSkpO1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoIWlzVW5kZWZpbmVkKHNldCkpIHtcbiAgICAgICAgICAgICAgZGVzY3JpcHRvci5zZXQgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgIC8vIEF0IHRoaXMgcG9pbnQgd2UgZG9uJ3QgaGF2ZSBhIGNsZWFyIGluZGljYXRpb24gb2Ygd2hldGhlclxuICAgICAgICAgICAgICAgICAgLy8gb3Igbm90IGEgdmFsaWQgbXV0YXRpb24gd2lsbCBvY2N1ciwgd2UgZG9uJ3QgaGF2ZSB0aGUga2V5LFxuICAgICAgICAgICAgICAgICAgLy8gYW5kIHdlIGFyZSBub3Qgc3VyZSB3aHkgYW5kIGhvdyB0aGV5IGFyZSBpbnZva2luZyB0aGlzIHNldHRlci5cbiAgICAgICAgICAgICAgICAgIC8vIE5ldmVydGhlbGVzcyB3ZSBwcmVzZXJ2ZSB0aGUgb3JpZ2luYWwgc2VtYW50aWNzIGJ5IGludm9raW5nIHRoZVxuICAgICAgICAgICAgICAgICAgLy8gb3JpZ2luYWwgc2V0dGVyIHdpdGggdGhlIG9yaWdpbmFsIHRhcmdldCBhbmQgdGhlIHVud3JhcHBlZCB2YWx1ZVxuICAgICAgICAgICAgICAgICAgc2V0LmNhbGwodW53cmFwKHRoaXMpLCBtZW1icmFuZS51bndyYXBQcm94eSh2YWx1ZSkpO1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBkZXNjcmlwdG9yO1xuICB9XG4gIGNsYXNzIFJlYWN0aXZlTWVtYnJhbmUge1xuICAgICAgY29uc3RydWN0b3Iob3B0aW9ucykge1xuICAgICAgICAgIHRoaXMudmFsdWVEaXN0b3J0aW9uID0gZGVmYXVsdFZhbHVlRGlzdG9ydGlvbjtcbiAgICAgICAgICB0aGlzLnZhbHVlTXV0YXRlZCA9IGRlZmF1bHRWYWx1ZU11dGF0ZWQ7XG4gICAgICAgICAgdGhpcy52YWx1ZU9ic2VydmVkID0gZGVmYXVsdFZhbHVlT2JzZXJ2ZWQ7XG4gICAgICAgICAgdGhpcy52YWx1ZUlzT2JzZXJ2YWJsZSA9IGRlZmF1bHRWYWx1ZUlzT2JzZXJ2YWJsZTtcbiAgICAgICAgICB0aGlzLm9iamVjdEdyYXBoID0gbmV3IFdlYWtNYXAoKTtcbiAgICAgICAgICBpZiAoIWlzVW5kZWZpbmVkKG9wdGlvbnMpKSB7XG4gICAgICAgICAgICAgIGNvbnN0IHsgdmFsdWVEaXN0b3J0aW9uLCB2YWx1ZU11dGF0ZWQsIHZhbHVlT2JzZXJ2ZWQsIHZhbHVlSXNPYnNlcnZhYmxlIH0gPSBvcHRpb25zO1xuICAgICAgICAgICAgICB0aGlzLnZhbHVlRGlzdG9ydGlvbiA9IGlzRnVuY3Rpb24odmFsdWVEaXN0b3J0aW9uKSA/IHZhbHVlRGlzdG9ydGlvbiA6IGRlZmF1bHRWYWx1ZURpc3RvcnRpb247XG4gICAgICAgICAgICAgIHRoaXMudmFsdWVNdXRhdGVkID0gaXNGdW5jdGlvbih2YWx1ZU11dGF0ZWQpID8gdmFsdWVNdXRhdGVkIDogZGVmYXVsdFZhbHVlTXV0YXRlZDtcbiAgICAgICAgICAgICAgdGhpcy52YWx1ZU9ic2VydmVkID0gaXNGdW5jdGlvbih2YWx1ZU9ic2VydmVkKSA/IHZhbHVlT2JzZXJ2ZWQgOiBkZWZhdWx0VmFsdWVPYnNlcnZlZDtcbiAgICAgICAgICAgICAgdGhpcy52YWx1ZUlzT2JzZXJ2YWJsZSA9IGlzRnVuY3Rpb24odmFsdWVJc09ic2VydmFibGUpID8gdmFsdWVJc09ic2VydmFibGUgOiBkZWZhdWx0VmFsdWVJc09ic2VydmFibGU7XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgICAgZ2V0UHJveHkodmFsdWUpIHtcbiAgICAgICAgICBjb25zdCB1bndyYXBwZWRWYWx1ZSA9IHVud3JhcCh2YWx1ZSk7XG4gICAgICAgICAgY29uc3QgZGlzdG9ydGVkID0gdGhpcy52YWx1ZURpc3RvcnRpb24odW53cmFwcGVkVmFsdWUpO1xuICAgICAgICAgIGlmICh0aGlzLnZhbHVlSXNPYnNlcnZhYmxlKGRpc3RvcnRlZCkpIHtcbiAgICAgICAgICAgICAgY29uc3QgbyA9IHRoaXMuZ2V0UmVhY3RpdmVTdGF0ZSh1bndyYXBwZWRWYWx1ZSwgZGlzdG9ydGVkKTtcbiAgICAgICAgICAgICAgLy8gd2hlbiB0cnlpbmcgdG8gZXh0cmFjdCB0aGUgd3JpdGFibGUgdmVyc2lvbiBvZiBhIHJlYWRvbmx5XG4gICAgICAgICAgICAgIC8vIHdlIHJldHVybiB0aGUgcmVhZG9ubHkuXG4gICAgICAgICAgICAgIHJldHVybiBvLnJlYWRPbmx5ID09PSB2YWx1ZSA/IHZhbHVlIDogby5yZWFjdGl2ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGRpc3RvcnRlZDtcbiAgICAgIH1cbiAgICAgIGdldFJlYWRPbmx5UHJveHkodmFsdWUpIHtcbiAgICAgICAgICB2YWx1ZSA9IHVud3JhcCh2YWx1ZSk7XG4gICAgICAgICAgY29uc3QgZGlzdG9ydGVkID0gdGhpcy52YWx1ZURpc3RvcnRpb24odmFsdWUpO1xuICAgICAgICAgIGlmICh0aGlzLnZhbHVlSXNPYnNlcnZhYmxlKGRpc3RvcnRlZCkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UmVhY3RpdmVTdGF0ZSh2YWx1ZSwgZGlzdG9ydGVkKS5yZWFkT25seTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGRpc3RvcnRlZDtcbiAgICAgIH1cbiAgICAgIHVud3JhcFByb3h5KHApIHtcbiAgICAgICAgICByZXR1cm4gdW53cmFwKHApO1xuICAgICAgfVxuICAgICAgZ2V0UmVhY3RpdmVTdGF0ZSh2YWx1ZSwgZGlzdG9ydGVkVmFsdWUpIHtcbiAgICAgICAgICBjb25zdCB7IG9iamVjdEdyYXBoLCB9ID0gdGhpcztcbiAgICAgICAgICBsZXQgcmVhY3RpdmVTdGF0ZSA9IG9iamVjdEdyYXBoLmdldChkaXN0b3J0ZWRWYWx1ZSk7XG4gICAgICAgICAgaWYgKHJlYWN0aXZlU3RhdGUpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHJlYWN0aXZlU3RhdGU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnN0IG1lbWJyYW5lID0gdGhpcztcbiAgICAgICAgICByZWFjdGl2ZVN0YXRlID0ge1xuICAgICAgICAgICAgICBnZXQgcmVhY3RpdmUoKSB7XG4gICAgICAgICAgICAgICAgICBjb25zdCByZWFjdGl2ZUhhbmRsZXIgPSBuZXcgUmVhY3RpdmVQcm94eUhhbmRsZXIobWVtYnJhbmUsIGRpc3RvcnRlZFZhbHVlKTtcbiAgICAgICAgICAgICAgICAgIC8vIGNhY2hpbmcgdGhlIHJlYWN0aXZlIHByb3h5IGFmdGVyIHRoZSBmaXJzdCB0aW1lIGl0IGlzIGFjY2Vzc2VkXG4gICAgICAgICAgICAgICAgICBjb25zdCBwcm94eSA9IG5ldyBQcm94eShjcmVhdGVTaGFkb3dUYXJnZXQoZGlzdG9ydGVkVmFsdWUpLCByZWFjdGl2ZUhhbmRsZXIpO1xuICAgICAgICAgICAgICAgICAgcmVnaXN0ZXJQcm94eShwcm94eSwgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgT2JqZWN0RGVmaW5lUHJvcGVydHkodGhpcywgJ3JlYWN0aXZlJywgeyB2YWx1ZTogcHJveHkgfSk7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gcHJveHk7XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGdldCByZWFkT25seSgpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnN0IHJlYWRPbmx5SGFuZGxlciA9IG5ldyBSZWFkT25seUhhbmRsZXIobWVtYnJhbmUsIGRpc3RvcnRlZFZhbHVlKTtcbiAgICAgICAgICAgICAgICAgIC8vIGNhY2hpbmcgdGhlIHJlYWRPbmx5IHByb3h5IGFmdGVyIHRoZSBmaXJzdCB0aW1lIGl0IGlzIGFjY2Vzc2VkXG4gICAgICAgICAgICAgICAgICBjb25zdCBwcm94eSA9IG5ldyBQcm94eShjcmVhdGVTaGFkb3dUYXJnZXQoZGlzdG9ydGVkVmFsdWUpLCByZWFkT25seUhhbmRsZXIpO1xuICAgICAgICAgICAgICAgICAgcmVnaXN0ZXJQcm94eShwcm94eSwgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgT2JqZWN0RGVmaW5lUHJvcGVydHkodGhpcywgJ3JlYWRPbmx5JywgeyB2YWx1ZTogcHJveHkgfSk7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gcHJveHk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9O1xuICAgICAgICAgIG9iamVjdEdyYXBoLnNldChkaXN0b3J0ZWRWYWx1ZSwgcmVhY3RpdmVTdGF0ZSk7XG4gICAgICAgICAgcmV0dXJuIHJlYWN0aXZlU3RhdGU7XG4gICAgICB9XG4gIH1cbiAgLyoqIHZlcnNpb246IDAuMjYuMCAqL1xuXG4gIGZ1bmN0aW9uIHdyYXAoZGF0YSwgbXV0YXRpb25DYWxsYmFjaykge1xuXG4gICAgbGV0IG1lbWJyYW5lID0gbmV3IFJlYWN0aXZlTWVtYnJhbmUoe1xuICAgICAgdmFsdWVNdXRhdGVkKHRhcmdldCwga2V5KSB7XG4gICAgICAgIG11dGF0aW9uQ2FsbGJhY2sodGFyZ2V0LCBrZXkpO1xuICAgICAgfVxuXG4gICAgfSk7XG4gICAgcmV0dXJuIHtcbiAgICAgIGRhdGE6IG1lbWJyYW5lLmdldFByb3h5KGRhdGEpLFxuICAgICAgbWVtYnJhbmU6IG1lbWJyYW5lXG4gICAgfTtcbiAgfVxuICBmdW5jdGlvbiB1bndyYXAkMShtZW1icmFuZSwgb2JzZXJ2YWJsZSkge1xuICAgIGxldCB1bndyYXBwZWREYXRhID0gbWVtYnJhbmUudW53cmFwUHJveHkob2JzZXJ2YWJsZSk7XG4gICAgbGV0IGNvcHkgPSB7fTtcbiAgICBPYmplY3Qua2V5cyh1bndyYXBwZWREYXRhKS5mb3JFYWNoKGtleSA9PiB7XG4gICAgICBpZiAoWyckZWwnLCAnJHJlZnMnLCAnJG5leHRUaWNrJywgJyR3YXRjaCddLmluY2x1ZGVzKGtleSkpIHJldHVybjtcbiAgICAgIGNvcHlba2V5XSA9IHVud3JhcHBlZERhdGFba2V5XTtcbiAgICB9KTtcbiAgICByZXR1cm4gY29weTtcbiAgfVxuXG4gIGNsYXNzIENvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoZWwsIGNvbXBvbmVudEZvckNsb25lID0gbnVsbCkge1xuICAgICAgdGhpcy4kZWwgPSBlbDtcbiAgICAgIGNvbnN0IGRhdGFBdHRyID0gdGhpcy4kZWwuZ2V0QXR0cmlidXRlKCd4LWRhdGEnKTtcbiAgICAgIGNvbnN0IGRhdGFFeHByZXNzaW9uID0gZGF0YUF0dHIgPT09ICcnID8gJ3t9JyA6IGRhdGFBdHRyO1xuICAgICAgY29uc3QgaW5pdEV4cHJlc3Npb24gPSB0aGlzLiRlbC5nZXRBdHRyaWJ1dGUoJ3gtaW5pdCcpO1xuICAgICAgbGV0IGRhdGFFeHRyYXMgPSB7XG4gICAgICAgICRlbDogdGhpcy4kZWxcbiAgICAgIH07XG4gICAgICBsZXQgY2Fub25pY2FsQ29tcG9uZW50RWxlbWVudFJlZmVyZW5jZSA9IGNvbXBvbmVudEZvckNsb25lID8gY29tcG9uZW50Rm9yQ2xvbmUuJGVsIDogdGhpcy4kZWw7XG4gICAgICBPYmplY3QuZW50cmllcyhBbHBpbmUubWFnaWNQcm9wZXJ0aWVzKS5mb3JFYWNoKChbbmFtZSwgY2FsbGJhY2tdKSA9PiB7XG4gICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShkYXRhRXh0cmFzLCBgJCR7bmFtZX1gLCB7XG4gICAgICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgICAgICByZXR1cm4gY2FsbGJhY2soY2Fub25pY2FsQ29tcG9uZW50RWxlbWVudFJlZmVyZW5jZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgICAgdGhpcy51bm9ic2VydmVkRGF0YSA9IGNvbXBvbmVudEZvckNsb25lID8gY29tcG9uZW50Rm9yQ2xvbmUuZ2V0VW5vYnNlcnZlZERhdGEoKSA6IHNhZmVyRXZhbChlbCwgZGF0YUV4cHJlc3Npb24sIGRhdGFFeHRyYXMpO1xuICAgICAgLy8gQ29uc3RydWN0IGEgUHJveHktYmFzZWQgb2JzZXJ2YWJsZS4gVGhpcyB3aWxsIGJlIHVzZWQgdG8gaGFuZGxlIHJlYWN0aXZpdHkuXG5cbiAgICAgIGxldCB7XG4gICAgICAgIG1lbWJyYW5lLFxuICAgICAgICBkYXRhXG4gICAgICB9ID0gdGhpcy53cmFwRGF0YUluT2JzZXJ2YWJsZSh0aGlzLnVub2JzZXJ2ZWREYXRhKTtcbiAgICAgIHRoaXMuJGRhdGEgPSBkYXRhO1xuICAgICAgdGhpcy5tZW1icmFuZSA9IG1lbWJyYW5lOyAvLyBBZnRlciBtYWtpbmcgdXNlci1zdXBwbGllZCBkYXRhIG1ldGhvZHMgcmVhY3RpdmUsIHdlIGNhbiBub3cgYWRkXG4gICAgICAvLyBvdXIgbWFnaWMgcHJvcGVydGllcyB0byB0aGUgb3JpZ2luYWwgZGF0YSBmb3IgYWNjZXNzLlxuXG4gICAgICB0aGlzLnVub2JzZXJ2ZWREYXRhLiRlbCA9IHRoaXMuJGVsO1xuICAgICAgdGhpcy51bm9ic2VydmVkRGF0YS4kcmVmcyA9IHRoaXMuZ2V0UmVmc1Byb3h5KCk7XG4gICAgICB0aGlzLm5leHRUaWNrU3RhY2sgPSBbXTtcblxuICAgICAgdGhpcy51bm9ic2VydmVkRGF0YS4kbmV4dFRpY2sgPSBjYWxsYmFjayA9PiB7XG4gICAgICAgIHRoaXMubmV4dFRpY2tTdGFjay5wdXNoKGNhbGxiYWNrKTtcbiAgICAgIH07XG5cbiAgICAgIHRoaXMud2F0Y2hlcnMgPSB7fTtcblxuICAgICAgdGhpcy51bm9ic2VydmVkRGF0YS4kd2F0Y2ggPSAocHJvcGVydHksIGNhbGxiYWNrKSA9PiB7XG4gICAgICAgIGlmICghdGhpcy53YXRjaGVyc1twcm9wZXJ0eV0pIHRoaXMud2F0Y2hlcnNbcHJvcGVydHldID0gW107XG4gICAgICAgIHRoaXMud2F0Y2hlcnNbcHJvcGVydHldLnB1c2goY2FsbGJhY2spO1xuICAgICAgfTtcbiAgICAgIC8qIE1PREVSTi1PTkxZOlNUQVJUICovXG4gICAgICAvLyBXZSByZW1vdmUgdGhpcyBwaWVjZSBvZiBjb2RlIGZyb20gdGhlIGxlZ2FjeSBidWlsZC5cbiAgICAgIC8vIEluIElFMTEsIHdlIGhhdmUgYWxyZWFkeSBkZWZpbmVkIG91ciBoZWxwZXJzIGF0IHRoaXMgcG9pbnQuXG4gICAgICAvLyBSZWdpc3RlciBjdXN0b20gbWFnaWMgcHJvcGVydGllcy5cblxuXG4gICAgICBPYmplY3QuZW50cmllcyhBbHBpbmUubWFnaWNQcm9wZXJ0aWVzKS5mb3JFYWNoKChbbmFtZSwgY2FsbGJhY2tdKSA9PiB7XG4gICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLnVub2JzZXJ2ZWREYXRhLCBgJCR7bmFtZX1gLCB7XG4gICAgICAgICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICAgICAgICByZXR1cm4gY2FsbGJhY2soY2Fub25pY2FsQ29tcG9uZW50RWxlbWVudFJlZmVyZW5jZSwgdGhpcy4kZWwpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICAgIC8qIE1PREVSTi1PTkxZOkVORCAqL1xuXG4gICAgICB0aGlzLnNob3dEaXJlY3RpdmVTdGFjayA9IFtdO1xuICAgICAgdGhpcy5zaG93RGlyZWN0aXZlTGFzdEVsZW1lbnQ7XG4gICAgICBjb21wb25lbnRGb3JDbG9uZSB8fCBBbHBpbmUub25CZWZvcmVDb21wb25lbnRJbml0aWFsaXplZHMuZm9yRWFjaChjYWxsYmFjayA9PiBjYWxsYmFjayh0aGlzKSk7XG4gICAgICB2YXIgaW5pdFJldHVybmVkQ2FsbGJhY2s7IC8vIElmIHgtaW5pdCBpcyBwcmVzZW50IEFORCB3ZSBhcmVuJ3QgY2xvbmluZyAoc2tpcCB4LWluaXQgb24gY2xvbmUpXG5cbiAgICAgIGlmIChpbml0RXhwcmVzc2lvbiAmJiAhY29tcG9uZW50Rm9yQ2xvbmUpIHtcbiAgICAgICAgLy8gV2Ugd2FudCB0byBhbGxvdyBkYXRhIG1hbmlwdWxhdGlvbiwgYnV0IG5vdCB0cmlnZ2VyIERPTSB1cGRhdGVzIGp1c3QgeWV0LlxuICAgICAgICAvLyBXZSBoYXZlbid0IGV2ZW4gaW5pdGlhbGl6ZWQgdGhlIGVsZW1lbnRzIHdpdGggdGhlaXIgQWxwaW5lIGJpbmRpbmdzLiBJIG1lYW4gYydtb24uXG4gICAgICAgIHRoaXMucGF1c2VSZWFjdGl2aXR5ID0gdHJ1ZTtcbiAgICAgICAgaW5pdFJldHVybmVkQ2FsbGJhY2sgPSB0aGlzLmV2YWx1YXRlUmV0dXJuRXhwcmVzc2lvbih0aGlzLiRlbCwgaW5pdEV4cHJlc3Npb24pO1xuICAgICAgICB0aGlzLnBhdXNlUmVhY3Rpdml0eSA9IGZhbHNlO1xuICAgICAgfSAvLyBSZWdpc3RlciBhbGwgb3VyIGxpc3RlbmVycyBhbmQgc2V0IGFsbCBvdXIgYXR0cmlidXRlIGJpbmRpbmdzLlxuICAgICAgLy8gSWYgd2UncmUgY2xvbmluZyBhIGNvbXBvbmVudCwgdGhlIHRoaXJkIHBhcmFtZXRlciBlbnN1cmVzIG5vIGR1cGxpY2F0ZVxuICAgICAgLy8gZXZlbnQgbGlzdGVuZXJzIGFyZSByZWdpc3RlcmVkICh0aGUgbXV0YXRpb24gb2JzZXJ2ZXIgd2lsbCB0YWtlIGNhcmUgb2YgdGhlbSlcblxuXG4gICAgICB0aGlzLmluaXRpYWxpemVFbGVtZW50cyh0aGlzLiRlbCwgKCkgPT4ge30sIGNvbXBvbmVudEZvckNsb25lKTsgLy8gVXNlIG11dGF0aW9uIG9ic2VydmVyIHRvIGRldGVjdCBuZXcgZWxlbWVudHMgYmVpbmcgYWRkZWQgd2l0aGluIHRoaXMgY29tcG9uZW50IGF0IHJ1bi10aW1lLlxuICAgICAgLy8gQWxwaW5lJ3MganVzdCBzbyBkYXJuIGZsZXhpYmxlIGFtaXJpdGU/XG5cbiAgICAgIHRoaXMubGlzdGVuRm9yTmV3RWxlbWVudHNUb0luaXRpYWxpemUoKTtcblxuICAgICAgaWYgKHR5cGVvZiBpbml0UmV0dXJuZWRDYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAvLyBSdW4gdGhlIGNhbGxiYWNrIHJldHVybmVkIGZyb20gdGhlIFwieC1pbml0XCIgaG9vayB0byBhbGxvdyB0aGUgdXNlciB0byBkbyBzdHVmZiBhZnRlclxuICAgICAgICAvLyBBbHBpbmUncyBnb3QgaXQncyBncnViYnkgbGl0dGxlIHBhd3MgYWxsIG92ZXIgZXZlcnl0aGluZy5cbiAgICAgICAgaW5pdFJldHVybmVkQ2FsbGJhY2suY2FsbCh0aGlzLiRkYXRhKTtcbiAgICAgIH1cblxuICAgICAgY29tcG9uZW50Rm9yQ2xvbmUgfHwgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIEFscGluZS5vbkNvbXBvbmVudEluaXRpYWxpemVkcy5mb3JFYWNoKGNhbGxiYWNrID0+IGNhbGxiYWNrKHRoaXMpKTtcbiAgICAgIH0sIDApO1xuICAgIH1cblxuICAgIGdldFVub2JzZXJ2ZWREYXRhKCkge1xuICAgICAgcmV0dXJuIHVud3JhcCQxKHRoaXMubWVtYnJhbmUsIHRoaXMuJGRhdGEpO1xuICAgIH1cblxuICAgIHdyYXBEYXRhSW5PYnNlcnZhYmxlKGRhdGEpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgIGxldCB1cGRhdGVEb20gPSBkZWJvdW5jZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHNlbGYudXBkYXRlRWxlbWVudHMoc2VsZi4kZWwpO1xuICAgICAgfSwgMCk7XG4gICAgICByZXR1cm4gd3JhcChkYXRhLCAodGFyZ2V0LCBrZXkpID0+IHtcbiAgICAgICAgaWYgKHNlbGYud2F0Y2hlcnNba2V5XSkge1xuICAgICAgICAgIC8vIElmIHRoZXJlJ3MgYSB3YXRjaGVyIGZvciB0aGlzIHNwZWNpZmljIGtleSwgcnVuIGl0LlxuICAgICAgICAgIHNlbGYud2F0Y2hlcnNba2V5XS5mb3JFYWNoKGNhbGxiYWNrID0+IGNhbGxiYWNrKHRhcmdldFtrZXldKSk7XG4gICAgICAgIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheSh0YXJnZXQpKSB7XG4gICAgICAgICAgLy8gQXJyYXlzIGFyZSBzcGVjaWFsIGNhc2VzLCBpZiBhbnkgb2YgdGhlIGl0ZW1zIGNoYW5nZSwgd2UgY29uc2lkZXIgdGhlIGFycmF5IGFzIG11dGF0ZWQuXG4gICAgICAgICAgT2JqZWN0LmtleXMoc2VsZi53YXRjaGVycykuZm9yRWFjaChmdWxsRG90Tm90YXRpb25LZXkgPT4ge1xuICAgICAgICAgICAgbGV0IGRvdE5vdGF0aW9uUGFydHMgPSBmdWxsRG90Tm90YXRpb25LZXkuc3BsaXQoJy4nKTsgLy8gSWdub3JlIGxlbmd0aCBtdXRhdGlvbnMgc2luY2UgdGhleSB3b3VsZCByZXN1bHQgaW4gZHVwbGljYXRlIGNhbGxzLlxuICAgICAgICAgICAgLy8gRm9yIGV4YW1wbGUsIHdoZW4gY2FsbGluZyBwdXNoLCB3ZSB3b3VsZCBnZXQgYSBtdXRhdGlvbiBmb3IgdGhlIGl0ZW0ncyBrZXlcbiAgICAgICAgICAgIC8vIGFuZCBhIHNlY29uZCBtdXRhdGlvbiBmb3IgdGhlIGxlbmd0aCBwcm9wZXJ0eS5cblxuICAgICAgICAgICAgaWYgKGtleSA9PT0gJ2xlbmd0aCcpIHJldHVybjtcbiAgICAgICAgICAgIGRvdE5vdGF0aW9uUGFydHMucmVkdWNlKChjb21wYXJpc29uRGF0YSwgcGFydCkgPT4ge1xuICAgICAgICAgICAgICBpZiAoT2JqZWN0LmlzKHRhcmdldCwgY29tcGFyaXNvbkRhdGFbcGFydF0pKSB7XG4gICAgICAgICAgICAgICAgc2VsZi53YXRjaGVyc1tmdWxsRG90Tm90YXRpb25LZXldLmZvckVhY2goY2FsbGJhY2sgPT4gY2FsbGJhY2sodGFyZ2V0KSk7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICByZXR1cm4gY29tcGFyaXNvbkRhdGFbcGFydF07XG4gICAgICAgICAgICB9LCBzZWxmLnVub2JzZXJ2ZWREYXRhKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBMZXQncyB3YWxrIHRocm91Z2ggdGhlIHdhdGNoZXJzIHdpdGggXCJkb3Qtbm90YXRpb25cIiAoZm9vLmJhcikgYW5kIHNlZVxuICAgICAgICAgIC8vIGlmIHRoaXMgbXV0YXRpb24gZml0cyBhbnkgb2YgdGhlbS5cbiAgICAgICAgICBPYmplY3Qua2V5cyhzZWxmLndhdGNoZXJzKS5maWx0ZXIoaSA9PiBpLmluY2x1ZGVzKCcuJykpLmZvckVhY2goZnVsbERvdE5vdGF0aW9uS2V5ID0+IHtcbiAgICAgICAgICAgIGxldCBkb3ROb3RhdGlvblBhcnRzID0gZnVsbERvdE5vdGF0aW9uS2V5LnNwbGl0KCcuJyk7IC8vIElmIHRoaXMgZG90LW5vdGF0aW9uIHdhdGNoZXIncyBsYXN0IFwicGFydFwiIGRvZXNuJ3QgbWF0Y2ggdGhlIGN1cnJlbnRcbiAgICAgICAgICAgIC8vIGtleSwgdGhlbiBza2lwIGl0IGVhcmx5IGZvciBwZXJmb3JtYW5jZSByZWFzb25zLlxuXG4gICAgICAgICAgICBpZiAoa2V5ICE9PSBkb3ROb3RhdGlvblBhcnRzW2RvdE5vdGF0aW9uUGFydHMubGVuZ3RoIC0gMV0pIHJldHVybjsgLy8gTm93LCB3YWxrIHRocm91Z2ggdGhlIGRvdC1ub3RhdGlvbiBcInBhcnRzXCIgcmVjdXJzaXZlbHkgdG8gZmluZFxuICAgICAgICAgICAgLy8gYSBtYXRjaCwgYW5kIGNhbGwgdGhlIHdhdGNoZXIgaWYgb25lJ3MgZm91bmQuXG5cbiAgICAgICAgICAgIGRvdE5vdGF0aW9uUGFydHMucmVkdWNlKChjb21wYXJpc29uRGF0YSwgcGFydCkgPT4ge1xuICAgICAgICAgICAgICBpZiAoT2JqZWN0LmlzKHRhcmdldCwgY29tcGFyaXNvbkRhdGEpKSB7XG4gICAgICAgICAgICAgICAgLy8gUnVuIHRoZSB3YXRjaGVycy5cbiAgICAgICAgICAgICAgICBzZWxmLndhdGNoZXJzW2Z1bGxEb3ROb3RhdGlvbktleV0uZm9yRWFjaChjYWxsYmFjayA9PiBjYWxsYmFjayh0YXJnZXRba2V5XSkpO1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgcmV0dXJuIGNvbXBhcmlzb25EYXRhW3BhcnRdO1xuICAgICAgICAgICAgfSwgc2VsZi51bm9ic2VydmVkRGF0YSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gLy8gRG9uJ3QgcmVhY3QgdG8gZGF0YSBjaGFuZ2VzIGZvciBjYXNlcyBsaWtlIHRoZSBgeC1jcmVhdGVkYCBob29rLlxuXG5cbiAgICAgICAgaWYgKHNlbGYucGF1c2VSZWFjdGl2aXR5KSByZXR1cm47XG4gICAgICAgIHVwZGF0ZURvbSgpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgd2Fsa0FuZFNraXBOZXN0ZWRDb21wb25lbnRzKGVsLCBjYWxsYmFjaywgaW5pdGlhbGl6ZUNvbXBvbmVudENhbGxiYWNrID0gKCkgPT4ge30pIHtcbiAgICAgIHdhbGsoZWwsIGVsID0+IHtcbiAgICAgICAgLy8gV2UndmUgaGl0IGEgY29tcG9uZW50LlxuICAgICAgICBpZiAoZWwuaGFzQXR0cmlidXRlKCd4LWRhdGEnKSkge1xuICAgICAgICAgIC8vIElmIGl0J3Mgbm90IHRoZSBjdXJyZW50IG9uZS5cbiAgICAgICAgICBpZiAoIWVsLmlzU2FtZU5vZGUodGhpcy4kZWwpKSB7XG4gICAgICAgICAgICAvLyBJbml0aWFsaXplIGl0IGlmIGl0J3Mgbm90LlxuICAgICAgICAgICAgaWYgKCFlbC5fX3gpIGluaXRpYWxpemVDb21wb25lbnRDYWxsYmFjayhlbCk7IC8vIE5vdyB3ZSdsbCBsZXQgdGhhdCBzdWItY29tcG9uZW50IGRlYWwgd2l0aCBpdHNlbGYuXG5cbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gY2FsbGJhY2soZWwpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaW5pdGlhbGl6ZUVsZW1lbnRzKHJvb3RFbCwgZXh0cmFWYXJzID0gKCkgPT4ge30sIGNvbXBvbmVudEZvckNsb25lID0gZmFsc2UpIHtcbiAgICAgIHRoaXMud2Fsa0FuZFNraXBOZXN0ZWRDb21wb25lbnRzKHJvb3RFbCwgZWwgPT4ge1xuICAgICAgICAvLyBEb24ndCB0b3VjaCBzcGF3bnMgZnJvbSBmb3IgbG9vcFxuICAgICAgICBpZiAoZWwuX194X2Zvcl9rZXkgIT09IHVuZGVmaW5lZCkgcmV0dXJuIGZhbHNlOyAvLyBEb24ndCB0b3VjaCBzcGF3bnMgZnJvbSBpZiBkaXJlY3RpdmVzXG5cbiAgICAgICAgaWYgKGVsLl9feF9pbnNlcnRlZF9tZSAhPT0gdW5kZWZpbmVkKSByZXR1cm4gZmFsc2U7XG4gICAgICAgIHRoaXMuaW5pdGlhbGl6ZUVsZW1lbnQoZWwsIGV4dHJhVmFycywgY29tcG9uZW50Rm9yQ2xvbmUgPyBmYWxzZSA6IHRydWUpO1xuICAgICAgfSwgZWwgPT4ge1xuICAgICAgICBpZiAoIWNvbXBvbmVudEZvckNsb25lKSBlbC5fX3ggPSBuZXcgQ29tcG9uZW50KGVsKTtcbiAgICAgIH0pO1xuICAgICAgdGhpcy5leGVjdXRlQW5kQ2xlYXJSZW1haW5pbmdTaG93RGlyZWN0aXZlU3RhY2soKTtcbiAgICAgIHRoaXMuZXhlY3V0ZUFuZENsZWFyTmV4dFRpY2tTdGFjayhyb290RWwpO1xuICAgIH1cblxuICAgIGluaXRpYWxpemVFbGVtZW50KGVsLCBleHRyYVZhcnMsIHNob3VsZFJlZ2lzdGVyTGlzdGVuZXJzID0gdHJ1ZSkge1xuICAgICAgLy8gVG8gc3VwcG9ydCBjbGFzcyBhdHRyaWJ1dGUgbWVyZ2luZywgd2UgaGF2ZSB0byBrbm93IHdoYXQgdGhlIGVsZW1lbnQnc1xuICAgICAgLy8gb3JpZ2luYWwgY2xhc3MgYXR0cmlidXRlIGxvb2tlZCBsaWtlIGZvciByZWZlcmVuY2UuXG4gICAgICBpZiAoZWwuaGFzQXR0cmlidXRlKCdjbGFzcycpICYmIGdldFhBdHRycyhlbCwgdGhpcykubGVuZ3RoID4gMCkge1xuICAgICAgICBlbC5fX3hfb3JpZ2luYWxfY2xhc3NlcyA9IGNvbnZlcnRDbGFzc1N0cmluZ1RvQXJyYXkoZWwuZ2V0QXR0cmlidXRlKCdjbGFzcycpKTtcbiAgICAgIH1cblxuICAgICAgc2hvdWxkUmVnaXN0ZXJMaXN0ZW5lcnMgJiYgdGhpcy5yZWdpc3Rlckxpc3RlbmVycyhlbCwgZXh0cmFWYXJzKTtcbiAgICAgIHRoaXMucmVzb2x2ZUJvdW5kQXR0cmlidXRlcyhlbCwgdHJ1ZSwgZXh0cmFWYXJzKTtcbiAgICB9XG5cbiAgICB1cGRhdGVFbGVtZW50cyhyb290RWwsIGV4dHJhVmFycyA9ICgpID0+IHt9KSB7XG4gICAgICB0aGlzLndhbGtBbmRTa2lwTmVzdGVkQ29tcG9uZW50cyhyb290RWwsIGVsID0+IHtcbiAgICAgICAgLy8gRG9uJ3QgdG91Y2ggc3Bhd25zIGZyb20gZm9yIGxvb3AgKGFuZCBjaGVjayBpZiB0aGUgcm9vdCBpcyBhY3R1YWxseSBhIGZvciBsb29wIGluIGEgcGFyZW50LCBkb24ndCBza2lwIGl0LilcbiAgICAgICAgaWYgKGVsLl9feF9mb3Jfa2V5ICE9PSB1bmRlZmluZWQgJiYgIWVsLmlzU2FtZU5vZGUodGhpcy4kZWwpKSByZXR1cm4gZmFsc2U7XG4gICAgICAgIHRoaXMudXBkYXRlRWxlbWVudChlbCwgZXh0cmFWYXJzKTtcbiAgICAgIH0sIGVsID0+IHtcbiAgICAgICAgZWwuX194ID0gbmV3IENvbXBvbmVudChlbCk7XG4gICAgICB9KTtcbiAgICAgIHRoaXMuZXhlY3V0ZUFuZENsZWFyUmVtYWluaW5nU2hvd0RpcmVjdGl2ZVN0YWNrKCk7XG4gICAgICB0aGlzLmV4ZWN1dGVBbmRDbGVhck5leHRUaWNrU3RhY2socm9vdEVsKTtcbiAgICB9XG5cbiAgICBleGVjdXRlQW5kQ2xlYXJOZXh0VGlja1N0YWNrKGVsKSB7XG4gICAgICAvLyBTa2lwIHNwYXducyBmcm9tIGFscGluZSBkaXJlY3RpdmVzXG4gICAgICBpZiAoZWwgPT09IHRoaXMuJGVsICYmIHRoaXMubmV4dFRpY2tTdGFjay5sZW5ndGggPiAwKSB7XG4gICAgICAgIC8vIFdlIHJ1biB0aGUgdGljayBzdGFjayBhZnRlciB0aGUgbmV4dCBmcmFtZSB0byBhbGxvdyBhbnlcbiAgICAgICAgLy8gcnVubmluZyB0cmFuc2l0aW9ucyB0byBwYXNzIHRoZSBpbml0aWFsIHNob3cgc3RhZ2UuXG4gICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZSgoKSA9PiB7XG4gICAgICAgICAgd2hpbGUgKHRoaXMubmV4dFRpY2tTdGFjay5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICB0aGlzLm5leHRUaWNrU3RhY2suc2hpZnQoKSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgZXhlY3V0ZUFuZENsZWFyUmVtYWluaW5nU2hvd0RpcmVjdGl2ZVN0YWNrKCkge1xuICAgICAgLy8gVGhlIGdvYWwgaGVyZSBpcyB0byBzdGFydCBhbGwgdGhlIHgtc2hvdyB0cmFuc2l0aW9uc1xuICAgICAgLy8gYW5kIGJ1aWxkIGEgbmVzdGVkIHByb21pc2UgY2hhaW4gc28gdGhhdCBlbGVtZW50c1xuICAgICAgLy8gb25seSBoaWRlIHdoZW4gdGhlIGNoaWxkcmVuIGFyZSBmaW5pc2hlZCBoaWRpbmcuXG4gICAgICB0aGlzLnNob3dEaXJlY3RpdmVTdGFjay5yZXZlcnNlKCkubWFwKGhhbmRsZXIgPT4ge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgIGhhbmRsZXIocmVzb2x2ZSwgcmVqZWN0KTtcbiAgICAgICAgfSk7XG4gICAgICB9KS5yZWR1Y2UoKHByb21pc2VDaGFpbiwgcHJvbWlzZSkgPT4ge1xuICAgICAgICByZXR1cm4gcHJvbWlzZUNoYWluLnRoZW4oKCkgPT4ge1xuICAgICAgICAgIHJldHVybiBwcm9taXNlLnRoZW4oZmluaXNoRWxlbWVudCA9PiB7XG4gICAgICAgICAgICBmaW5pc2hFbGVtZW50KCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgfSwgUHJvbWlzZS5yZXNvbHZlKCgpID0+IHt9KSkuY2F0Y2goZSA9PiB7XG4gICAgICAgIGlmIChlICE9PSBUUkFOU0lUSU9OX0NBTkNFTExFRCkgdGhyb3cgZTtcbiAgICAgIH0pOyAvLyBXZSd2ZSBwcm9jZXNzZWQgdGhlIGhhbmRsZXIgc3RhY2suIGxldCdzIGNsZWFyIGl0LlxuXG4gICAgICB0aGlzLnNob3dEaXJlY3RpdmVTdGFjayA9IFtdO1xuICAgICAgdGhpcy5zaG93RGlyZWN0aXZlTGFzdEVsZW1lbnQgPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgdXBkYXRlRWxlbWVudChlbCwgZXh0cmFWYXJzKSB7XG4gICAgICB0aGlzLnJlc29sdmVCb3VuZEF0dHJpYnV0ZXMoZWwsIGZhbHNlLCBleHRyYVZhcnMpO1xuICAgIH1cblxuICAgIHJlZ2lzdGVyTGlzdGVuZXJzKGVsLCBleHRyYVZhcnMpIHtcbiAgICAgIGdldFhBdHRycyhlbCwgdGhpcykuZm9yRWFjaCgoe1xuICAgICAgICB0eXBlLFxuICAgICAgICB2YWx1ZSxcbiAgICAgICAgbW9kaWZpZXJzLFxuICAgICAgICBleHByZXNzaW9uXG4gICAgICB9KSA9PiB7XG4gICAgICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICAgIGNhc2UgJ29uJzpcbiAgICAgICAgICAgIHJlZ2lzdGVyTGlzdGVuZXIodGhpcywgZWwsIHZhbHVlLCBtb2RpZmllcnMsIGV4cHJlc3Npb24sIGV4dHJhVmFycyk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgIGNhc2UgJ21vZGVsJzpcbiAgICAgICAgICAgIHJlZ2lzdGVyTW9kZWxMaXN0ZW5lcih0aGlzLCBlbCwgbW9kaWZpZXJzLCBleHByZXNzaW9uLCBleHRyYVZhcnMpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIHJlc29sdmVCb3VuZEF0dHJpYnV0ZXMoZWwsIGluaXRpYWxVcGRhdGUgPSBmYWxzZSwgZXh0cmFWYXJzKSB7XG4gICAgICBsZXQgYXR0cnMgPSBnZXRYQXR0cnMoZWwsIHRoaXMpO1xuICAgICAgYXR0cnMuZm9yRWFjaCgoe1xuICAgICAgICB0eXBlLFxuICAgICAgICB2YWx1ZSxcbiAgICAgICAgbW9kaWZpZXJzLFxuICAgICAgICBleHByZXNzaW9uXG4gICAgICB9KSA9PiB7XG4gICAgICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICAgIGNhc2UgJ21vZGVsJzpcbiAgICAgICAgICAgIGhhbmRsZUF0dHJpYnV0ZUJpbmRpbmdEaXJlY3RpdmUodGhpcywgZWwsICd2YWx1ZScsIGV4cHJlc3Npb24sIGV4dHJhVmFycywgdHlwZSwgbW9kaWZpZXJzKTtcbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgY2FzZSAnYmluZCc6XG4gICAgICAgICAgICAvLyBUaGUgOmtleSBiaW5kaW5nIG9uIGFuIHgtZm9yIGlzIHNwZWNpYWwsIGlnbm9yZSBpdC5cbiAgICAgICAgICAgIGlmIChlbC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgPT09ICd0ZW1wbGF0ZScgJiYgdmFsdWUgPT09ICdrZXknKSByZXR1cm47XG4gICAgICAgICAgICBoYW5kbGVBdHRyaWJ1dGVCaW5kaW5nRGlyZWN0aXZlKHRoaXMsIGVsLCB2YWx1ZSwgZXhwcmVzc2lvbiwgZXh0cmFWYXJzLCB0eXBlLCBtb2RpZmllcnMpO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBjYXNlICd0ZXh0JzpcbiAgICAgICAgICAgIHZhciBvdXRwdXQgPSB0aGlzLmV2YWx1YXRlUmV0dXJuRXhwcmVzc2lvbihlbCwgZXhwcmVzc2lvbiwgZXh0cmFWYXJzKTtcbiAgICAgICAgICAgIGhhbmRsZVRleHREaXJlY3RpdmUoZWwsIG91dHB1dCwgZXhwcmVzc2lvbik7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgIGNhc2UgJ2h0bWwnOlxuICAgICAgICAgICAgaGFuZGxlSHRtbERpcmVjdGl2ZSh0aGlzLCBlbCwgZXhwcmVzc2lvbiwgZXh0cmFWYXJzKTtcbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgY2FzZSAnc2hvdyc6XG4gICAgICAgICAgICB2YXIgb3V0cHV0ID0gdGhpcy5ldmFsdWF0ZVJldHVybkV4cHJlc3Npb24oZWwsIGV4cHJlc3Npb24sIGV4dHJhVmFycyk7XG4gICAgICAgICAgICBoYW5kbGVTaG93RGlyZWN0aXZlKHRoaXMsIGVsLCBvdXRwdXQsIG1vZGlmaWVycywgaW5pdGlhbFVwZGF0ZSk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgIGNhc2UgJ2lmJzpcbiAgICAgICAgICAgIC8vIElmIHRoaXMgZWxlbWVudCBhbHNvIGhhcyB4LWZvciBvbiBpdCwgZG9uJ3QgcHJvY2VzcyB4LWlmLlxuICAgICAgICAgICAgLy8gV2Ugd2lsbCBsZXQgdGhlIFwieC1mb3JcIiBkaXJlY3RpdmUgaGFuZGxlIHRoZSBcImlmXCJpbmcuXG4gICAgICAgICAgICBpZiAoYXR0cnMuc29tZShpID0+IGkudHlwZSA9PT0gJ2ZvcicpKSByZXR1cm47XG4gICAgICAgICAgICB2YXIgb3V0cHV0ID0gdGhpcy5ldmFsdWF0ZVJldHVybkV4cHJlc3Npb24oZWwsIGV4cHJlc3Npb24sIGV4dHJhVmFycyk7XG4gICAgICAgICAgICBoYW5kbGVJZkRpcmVjdGl2ZSh0aGlzLCBlbCwgb3V0cHV0LCBpbml0aWFsVXBkYXRlLCBleHRyYVZhcnMpO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBjYXNlICdmb3InOlxuICAgICAgICAgICAgaGFuZGxlRm9yRGlyZWN0aXZlKHRoaXMsIGVsLCBleHByZXNzaW9uLCBpbml0aWFsVXBkYXRlLCBleHRyYVZhcnMpO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBjYXNlICdjbG9hayc6XG4gICAgICAgICAgICBlbC5yZW1vdmVBdHRyaWJ1dGUoJ3gtY2xvYWsnKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBldmFsdWF0ZVJldHVybkV4cHJlc3Npb24oZWwsIGV4cHJlc3Npb24sIGV4dHJhVmFycyA9ICgpID0+IHt9KSB7XG4gICAgICByZXR1cm4gc2FmZXJFdmFsKGVsLCBleHByZXNzaW9uLCB0aGlzLiRkYXRhLCBfb2JqZWN0U3ByZWFkMihfb2JqZWN0U3ByZWFkMih7fSwgZXh0cmFWYXJzKCkpLCB7fSwge1xuICAgICAgICAkZGlzcGF0Y2g6IHRoaXMuZ2V0RGlzcGF0Y2hGdW5jdGlvbihlbClcbiAgICAgIH0pKTtcbiAgICB9XG5cbiAgICBldmFsdWF0ZUNvbW1hbmRFeHByZXNzaW9uKGVsLCBleHByZXNzaW9uLCBleHRyYVZhcnMgPSAoKSA9PiB7fSkge1xuICAgICAgcmV0dXJuIHNhZmVyRXZhbE5vUmV0dXJuKGVsLCBleHByZXNzaW9uLCB0aGlzLiRkYXRhLCBfb2JqZWN0U3ByZWFkMihfb2JqZWN0U3ByZWFkMih7fSwgZXh0cmFWYXJzKCkpLCB7fSwge1xuICAgICAgICAkZGlzcGF0Y2g6IHRoaXMuZ2V0RGlzcGF0Y2hGdW5jdGlvbihlbClcbiAgICAgIH0pKTtcbiAgICB9XG5cbiAgICBnZXREaXNwYXRjaEZ1bmN0aW9uKGVsKSB7XG4gICAgICByZXR1cm4gKGV2ZW50LCBkZXRhaWwgPSB7fSkgPT4ge1xuICAgICAgICBlbC5kaXNwYXRjaEV2ZW50KG5ldyBDdXN0b21FdmVudChldmVudCwge1xuICAgICAgICAgIGRldGFpbCxcbiAgICAgICAgICBidWJibGVzOiB0cnVlXG4gICAgICAgIH0pKTtcbiAgICAgIH07XG4gICAgfVxuXG4gICAgbGlzdGVuRm9yTmV3RWxlbWVudHNUb0luaXRpYWxpemUoKSB7XG4gICAgICBjb25zdCB0YXJnZXROb2RlID0gdGhpcy4kZWw7XG4gICAgICBjb25zdCBvYnNlcnZlck9wdGlvbnMgPSB7XG4gICAgICAgIGNoaWxkTGlzdDogdHJ1ZSxcbiAgICAgICAgYXR0cmlidXRlczogdHJ1ZSxcbiAgICAgICAgc3VidHJlZTogdHJ1ZVxuICAgICAgfTtcbiAgICAgIGNvbnN0IG9ic2VydmVyID0gbmV3IE11dGF0aW9uT2JzZXJ2ZXIobXV0YXRpb25zID0+IHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBtdXRhdGlvbnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAvLyBGaWx0ZXIgb3V0IG11dGF0aW9ucyB0cmlnZ2VyZWQgZnJvbSBjaGlsZCBjb21wb25lbnRzLlxuICAgICAgICAgIGNvbnN0IGNsb3Nlc3RQYXJlbnRDb21wb25lbnQgPSBtdXRhdGlvbnNbaV0udGFyZ2V0LmNsb3Nlc3QoJ1t4LWRhdGFdJyk7XG4gICAgICAgICAgaWYgKCEoY2xvc2VzdFBhcmVudENvbXBvbmVudCAmJiBjbG9zZXN0UGFyZW50Q29tcG9uZW50LmlzU2FtZU5vZGUodGhpcy4kZWwpKSkgY29udGludWU7XG5cbiAgICAgICAgICBpZiAobXV0YXRpb25zW2ldLnR5cGUgPT09ICdhdHRyaWJ1dGVzJyAmJiBtdXRhdGlvbnNbaV0uYXR0cmlidXRlTmFtZSA9PT0gJ3gtZGF0YScpIHtcbiAgICAgICAgICAgIGNvbnN0IHhBdHRyID0gbXV0YXRpb25zW2ldLnRhcmdldC5nZXRBdHRyaWJ1dGUoJ3gtZGF0YScpIHx8ICd7fSc7XG4gICAgICAgICAgICBjb25zdCByYXdEYXRhID0gc2FmZXJFdmFsKHRoaXMuJGVsLCB4QXR0ciwge1xuICAgICAgICAgICAgICAkZWw6IHRoaXMuJGVsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKHJhd0RhdGEpLmZvckVhY2goa2V5ID0+IHtcbiAgICAgICAgICAgICAgaWYgKHRoaXMuJGRhdGFba2V5XSAhPT0gcmF3RGF0YVtrZXldKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kZGF0YVtrZXldID0gcmF3RGF0YVtrZXldO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAobXV0YXRpb25zW2ldLmFkZGVkTm9kZXMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgbXV0YXRpb25zW2ldLmFkZGVkTm9kZXMuZm9yRWFjaChub2RlID0+IHtcbiAgICAgICAgICAgICAgaWYgKG5vZGUubm9kZVR5cGUgIT09IDEgfHwgbm9kZS5fX3hfaW5zZXJ0ZWRfbWUpIHJldHVybjtcblxuICAgICAgICAgICAgICBpZiAobm9kZS5tYXRjaGVzKCdbeC1kYXRhXScpICYmICFub2RlLl9feCkge1xuICAgICAgICAgICAgICAgIG5vZGUuX194ID0gbmV3IENvbXBvbmVudChub2RlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICB0aGlzLmluaXRpYWxpemVFbGVtZW50cyhub2RlKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICBvYnNlcnZlci5vYnNlcnZlKHRhcmdldE5vZGUsIG9ic2VydmVyT3B0aW9ucyk7XG4gICAgfVxuXG4gICAgZ2V0UmVmc1Byb3h5KCkge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgdmFyIHJlZk9iaiA9IHt9O1xuICAgICAgLy8gT25lIG9mIHRoZSBnb2FscyBvZiB0aGlzIGlzIHRvIG5vdCBob2xkIGVsZW1lbnRzIGluIG1lbW9yeSwgYnV0IHJhdGhlciByZS1ldmFsdWF0ZVxuICAgICAgLy8gdGhlIERPTSB3aGVuIHRoZSBzeXN0ZW0gbmVlZHMgc29tZXRoaW5nIGZyb20gaXQuIFRoaXMgd2F5LCB0aGUgZnJhbWV3b3JrIGlzIGZsZXhpYmxlIGFuZFxuICAgICAgLy8gZnJpZW5kbHkgdG8gb3V0c2lkZSBET00gY2hhbmdlcyBmcm9tIGxpYnJhcmllcyBsaWtlIFZ1ZS9MaXZld2lyZS5cbiAgICAgIC8vIEZvciB0aGlzIHJlYXNvbiwgSSdtIHVzaW5nIGFuIFwib24tZGVtYW5kXCIgcHJveHkgdG8gZmFrZSBhIFwiJHJlZnNcIiBvYmplY3QuXG5cbiAgICAgIHJldHVybiBuZXcgUHJveHkocmVmT2JqLCB7XG4gICAgICAgIGdldChvYmplY3QsIHByb3BlcnR5KSB7XG4gICAgICAgICAgaWYgKHByb3BlcnR5ID09PSAnJGlzQWxwaW5lUHJveHknKSByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB2YXIgcmVmOyAvLyBXZSBjYW4ndCBqdXN0IHF1ZXJ5IHRoZSBET00gYmVjYXVzZSBpdCdzIGhhcmQgdG8gZmlsdGVyIG91dCByZWZzIGluXG4gICAgICAgICAgLy8gbmVzdGVkIGNvbXBvbmVudHMuXG5cbiAgICAgICAgICBzZWxmLndhbGtBbmRTa2lwTmVzdGVkQ29tcG9uZW50cyhzZWxmLiRlbCwgZWwgPT4ge1xuICAgICAgICAgICAgaWYgKGVsLmhhc0F0dHJpYnV0ZSgneC1yZWYnKSAmJiBlbC5nZXRBdHRyaWJ1dGUoJ3gtcmVmJykgPT09IHByb3BlcnR5KSB7XG4gICAgICAgICAgICAgIHJlZiA9IGVsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiByZWY7XG4gICAgICAgIH1cblxuICAgICAgfSk7XG4gICAgfVxuXG4gIH1cblxuICBjb25zdCBBbHBpbmUgPSB7XG4gICAgdmVyc2lvbjogXCIyLjguMlwiLFxuICAgIHBhdXNlTXV0YXRpb25PYnNlcnZlcjogZmFsc2UsXG4gICAgbWFnaWNQcm9wZXJ0aWVzOiB7fSxcbiAgICBvbkNvbXBvbmVudEluaXRpYWxpemVkczogW10sXG4gICAgb25CZWZvcmVDb21wb25lbnRJbml0aWFsaXplZHM6IFtdLFxuICAgIGlnbm9yZUZvY3VzZWRGb3JWYWx1ZUJpbmRpbmc6IGZhbHNlLFxuICAgIHN0YXJ0OiBhc3luYyBmdW5jdGlvbiBzdGFydCgpIHtcbiAgICAgIGlmICghaXNUZXN0aW5nKCkpIHtcbiAgICAgICAgYXdhaXQgZG9tUmVhZHkoKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5kaXNjb3ZlckNvbXBvbmVudHMoZWwgPT4ge1xuICAgICAgICB0aGlzLmluaXRpYWxpemVDb21wb25lbnQoZWwpO1xuICAgICAgfSk7IC8vIEl0J3MgZWFzaWVyIGFuZCBtb3JlIHBlcmZvcm1hbnQgdG8ganVzdCBzdXBwb3J0IFR1cmJvbGlua3MgdGhhbiBsaXN0ZW5cbiAgICAgIC8vIHRvIE11dGF0aW9uT2JzZXJ2ZXIgbXV0YXRpb25zIGF0IHRoZSBkb2N1bWVudCBsZXZlbC5cblxuICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcInR1cmJvbGlua3M6bG9hZFwiLCAoKSA9PiB7XG4gICAgICAgIHRoaXMuZGlzY292ZXJVbmluaXRpYWxpemVkQ29tcG9uZW50cyhlbCA9PiB7XG4gICAgICAgICAgdGhpcy5pbml0aWFsaXplQ29tcG9uZW50KGVsKTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICAgIHRoaXMubGlzdGVuRm9yTmV3VW5pbml0aWFsaXplZENvbXBvbmVudHNBdFJ1blRpbWUoKTtcbiAgICB9LFxuICAgIGRpc2NvdmVyQ29tcG9uZW50czogZnVuY3Rpb24gZGlzY292ZXJDb21wb25lbnRzKGNhbGxiYWNrKSB7XG4gICAgICBjb25zdCByb290RWxzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW3gtZGF0YV0nKTtcbiAgICAgIHJvb3RFbHMuZm9yRWFjaChyb290RWwgPT4ge1xuICAgICAgICBjYWxsYmFjayhyb290RWwpO1xuICAgICAgfSk7XG4gICAgfSxcbiAgICBkaXNjb3ZlclVuaW5pdGlhbGl6ZWRDb21wb25lbnRzOiBmdW5jdGlvbiBkaXNjb3ZlclVuaW5pdGlhbGl6ZWRDb21wb25lbnRzKGNhbGxiYWNrLCBlbCA9IG51bGwpIHtcbiAgICAgIGNvbnN0IHJvb3RFbHMgPSAoZWwgfHwgZG9jdW1lbnQpLnF1ZXJ5U2VsZWN0b3JBbGwoJ1t4LWRhdGFdJyk7XG4gICAgICBBcnJheS5mcm9tKHJvb3RFbHMpLmZpbHRlcihlbCA9PiBlbC5fX3ggPT09IHVuZGVmaW5lZCkuZm9yRWFjaChyb290RWwgPT4ge1xuICAgICAgICBjYWxsYmFjayhyb290RWwpO1xuICAgICAgfSk7XG4gICAgfSxcbiAgICBsaXN0ZW5Gb3JOZXdVbmluaXRpYWxpemVkQ29tcG9uZW50c0F0UnVuVGltZTogZnVuY3Rpb24gbGlzdGVuRm9yTmV3VW5pbml0aWFsaXplZENvbXBvbmVudHNBdFJ1blRpbWUoKSB7XG4gICAgICBjb25zdCB0YXJnZXROb2RlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignYm9keScpO1xuICAgICAgY29uc3Qgb2JzZXJ2ZXJPcHRpb25zID0ge1xuICAgICAgICBjaGlsZExpc3Q6IHRydWUsXG4gICAgICAgIGF0dHJpYnV0ZXM6IHRydWUsXG4gICAgICAgIHN1YnRyZWU6IHRydWVcbiAgICAgIH07XG4gICAgICBjb25zdCBvYnNlcnZlciA9IG5ldyBNdXRhdGlvbk9ic2VydmVyKG11dGF0aW9ucyA9PiB7XG4gICAgICAgIGlmICh0aGlzLnBhdXNlTXV0YXRpb25PYnNlcnZlcikgcmV0dXJuO1xuXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbXV0YXRpb25zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgaWYgKG11dGF0aW9uc1tpXS5hZGRlZE5vZGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIG11dGF0aW9uc1tpXS5hZGRlZE5vZGVzLmZvckVhY2gobm9kZSA9PiB7XG4gICAgICAgICAgICAgIC8vIERpc2NhcmQgbm9uLWVsZW1lbnQgbm9kZXMgKGxpa2UgbGluZS1icmVha3MpXG4gICAgICAgICAgICAgIGlmIChub2RlLm5vZGVUeXBlICE9PSAxKSByZXR1cm47IC8vIERpc2NhcmQgYW55IGNoYW5nZXMgaGFwcGVuaW5nIHdpdGhpbiBhbiBleGlzdGluZyBjb21wb25lbnQuXG4gICAgICAgICAgICAgIC8vIFRoZXkgd2lsbCB0YWtlIGNhcmUgb2YgdGhlbXNlbHZlcy5cblxuICAgICAgICAgICAgICBpZiAobm9kZS5wYXJlbnRFbGVtZW50ICYmIG5vZGUucGFyZW50RWxlbWVudC5jbG9zZXN0KCdbeC1kYXRhXScpKSByZXR1cm47XG4gICAgICAgICAgICAgIHRoaXMuZGlzY292ZXJVbmluaXRpYWxpemVkQ29tcG9uZW50cyhlbCA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5pbml0aWFsaXplQ29tcG9uZW50KGVsKTtcbiAgICAgICAgICAgICAgfSwgbm9kZS5wYXJlbnRFbGVtZW50KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICBvYnNlcnZlci5vYnNlcnZlKHRhcmdldE5vZGUsIG9ic2VydmVyT3B0aW9ucyk7XG4gICAgfSxcbiAgICBpbml0aWFsaXplQ29tcG9uZW50OiBmdW5jdGlvbiBpbml0aWFsaXplQ29tcG9uZW50KGVsKSB7XG4gICAgICBpZiAoIWVsLl9feCkge1xuICAgICAgICAvLyBXcmFwIGluIGEgdHJ5L2NhdGNoIHNvIHRoYXQgd2UgZG9uJ3QgcHJldmVudCBvdGhlciBjb21wb25lbnRzXG4gICAgICAgIC8vIGZyb20gaW5pdGlhbGl6aW5nIHdoZW4gb25lIGNvbXBvbmVudCBjb250YWlucyBhbiBlcnJvci5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICBlbC5fX3ggPSBuZXcgQ29tcG9uZW50KGVsKTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIHRocm93IGVycm9yO1xuICAgICAgICAgIH0sIDApO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcbiAgICBjbG9uZTogZnVuY3Rpb24gY2xvbmUoY29tcG9uZW50LCBuZXdFbCkge1xuICAgICAgaWYgKCFuZXdFbC5fX3gpIHtcbiAgICAgICAgbmV3RWwuX194ID0gbmV3IENvbXBvbmVudChuZXdFbCwgY29tcG9uZW50KTtcbiAgICAgIH1cbiAgICB9LFxuICAgIGFkZE1hZ2ljUHJvcGVydHk6IGZ1bmN0aW9uIGFkZE1hZ2ljUHJvcGVydHkobmFtZSwgY2FsbGJhY2spIHtcbiAgICAgIHRoaXMubWFnaWNQcm9wZXJ0aWVzW25hbWVdID0gY2FsbGJhY2s7XG4gICAgfSxcbiAgICBvbkNvbXBvbmVudEluaXRpYWxpemVkOiBmdW5jdGlvbiBvbkNvbXBvbmVudEluaXRpYWxpemVkKGNhbGxiYWNrKSB7XG4gICAgICB0aGlzLm9uQ29tcG9uZW50SW5pdGlhbGl6ZWRzLnB1c2goY2FsbGJhY2spO1xuICAgIH0sXG4gICAgb25CZWZvcmVDb21wb25lbnRJbml0aWFsaXplZDogZnVuY3Rpb24gb25CZWZvcmVDb21wb25lbnRJbml0aWFsaXplZChjYWxsYmFjaykge1xuICAgICAgdGhpcy5vbkJlZm9yZUNvbXBvbmVudEluaXRpYWxpemVkcy5wdXNoKGNhbGxiYWNrKTtcbiAgICB9XG4gIH07XG5cbiAgaWYgKCFpc1Rlc3RpbmcoKSkge1xuICAgIHdpbmRvdy5BbHBpbmUgPSBBbHBpbmU7XG5cbiAgICBpZiAod2luZG93LmRlZmVyTG9hZGluZ0FscGluZSkge1xuICAgICAgd2luZG93LmRlZmVyTG9hZGluZ0FscGluZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHdpbmRvdy5BbHBpbmUuc3RhcnQoKTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB3aW5kb3cuQWxwaW5lLnN0YXJ0KCk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIEFscGluZTtcblxufSkpKTtcbiIsICJpbXBvcnQgJ2FscGluZWpzJ1xuXG4vLyBFeHRlbmQgJ1dpbmRvdycgb2JqZWN0XG5kZWNsYXJlIGdsb2JhbCB7XG4gICAgaW50ZXJmYWNlIFdpbmRvdyB7XG4gICAgICAgIG9wZW5GdWxsc2NyZWVuOmFueTtcbiAgICAgICAgY2xvc2VGdWxsc2NyZWVuOmFueTtcbiAgICB9XG59XG5cbi8vIFNlZSBodHRwczovL3d3dy53M3NjaG9vbHMuY29tL2hvd3RvL2hvd3RvX2pzX2Z1bGxzY3JlZW4uYXNwXG5jb25zdCBvcGVuRnVsbHNjcmVlbiA9IGZ1bmN0aW9uKCkge1xuICAgIGNvbnN0IGVsZW0gPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XG5cbiAgICBpZiAoZWxlbS5yZXF1ZXN0RnVsbHNjcmVlbikge1xuICAgICAgICBlbGVtLnJlcXVlc3RGdWxsc2NyZWVuKCk7XG4gICAgfVxuXG4gICAgLyogU2FmYXJpICovXG4gICAgaWYgKGVsZW0ud2Via2l0UmVxdWVzdEZ1bGxzY3JlZW4pIHtcbiAgICAgICAgZWxlbS53ZWJraXRSZXF1ZXN0RnVsbHNjcmVlbigpO1xuICAgIH1cblxuICAgIC8qIElFMTEgKi9cbiAgICBpZiAoZWxlbS5tc1JlcXVlc3RGdWxsc2NyZWVuKSB7XG4gICAgICAgIGVsZW0ubXNSZXF1ZXN0RnVsbHNjcmVlbigpO1xuICAgIH1cbn1cblxuY29uc3QgY2xvc2VGdWxsc2NyZWVuID0gZnVuY3Rpb24oKSB7XG4gICAgaWYgKGRvY3VtZW50LmV4aXRGdWxsc2NyZWVuKSB7XG4gICAgICAgIGRvY3VtZW50LmV4aXRGdWxsc2NyZWVuKCk7XG4gICAgfVxuXG4gICAgLyogU2FmYXJpICovXG4gICAgaWYgKGRvY3VtZW50LndlYmtpdEV4aXRGdWxsc2NyZWVuKSB7XG4gICAgICAgIGRvY3VtZW50LndlYmtpdEV4aXRGdWxsc2NyZWVuKCk7XG4gICAgfVxuXG4gICAgLyogSUUxMSAqL1xuICAgIGlmIChkb2N1bWVudC5tc0V4aXRGdWxsc2NyZWVuKSB7XG4gICAgICAgIGRvY3VtZW50Lm1zRXhpdEZ1bGxzY3JlZW4oKTtcbiAgICB9XG59XG5cbndpbmRvdy5vcGVuRnVsbHNjcmVlbiA9IG9wZW5GdWxsc2NyZWVuXG53aW5kb3cuY2xvc2VGdWxsc2NyZWVuID0gY2xvc2VGdWxsc2NyZWVuXG4iXSwKICAibWFwcGluZ3MiOiAiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQUE7QUFBQSxNQUFDLFVBQVUsUUFBUSxTQUFTO0FBQzFCLGVBQU8sWUFBWSxZQUFZLE9BQU8sV0FBVyxjQUFjLE9BQU8sVUFBVSxZQUNoRixPQUFPLFdBQVcsY0FBYyxPQUFPLE1BQU0sT0FBTyxXQUNuRCxVQUFTLFVBQVUsTUFBTSxPQUFPLFNBQVM7QUFBQSxTQUMxQyxTQUFPLFdBQVk7QUFBRTtBQUVyQixpQ0FBeUIsS0FBSyxLQUFLLE9BQU87QUFDeEMsY0FBSSxPQUFPLEtBQUs7QUFDZCxtQkFBTyxlQUFlLEtBQUssS0FBSztBQUFBLGNBQzlCO0FBQUEsY0FDQSxZQUFZO0FBQUEsY0FDWixjQUFjO0FBQUEsY0FDZCxVQUFVO0FBQUE7QUFBQSxpQkFFUDtBQUNMLGdCQUFJLE9BQU87QUFBQTtBQUdiLGlCQUFPO0FBQUE7QUFHVCx5QkFBaUIsUUFBUSxnQkFBZ0I7QUFDdkMsY0FBSSxPQUFPLE9BQU8sS0FBSztBQUV2QixjQUFJLE9BQU8sdUJBQXVCO0FBQ2hDLGdCQUFJLFVBQVUsT0FBTyxzQkFBc0I7QUFDM0MsZ0JBQUk7QUFBZ0Isd0JBQVUsUUFBUSxPQUFPLFNBQVUsS0FBSztBQUMxRCx1QkFBTyxPQUFPLHlCQUF5QixRQUFRLEtBQUs7QUFBQTtBQUV0RCxpQkFBSyxLQUFLLE1BQU0sTUFBTTtBQUFBO0FBR3hCLGlCQUFPO0FBQUE7QUFHVCxnQ0FBd0IsUUFBUTtBQUM5QixtQkFBUyxJQUFJLEdBQUcsSUFBSSxVQUFVLFFBQVEsS0FBSztBQUN6QyxnQkFBSSxTQUFTLFVBQVUsTUFBTSxPQUFPLFVBQVUsS0FBSztBQUVuRCxnQkFBSSxJQUFJLEdBQUc7QUFDVCxzQkFBUSxPQUFPLFNBQVMsTUFBTSxRQUFRLFNBQVUsS0FBSztBQUNuRCxnQ0FBZ0IsUUFBUSxLQUFLLE9BQU87QUFBQTtBQUFBLHVCQUU3QixPQUFPLDJCQUEyQjtBQUMzQyxxQkFBTyxpQkFBaUIsUUFBUSxPQUFPLDBCQUEwQjtBQUFBLG1CQUM1RDtBQUNMLHNCQUFRLE9BQU8sU0FBUyxRQUFRLFNBQVUsS0FBSztBQUM3Qyx1QkFBTyxlQUFlLFFBQVEsS0FBSyxPQUFPLHlCQUF5QixRQUFRO0FBQUE7QUFBQTtBQUFBO0FBS2pGLGlCQUFPO0FBQUE7QUFLVCw0QkFBb0I7QUFDbEIsaUJBQU8sSUFBSSxRQUFRLGFBQVc7QUFDNUIsZ0JBQUksU0FBUyxjQUFjLFdBQVc7QUFDcEMsdUJBQVMsaUJBQWlCLG9CQUFvQjtBQUFBLG1CQUN6QztBQUNMO0FBQUE7QUFBQTtBQUFBO0FBSU4sNkJBQXFCLE9BQU87QUFDMUIsaUJBQU8sTUFBTSxLQUFLLElBQUksSUFBSTtBQUFBO0FBRTVCLDZCQUFxQjtBQUNuQixpQkFBTyxVQUFVLFVBQVUsU0FBUyxjQUFjLFVBQVUsVUFBVSxTQUFTO0FBQUE7QUFFakYseUNBQWlDLFFBQVEsUUFBUTtBQUMvQyxpQkFBTyxVQUFVO0FBQUE7QUFFbkIseUNBQWlDLElBQUksV0FBVztBQUM5QyxjQUFJLEdBQUcsUUFBUSxrQkFBa0IsWUFBWTtBQUMzQyxvQkFBUSxLQUFLLFlBQVksd0dBQXdHO0FBQUEscUJBQ3hILEdBQUcsUUFBUSxzQkFBc0IsR0FBRztBQUM3QyxvQkFBUSxLQUFLLGdDQUFnQztBQUFBO0FBQUE7QUFHakQsMkJBQW1CLFNBQVM7QUFDMUIsaUJBQU8sUUFBUSxRQUFRLG1CQUFtQixTQUFTLFFBQVEsU0FBUyxLQUFLO0FBQUE7QUFFM0UsMkJBQW1CLFNBQVM7QUFDMUIsaUJBQU8sUUFBUSxjQUFjLFFBQVEsVUFBVSxDQUFDLE9BQU8sU0FBUyxLQUFLO0FBQUE7QUFFdkUsc0JBQWMsSUFBSSxVQUFVO0FBQzFCLGNBQUksU0FBUyxRQUFRO0FBQU87QUFDNUIsY0FBSSxPQUFPLEdBQUc7QUFFZCxpQkFBTyxNQUFNO0FBQ1gsaUJBQUssTUFBTTtBQUNYLG1CQUFPLEtBQUs7QUFBQTtBQUFBO0FBR2hCLDBCQUFrQixNQUFNLE1BQU07QUFDNUIsY0FBSTtBQUNKLGlCQUFPLFdBQVk7QUFDakIsZ0JBQUksVUFBVSxNQUNWLE9BQU87QUFFWCxnQkFBSSxRQUFRLGtCQUFpQjtBQUMzQix3QkFBVTtBQUNWLG1CQUFLLE1BQU0sU0FBUztBQUFBO0FBR3RCLHlCQUFhO0FBQ2Isc0JBQVUsV0FBVyxPQUFPO0FBQUE7QUFBQTtBQUloQyxjQUFNLGNBQWMsQ0FBQyxJQUFJLFlBQVksVUFBVTtBQUM3QyxrQkFBUSxLQUFLLGtCQUFrQjtBQUFBO0FBQUEsZUFBMEI7QUFBQSxXQUF5QjtBQUVsRixjQUFJLENBQUMsYUFBYTtBQUNoQixtQkFBTyxPQUFPLE9BQU87QUFBQSxjQUNuQjtBQUFBLGNBQ0E7QUFBQTtBQUVGLGtCQUFNO0FBQUE7QUFBQTtBQUlWLDBCQUFrQixJQUFJO0FBQUEsVUFDcEI7QUFBQSxVQUNBO0FBQUEsV0FDQztBQUNELGNBQUk7QUFDRixrQkFBTSxRQUFRO0FBQ2QsbUJBQU8saUJBQWlCLFVBQVUsTUFBTSxNQUFNLE9BQUssWUFBWSxJQUFJLFlBQVksTUFBTTtBQUFBLG1CQUM5RSxHQUFQO0FBQ0Esd0JBQVksSUFBSSxZQUFZO0FBQUE7QUFBQTtBQUloQywyQkFBbUIsSUFBSSxZQUFZLGFBQWEsNEJBQTRCLElBQUk7QUFDOUUsaUJBQU8sU0FBUyxNQUFNO0FBQ3BCLGdCQUFJLE9BQU8sZUFBZSxZQUFZO0FBQ3BDLHFCQUFPLFdBQVcsS0FBSztBQUFBO0FBR3pCLG1CQUFPLElBQUksU0FBUyxDQUFDLFNBQVMsR0FBRyxPQUFPLEtBQUssNkJBQTZCLHdEQUF3RCx3Q0FBd0MsYUFBYSxHQUFHLE9BQU8sT0FBTztBQUFBLGFBQ3ZNO0FBQUEsWUFDRDtBQUFBLFlBQ0E7QUFBQTtBQUFBO0FBR0osbUNBQTJCLElBQUksWUFBWSxhQUFhLDRCQUE0QixJQUFJO0FBQ3RGLGlCQUFPLFNBQVMsTUFBTTtBQUNwQixnQkFBSSxPQUFPLGVBQWUsWUFBWTtBQUNwQyxxQkFBTyxRQUFRLFFBQVEsV0FBVyxLQUFLLGFBQWEsMEJBQTBCO0FBQUE7QUFHaEYsZ0JBQUksZ0JBQWdCO0FBR3BCLDRCQUFnQixPQUFPLGVBQWUsV0FBa0I7QUFBQTtBQUFBO0FBQUEsZUFBSTtBQUs1RCxnQkFBSSxPQUFPLEtBQUssYUFBYSxTQUFTLGFBQWE7QUFDakQsa0JBQUksa0JBQWtCLElBQUksU0FBUyxDQUFDLGVBQWUsR0FBRyxPQUFPLEtBQUssNkJBQTZCLDhCQUE4QixnQkFBZ0IsYUFBYSxHQUFHLE9BQU8sT0FBTztBQUUzSyxrQkFBSSxPQUFPLG9CQUFvQixZQUFZO0FBQ3pDLHVCQUFPLFFBQVEsUUFBUSxnQkFBZ0IsS0FBSyxhQUFhLDBCQUEwQjtBQUFBLHFCQUM5RTtBQUNMLHVCQUFPLFFBQVE7QUFBQTtBQUFBO0FBSW5CLG1CQUFPLFFBQVEsUUFBUSxJQUFJLGNBQWMsQ0FBQyxlQUFlLEdBQUcsT0FBTyxLQUFLLDZCQUE2Qix1QkFBdUIsZ0JBQWdCLGFBQWEsR0FBRyxPQUFPLE9BQU87QUFBQSxhQUN6SztBQUFBLFlBQ0Q7QUFBQSxZQUNBO0FBQUE7QUFBQTtBQUdKLGNBQU0sVUFBVTtBQUNoQix5QkFBaUIsTUFBTTtBQUNyQixnQkFBTSxPQUFPLG9DQUFvQyxLQUFLO0FBQ3RELGlCQUFPLFFBQVEsS0FBSztBQUFBO0FBRXRCLDJCQUFtQixJQUFJLFdBQVcsTUFBTTtBQUN0QyxjQUFJLGFBQWEsTUFBTSxLQUFLLEdBQUcsWUFBWSxPQUFPLFNBQVMsSUFBSTtBQUUvRCxjQUFJLGtCQUFrQixXQUFXLE9BQU8sZUFBYSxVQUFVLFNBQVMsVUFBVTtBQUVsRixjQUFJLGlCQUFpQjtBQUNuQixnQkFBSSxlQUFlLFVBQVUsSUFBSSxnQkFBZ0IsWUFBWSxVQUFVO0FBRXZFLHlCQUFhLFdBQVcsT0FBTyxPQUFPLFFBQVEsY0FBYyxJQUFJLENBQUMsQ0FBQyxNQUFNLFdBQVcsbUJBQW1CO0FBQUEsY0FDcEc7QUFBQSxjQUNBO0FBQUE7QUFBQTtBQUlKLGNBQUk7QUFBTSxtQkFBTyxXQUFXLE9BQU8sT0FBSyxFQUFFLFNBQVM7QUFDbkQsaUJBQU8sZUFBZTtBQUFBO0FBR3hCLGdDQUF3QixZQUFZO0FBQ2xDLGNBQUksaUJBQWlCLENBQUMsUUFBUSxTQUFTLFFBQVE7QUFDL0MsaUJBQU8sV0FBVyxLQUFLLENBQUMsR0FBRyxNQUFNO0FBQy9CLGdCQUFJLFFBQVEsZUFBZSxRQUFRLEVBQUUsVUFBVSxLQUFLLGNBQWMsRUFBRTtBQUNwRSxnQkFBSSxRQUFRLGVBQWUsUUFBUSxFQUFFLFVBQVUsS0FBSyxjQUFjLEVBQUU7QUFDcEUsbUJBQU8sZUFBZSxRQUFRLFNBQVMsZUFBZSxRQUFRO0FBQUE7QUFBQTtBQUlsRSxvQ0FBNEI7QUFBQSxVQUMxQjtBQUFBLFVBQ0E7QUFBQSxXQUNDO0FBQ0QsZ0JBQU0saUJBQWlCLG9DQUFvQztBQUMzRCxnQkFBTSxZQUFZLGVBQWUsTUFBTTtBQUN2QyxnQkFBTSxhQUFhLGVBQWUsTUFBTTtBQUN4QyxnQkFBTSxZQUFZLGVBQWUsTUFBTSw0QkFBNEI7QUFDbkUsaUJBQU87QUFBQSxZQUNMLE1BQU0sWUFBWSxVQUFVLEtBQUs7QUFBQSxZQUNqQyxPQUFPLGFBQWEsV0FBVyxLQUFLO0FBQUEsWUFDcEMsV0FBVyxVQUFVLElBQUksT0FBSyxFQUFFLFFBQVEsS0FBSztBQUFBLFlBQzdDLFlBQVk7QUFBQTtBQUFBO0FBR2hCLCtCQUF1QixVQUFVO0FBRy9CLGdCQUFNLG9CQUFvQixDQUFDLFlBQVksV0FBVyxZQUFZLFlBQVksVUFBVSxRQUFRLFlBQVksYUFBYSxhQUFhLFlBQVksY0FBYyxtQkFBbUIsdUJBQXVCLGtCQUFrQixZQUFZLFlBQVksUUFBUSxTQUFTLGVBQWUsV0FBVyxTQUFTLFlBQVksU0FBUyxTQUFTO0FBQ2xVLGlCQUFPLGtCQUFrQixTQUFTO0FBQUE7QUFFcEMscURBQTZDLE1BQU07QUFDakQsY0FBSSxLQUFLLFdBQVcsTUFBTTtBQUN4QixtQkFBTyxLQUFLLFFBQVEsS0FBSztBQUFBLHFCQUNoQixLQUFLLFdBQVcsTUFBTTtBQUMvQixtQkFBTyxLQUFLLFFBQVEsS0FBSztBQUFBO0FBRzNCLGlCQUFPO0FBQUE7QUFFVCwyQ0FBbUMsV0FBVyxXQUFXLFNBQVM7QUFDaEUsaUJBQU8sVUFBVSxNQUFNLEtBQUssT0FBTztBQUFBO0FBRXJDLGNBQU0scUJBQXFCO0FBQzNCLGNBQU0sc0JBQXNCO0FBQzVCLGNBQU0sdUJBQXVCO0FBQzdCLDhCQUFzQixJQUFJLE1BQU0sUUFBUSxXQUFXLFlBQVksT0FBTztBQUVwRSxjQUFJO0FBQVcsbUJBQU87QUFFdEIsY0FBSSxHQUFHLGtCQUFrQixHQUFHLGVBQWUsU0FBUyxvQkFBb0I7QUFHdEU7QUFBQTtBQUdGLGdCQUFNLFFBQVEsVUFBVSxJQUFJLFdBQVc7QUFDdkMsZ0JBQU0sV0FBVyxVQUFVLElBQUksV0FBVyxRQUFRO0FBRWxELGNBQUksWUFBWSxTQUFTLFVBQVUsU0FBUyxlQUFlO0FBQ3pELGdCQUFJLFlBQVksU0FBUztBQUV6QixnQkFBSSxVQUFVLFNBQVMsVUFBVSxDQUFDLFVBQVUsU0FBUztBQUFPLHFCQUFPO0FBQ25FLGtCQUFNLCtCQUErQixVQUFVLFNBQVMsU0FBUyxVQUFVLFNBQVM7QUFFcEYsd0JBQVksK0JBQStCLFVBQVUsT0FBTyxDQUFDLEdBQUcsVUFBVSxRQUFRLFVBQVUsUUFBUSxVQUFVO0FBQzlHLCtCQUFtQixJQUFJLFdBQVcsTUFBTTtBQUFBLHFCQUMvQixNQUFNLEtBQUssVUFBUSxDQUFDLFNBQVMsZUFBZSxhQUFhLFNBQVMsS0FBSyxTQUFTO0FBQ3pGLGdDQUFvQixJQUFJLFdBQVcsT0FBTyxNQUFNO0FBQUEsaUJBQzNDO0FBRUw7QUFBQTtBQUFBO0FBR0osK0JBQXVCLElBQUksTUFBTSxRQUFRLFdBQVcsWUFBWSxPQUFPO0FBRXJFLGNBQUk7QUFBVyxtQkFBTztBQUV0QixjQUFJLEdBQUcsa0JBQWtCLEdBQUcsZUFBZSxTQUFTLHFCQUFxQjtBQUd2RTtBQUFBO0FBR0YsZ0JBQU0sUUFBUSxVQUFVLElBQUksV0FBVztBQUN2QyxnQkFBTSxXQUFXLFVBQVUsSUFBSSxXQUFXLFFBQVE7QUFFbEQsY0FBSSxZQUFZLFNBQVMsVUFBVSxTQUFTLGVBQWU7QUFDekQsZ0JBQUksWUFBWSxTQUFTO0FBQ3pCLGdCQUFJLFVBQVUsU0FBUyxTQUFTLENBQUMsVUFBVSxTQUFTO0FBQVEscUJBQU87QUFDbkUsa0JBQU0sK0JBQStCLFVBQVUsU0FBUyxTQUFTLFVBQVUsU0FBUztBQUNwRix3QkFBWSwrQkFBK0IsVUFBVSxPQUFPLENBQUMsR0FBRyxVQUFVLFFBQVEsVUFBVSxRQUFRLFVBQVU7QUFDOUcsZ0NBQW9CLElBQUksV0FBVyw4QkFBOEIsTUFBTTtBQUFBLHFCQUM5RCxNQUFNLEtBQUssVUFBUSxDQUFDLFNBQVMsZUFBZSxhQUFhLFNBQVMsS0FBSyxTQUFTO0FBQ3pGLGlDQUFxQixJQUFJLFdBQVcsT0FBTyxNQUFNO0FBQUEsaUJBQzVDO0FBQ0w7QUFBQTtBQUFBO0FBR0osb0NBQTRCLElBQUksV0FBVyxjQUFjLFFBQVE7QUFFL0QsZ0JBQU0sY0FBYztBQUFBLFlBQ2xCLFVBQVUsY0FBYyxXQUFXLFlBQVk7QUFBQSxZQUMvQyxRQUFRLGNBQWMsV0FBVyxVQUFVO0FBQUEsWUFDM0MsT0FBTztBQUFBLGNBQ0wsU0FBUztBQUFBLGNBQ1QsT0FBTyxjQUFjLFdBQVcsU0FBUztBQUFBO0FBQUEsWUFFM0MsUUFBUTtBQUFBLGNBQ04sU0FBUztBQUFBLGNBQ1QsT0FBTztBQUFBO0FBQUE7QUFHWCwyQkFBaUIsSUFBSSxXQUFXLGNBQWMsTUFBTTtBQUFBLGFBQUksUUFBUSxhQUFhO0FBQUE7QUFFL0UscUNBQTZCLElBQUksV0FBVyw4QkFBOEIsY0FBYyxRQUFRO0FBSTlGLGdCQUFNLFdBQVcsK0JBQStCLGNBQWMsV0FBVyxZQUFZLE9BQU8sY0FBYyxXQUFXLFlBQVksT0FBTztBQUN4SSxnQkFBTSxjQUFjO0FBQUEsWUFDbEI7QUFBQSxZQUNBLFFBQVEsY0FBYyxXQUFXLFVBQVU7QUFBQSxZQUMzQyxPQUFPO0FBQUEsY0FDTCxTQUFTO0FBQUEsY0FDVCxPQUFPO0FBQUE7QUFBQSxZQUVULFFBQVE7QUFBQSxjQUNOLFNBQVM7QUFBQSxjQUNULE9BQU8sY0FBYyxXQUFXLFNBQVM7QUFBQTtBQUFBO0FBRzdDLDJCQUFpQixJQUFJLFdBQVcsTUFBTTtBQUFBLGFBQUksY0FBYyxRQUFRLGFBQWE7QUFBQTtBQUcvRSwrQkFBdUIsV0FBVyxLQUFLLFVBQVU7QUFFL0MsY0FBSSxVQUFVLFFBQVEsU0FBUztBQUFJLG1CQUFPO0FBRTFDLGdCQUFNLFdBQVcsVUFBVSxVQUFVLFFBQVEsT0FBTztBQUNwRCxjQUFJLENBQUM7QUFBVSxtQkFBTztBQUV0QixjQUFJLFFBQVEsU0FBUztBQUluQixnQkFBSSxDQUFDLFVBQVU7QUFBVyxxQkFBTztBQUFBO0FBR25DLGNBQUksUUFBUSxZQUFZO0FBRXRCLGdCQUFJLFFBQVEsU0FBUyxNQUFNO0FBQzNCLGdCQUFJO0FBQU8scUJBQU8sTUFBTTtBQUFBO0FBRzFCLGNBQUksUUFBUSxVQUFVO0FBRXBCLGdCQUFJLENBQUMsT0FBTyxTQUFTLFFBQVEsVUFBVSxVQUFVLFNBQVMsVUFBVSxVQUFVLFFBQVEsT0FBTyxLQUFLO0FBQ2hHLHFCQUFPLENBQUMsVUFBVSxVQUFVLFVBQVUsUUFBUSxPQUFPLElBQUksS0FBSztBQUFBO0FBQUE7QUFJbEUsaUJBQU87QUFBQTtBQUdULGtDQUEwQixJQUFJLFdBQVcsT0FBTyxPQUFPLFFBQVEsYUFBYSxNQUFNO0FBRWhGLGNBQUksR0FBRyxnQkFBZ0I7QUFDckIsZUFBRyxlQUFlLFVBQVUsR0FBRyxlQUFlO0FBQUE7QUFJaEQsZ0JBQU0sZUFBZSxHQUFHLE1BQU07QUFDOUIsZ0JBQU0saUJBQWlCLEdBQUcsTUFBTTtBQUNoQyxnQkFBTSx1QkFBdUIsR0FBRyxNQUFNO0FBRXRDLGdCQUFNLGNBQWMsQ0FBQyxVQUFVLFNBQVMsY0FBYyxDQUFDLFVBQVUsU0FBUztBQUMxRSxnQkFBTSxvQkFBb0IsZUFBZSxVQUFVLFNBQVM7QUFDNUQsZ0JBQU0sa0JBQWtCLGVBQWUsVUFBVSxTQUFTO0FBSTFELGdCQUFNLFNBQVM7QUFBQSxZQUNiLFFBQVE7QUFDTixrQkFBSTtBQUFtQixtQkFBRyxNQUFNLFVBQVUsWUFBWSxNQUFNO0FBQzVELGtCQUFJO0FBQWlCLG1CQUFHLE1BQU0sWUFBWSxTQUFTLFlBQVksTUFBTSxRQUFRO0FBQUE7QUFBQSxZQUcvRSxTQUFTO0FBQ1Asa0JBQUk7QUFBaUIsbUJBQUcsTUFBTSxrQkFBa0IsWUFBWTtBQUM1RCxpQkFBRyxNQUFNLHFCQUFxQixDQUFDLG9CQUFvQixZQUFZLElBQUksa0JBQWtCLGNBQWMsSUFBSSxLQUFLLEtBQUs7QUFDakgsaUJBQUcsTUFBTSxxQkFBcUIsR0FBRyxZQUFZLFdBQVc7QUFDeEQsaUJBQUcsTUFBTSwyQkFBMkI7QUFBQTtBQUFBLFlBR3RDLE9BQU87QUFDTDtBQUFBO0FBQUEsWUFHRixNQUFNO0FBQ0osa0JBQUk7QUFBbUIsbUJBQUcsTUFBTSxVQUFVLFlBQVksT0FBTztBQUM3RCxrQkFBSTtBQUFpQixtQkFBRyxNQUFNLFlBQVksU0FBUyxZQUFZLE9BQU8sUUFBUTtBQUFBO0FBQUEsWUFHaEYsT0FBTztBQUNMO0FBQUE7QUFBQSxZQUdGLFVBQVU7QUFDUixrQkFBSTtBQUFtQixtQkFBRyxNQUFNLFVBQVU7QUFDMUMsa0JBQUk7QUFBaUIsbUJBQUcsTUFBTSxZQUFZO0FBQzFDLGtCQUFJO0FBQWlCLG1CQUFHLE1BQU0sa0JBQWtCO0FBQ2hELGlCQUFHLE1BQU0scUJBQXFCO0FBQzlCLGlCQUFHLE1BQU0scUJBQXFCO0FBQzlCLGlCQUFHLE1BQU0sMkJBQTJCO0FBQUE7QUFBQTtBQUl4QyxxQkFBVyxJQUFJLFFBQVEsTUFBTTtBQUFBO0FBRy9CLGNBQU0seUJBQXlCLENBQUMsWUFBWSxJQUFJLGNBQWM7QUFDNUQsaUJBQU8sT0FBTyxlQUFlLGFBQWEsVUFBVSx5QkFBeUIsSUFBSSxjQUFjO0FBQUE7QUFHakcscUNBQTZCLElBQUksV0FBVyxZQUFZLGNBQWMsUUFBUTtBQUM1RSxnQkFBTSxRQUFRLDBCQUEwQix1QkFBd0IsWUFBVyxLQUFLLE9BQUssRUFBRSxVQUFVLFlBQVk7QUFBQSxZQUMzRyxZQUFZO0FBQUEsYUFDWCxZQUFZLElBQUk7QUFDbkIsZ0JBQU0sYUFBYSwwQkFBMEIsdUJBQXdCLFlBQVcsS0FBSyxPQUFLLEVBQUUsVUFBVSxrQkFBa0I7QUFBQSxZQUN0SCxZQUFZO0FBQUEsYUFDWCxZQUFZLElBQUk7QUFDbkIsZ0JBQU0sV0FBVywwQkFBMEIsdUJBQXdCLFlBQVcsS0FBSyxPQUFLLEVBQUUsVUFBVSxnQkFBZ0I7QUFBQSxZQUNsSCxZQUFZO0FBQUEsYUFDWCxZQUFZLElBQUk7QUFDbkIsNEJBQWtCLElBQUksT0FBTyxZQUFZLFVBQVUsY0FBYyxNQUFNO0FBQUEsYUFBSSxvQkFBb0I7QUFBQTtBQUVqRyxzQ0FBOEIsSUFBSSxXQUFXLFlBQVksY0FBYyxRQUFRO0FBQzdFLGdCQUFNLFFBQVEsMEJBQTBCLHVCQUF3QixZQUFXLEtBQUssT0FBSyxFQUFFLFVBQVUsWUFBWTtBQUFBLFlBQzNHLFlBQVk7QUFBQSxhQUNYLFlBQVksSUFBSTtBQUNuQixnQkFBTSxhQUFhLDBCQUEwQix1QkFBd0IsWUFBVyxLQUFLLE9BQUssRUFBRSxVQUFVLGtCQUFrQjtBQUFBLFlBQ3RILFlBQVk7QUFBQSxhQUNYLFlBQVksSUFBSTtBQUNuQixnQkFBTSxXQUFXLDBCQUEwQix1QkFBd0IsWUFBVyxLQUFLLE9BQUssRUFBRSxVQUFVLGdCQUFnQjtBQUFBLFlBQ2xILFlBQVk7QUFBQSxhQUNYLFlBQVksSUFBSTtBQUNuQiw0QkFBa0IsSUFBSSxPQUFPLFlBQVksVUFBVSxNQUFNO0FBQUEsYUFBSSxjQUFjLHFCQUFxQjtBQUFBO0FBRWxHLG1DQUEyQixJQUFJLGVBQWUsY0FBYyxZQUFZLE9BQU8sT0FBTyxNQUFNLFFBQVE7QUFFbEcsY0FBSSxHQUFHLGdCQUFnQjtBQUNyQixlQUFHLGVBQWUsVUFBVSxHQUFHLGVBQWU7QUFBQTtBQUdoRCxnQkFBTSxrQkFBa0IsR0FBRyx3QkFBd0I7QUFDbkQsZ0JBQU0sU0FBUztBQUFBLFlBQ2IsUUFBUTtBQUNOLGlCQUFHLFVBQVUsSUFBSSxHQUFHO0FBQUE7QUFBQSxZQUd0QixTQUFTO0FBQ1AsaUJBQUcsVUFBVSxJQUFJLEdBQUc7QUFBQTtBQUFBLFlBR3RCLE9BQU87QUFDTDtBQUFBO0FBQUEsWUFHRixNQUFNO0FBRUosaUJBQUcsVUFBVSxPQUFPLEdBQUcsYUFBYSxPQUFPLE9BQUssQ0FBQyxnQkFBZ0IsU0FBUztBQUMxRSxpQkFBRyxVQUFVLElBQUksR0FBRztBQUFBO0FBQUEsWUFHdEIsT0FBTztBQUNMO0FBQUE7QUFBQSxZQUdGLFVBQVU7QUFDUixpQkFBRyxVQUFVLE9BQU8sR0FBRyxjQUFjLE9BQU8sT0FBSyxDQUFDLGdCQUFnQixTQUFTO0FBQzNFLGlCQUFHLFVBQVUsT0FBTyxHQUFHLFdBQVcsT0FBTyxPQUFLLENBQUMsZ0JBQWdCLFNBQVM7QUFBQTtBQUFBO0FBSTVFLHFCQUFXLElBQUksUUFBUSxNQUFNO0FBQUE7QUFFL0IsNEJBQW9CLElBQUksUUFBUSxNQUFNLFFBQVE7QUFDNUMsZ0JBQU0sU0FBUyxLQUFLLE1BQU07QUFDeEIsbUJBQU87QUFHUCxnQkFBSSxHQUFHLGFBQWE7QUFDbEIscUJBQU87QUFBQTtBQUdULG1CQUFPLEdBQUc7QUFBQTtBQUVaLGFBQUcsaUJBQWlCO0FBQUEsWUFFbEI7QUFBQSxZQUlBLFFBQVEsS0FBSyxNQUFNO0FBQ2pCLHFCQUFPO0FBQ1A7QUFBQTtBQUFBLFlBRUY7QUFBQSxZQUVBLFdBQVc7QUFBQTtBQUViLGlCQUFPO0FBQ1AsaUJBQU87QUFDUCxhQUFHLGVBQWUsWUFBWSxzQkFBc0IsTUFBTTtBQUd4RCxnQkFBSSxXQUFXLE9BQU8saUJBQWlCLElBQUksbUJBQW1CLFFBQVEsT0FBTyxJQUFJLFFBQVEsS0FBSyxPQUFPO0FBRXJHLGdCQUFJLGFBQWEsR0FBRztBQUNsQix5QkFBVyxPQUFPLGlCQUFpQixJQUFJLGtCQUFrQixRQUFRLEtBQUssT0FBTztBQUFBO0FBRy9FLG1CQUFPO0FBQ1AsZUFBRyxlQUFlLFlBQVksc0JBQXNCLE1BQU07QUFDeEQscUJBQU87QUFDUCx5QkFBVyxHQUFHLGVBQWUsUUFBUTtBQUFBO0FBQUE7QUFBQTtBQUkzQywyQkFBbUIsU0FBUztBQUMxQixpQkFBTyxDQUFDLE1BQU0sUUFBUSxZQUFZLENBQUMsTUFBTTtBQUFBO0FBSTNDLHNCQUFjLFVBQVU7QUFDdEIsY0FBSSxTQUFTO0FBQ2IsaUJBQU8sV0FBWTtBQUNqQixnQkFBSSxDQUFDLFFBQVE7QUFDWCx1QkFBUztBQUNULHVCQUFTLE1BQU0sTUFBTTtBQUFBO0FBQUE7QUFBQTtBQUszQixvQ0FBNEIsV0FBVyxZQUFZLFlBQVksZUFBZSxXQUFXO0FBQ3ZGLGtDQUF3QixZQUFZO0FBQ3BDLGNBQUksZ0JBQWdCLE9BQU8sZUFBZSxhQUFhLG1CQUFtQixVQUFVLHlCQUF5QixZQUFZLGVBQWUsbUJBQW1CO0FBQzNKLGNBQUksUUFBUSwyREFBMkQsV0FBVyxZQUFZLGVBQWU7QUFFN0csY0FBSSxZQUFZO0FBQ2hCLGdCQUFNLFFBQVEsQ0FBQyxNQUFNLFVBQVU7QUFDN0IsZ0JBQUksMEJBQTBCLDJCQUEyQixlQUFlLE1BQU0sT0FBTyxPQUFPO0FBQzVGLGdCQUFJLGFBQWEsd0JBQXdCLFdBQVcsWUFBWSxPQUFPO0FBQ3ZFLGdCQUFJLFNBQVMsaURBQWlELFVBQVUsb0JBQW9CO0FBRTVGLGdCQUFJLENBQUMsUUFBUTtBQUNYLHVCQUFTLCtCQUErQixZQUFZO0FBRXBELDJCQUFhLFFBQVEsTUFBTTtBQUFBLGlCQUFJLE1BQU07QUFBQSxpQkFBSSxXQUFXO0FBQ3BELHFCQUFPLFVBQVU7QUFDakIsd0JBQVUsbUJBQW1CLFFBQVEsTUFBTSxPQUFPO0FBQUEsbUJBQzdDO0FBRUwscUJBQU8sT0FBTztBQUNkLHFCQUFPLFVBQVU7QUFDakIsd0JBQVUsZUFBZSxRQUFRLE1BQU0sT0FBTztBQUFBO0FBR2hELHdCQUFZO0FBQ1osc0JBQVUsY0FBYztBQUFBO0FBRTFCLHNEQUE0QyxXQUFXO0FBQUE7QUFHekQsb0NBQTRCLFlBQVk7QUFDdEMsY0FBSSxnQkFBZ0I7QUFDcEIsY0FBSSxnQkFBZ0I7QUFDcEIsY0FBSSxhQUFhO0FBQ2pCLGNBQUksVUFBVSxPQUFPLFlBQVksTUFBTTtBQUN2QyxjQUFJLENBQUM7QUFBUztBQUNkLGNBQUksTUFBTTtBQUNWLGNBQUksUUFBUSxRQUFRLEdBQUc7QUFDdkIsY0FBSSxPQUFPLFFBQVEsR0FBRyxPQUFPLFFBQVEsZUFBZTtBQUNwRCxjQUFJLGdCQUFnQixLQUFLLE1BQU07QUFFL0IsY0FBSSxlQUFlO0FBQ2pCLGdCQUFJLE9BQU8sS0FBSyxRQUFRLGVBQWUsSUFBSTtBQUMzQyxnQkFBSSxRQUFRLGNBQWMsR0FBRztBQUU3QixnQkFBSSxjQUFjLElBQUk7QUFDcEIsa0JBQUksYUFBYSxjQUFjLEdBQUc7QUFBQTtBQUFBLGlCQUUvQjtBQUNMLGdCQUFJLE9BQU87QUFBQTtBQUdiLGlCQUFPO0FBQUE7QUFHVCw0Q0FBb0MsZUFBZSxNQUFNLE9BQU8sT0FBTyxXQUFXO0FBRWhGLGNBQUksaUJBQWlCLFlBQVksZUFBZSxJQUFJLGFBQWE7QUFDakUseUJBQWUsY0FBYyxRQUFRO0FBQ3JDLGNBQUksY0FBYztBQUFPLDJCQUFlLGNBQWMsU0FBUztBQUMvRCxjQUFJLGNBQWM7QUFBWSwyQkFBZSxjQUFjLGNBQWM7QUFDekUsaUJBQU87QUFBQTtBQUdULHlDQUFpQyxXQUFXLElBQUksT0FBTyx5QkFBeUI7QUFDOUUsY0FBSSxtQkFBbUIsVUFBVSxJQUFJLFdBQVcsUUFBUSxPQUFPLFVBQVEsS0FBSyxVQUFVLE9BQU87QUFFN0YsY0FBSSxDQUFDO0FBQWtCLG1CQUFPO0FBQzlCLGlCQUFPLFVBQVUseUJBQXlCLElBQUksaUJBQWlCLFlBQVksTUFBTTtBQUFBO0FBR25GLDRFQUFvRSxXQUFXLElBQUksZUFBZSxXQUFXO0FBQzNHLGNBQUksY0FBYyxVQUFVLElBQUksV0FBVyxNQUFNO0FBRWpELGNBQUksZUFBZSxDQUFDLFVBQVUseUJBQXlCLElBQUksWUFBWSxhQUFhO0FBQ2xGLG1CQUFPO0FBQUE7QUFHVCxjQUFJLFFBQVEsVUFBVSx5QkFBeUIsSUFBSSxjQUFjLE9BQU87QUFFeEUsY0FBSSxVQUFVLFVBQVUsU0FBUyxHQUFHO0FBQ2xDLG9CQUFRLE1BQU0sS0FBSyxNQUFNLE9BQU8sUUFBUSxPQUFLLElBQUk7QUFBQTtBQUduRCxpQkFBTztBQUFBO0FBR1QsZ0RBQXdDLFlBQVksV0FBVztBQUM3RCxjQUFJLFFBQVEsU0FBUyxXQUFXLFdBQVcsU0FBUztBQUNwRCxvQkFBVSxjQUFjLGFBQWEsT0FBTyxVQUFVO0FBQ3RELGlCQUFPLFVBQVU7QUFBQTtBQUduQixrRUFBMEQsUUFBUSxZQUFZO0FBQzVFLGNBQUksQ0FBQztBQUFRO0FBRWIsY0FBSSxPQUFPLGdCQUFnQjtBQUFXO0FBRXRDLGNBQUksT0FBTyxnQkFBZ0I7QUFBWSxtQkFBTztBQUc5QyxjQUFJLFlBQVk7QUFFaEIsaUJBQU8sV0FBVztBQUNoQixnQkFBSSxVQUFVLGdCQUFnQixZQUFZO0FBQ3hDLHFCQUFPLFVBQVUsY0FBYyxhQUFhLFdBQVc7QUFBQTtBQUd6RCx3QkFBWSxVQUFVLHNCQUFzQixVQUFVLG1CQUFtQixnQkFBZ0IsU0FBWSxVQUFVLHFCQUFxQjtBQUFBO0FBQUE7QUFJeEksNkRBQXFELFdBQVcsV0FBVztBQUN6RSxjQUFJLHlCQUF5QixVQUFVLHNCQUFzQixVQUFVLG1CQUFtQixnQkFBZ0IsU0FBWSxVQUFVLHFCQUFxQjtBQUVySixpQkFBTyx3QkFBd0I7QUFDN0IsZ0JBQUksa0NBQWtDO0FBQ3RDLGdCQUFJLGNBQWMsdUJBQXVCO0FBQ3pDLDBCQUFjLHdCQUF3QixNQUFNO0FBQzFDLDhDQUFnQztBQUFBLGVBQy9CLE1BQU07QUFBQSxlQUFJO0FBQ2IscUNBQXlCLGVBQWUsWUFBWSxnQkFBZ0IsU0FBWSxjQUFjO0FBQUE7QUFBQTtBQUlsRyxpREFBeUMsV0FBVyxJQUFJLFVBQVUsWUFBWSxXQUFXLFVBQVUsV0FBVztBQUM1RyxjQUFJLFFBQVEsVUFBVSx5QkFBeUIsSUFBSSxZQUFZO0FBRS9ELGNBQUksYUFBYSxTQUFTO0FBQ3hCLGdCQUFJLE9BQU8sZ0NBQWdDLFNBQVMsY0FBYyxXQUFXO0FBQUs7QUFFbEYsZ0JBQUksVUFBVSxVQUFhLE9BQU8sWUFBWSxNQUFNLE9BQU87QUFDekQsc0JBQVE7QUFBQTtBQUdWLGdCQUFJLEdBQUcsU0FBUyxTQUFTO0FBSXZCLGtCQUFJLEdBQUcsV0FBVyxVQUFVLFVBQWEsYUFBYSxRQUFRO0FBQzVELG1CQUFHLFFBQVE7QUFBQSx5QkFDRixhQUFhLFFBQVE7QUFDOUIsbUJBQUcsVUFBVSx3QkFBd0IsR0FBRyxPQUFPO0FBQUE7QUFBQSx1QkFFeEMsR0FBRyxTQUFTLFlBQVk7QUFJakMsa0JBQUksT0FBTyxVQUFVLGFBQWEsQ0FBQyxDQUFDLE1BQU0sUUFBVyxTQUFTLFVBQVUsYUFBYSxRQUFRO0FBQzNGLG1CQUFHLFFBQVEsT0FBTztBQUFBLHlCQUNULGFBQWEsUUFBUTtBQUM5QixvQkFBSSxNQUFNLFFBQVEsUUFBUTtBQUl4QixxQkFBRyxVQUFVLE1BQU0sS0FBSyxTQUFPLHdCQUF3QixLQUFLLEdBQUc7QUFBQSx1QkFDMUQ7QUFDTCxxQkFBRyxVQUFVLENBQUMsQ0FBQztBQUFBO0FBQUE7QUFBQSx1QkFHVixHQUFHLFlBQVksVUFBVTtBQUNsQywyQkFBYSxJQUFJO0FBQUEsbUJBQ1o7QUFDTCxrQkFBSSxHQUFHLFVBQVU7QUFBTztBQUN4QixpQkFBRyxRQUFRO0FBQUE7QUFBQSxxQkFFSixhQUFhLFNBQVM7QUFDL0IsZ0JBQUksTUFBTSxRQUFRLFFBQVE7QUFDeEIsb0JBQU0sa0JBQWtCLEdBQUcsd0JBQXdCO0FBQ25ELGlCQUFHLGFBQWEsU0FBUyxZQUFZLGdCQUFnQixPQUFPLFFBQVEsS0FBSztBQUFBLHVCQUNoRSxPQUFPLFVBQVUsVUFBVTtBQUdwQyxvQkFBTSwyQkFBMkIsT0FBTyxLQUFLLE9BQU8sS0FBSyxDQUFDLEdBQUcsTUFBTSxNQUFNLEtBQUssTUFBTTtBQUNwRix1Q0FBeUIsUUFBUSxnQkFBYztBQUM3QyxvQkFBSSxNQUFNLGFBQWE7QUFDckIsNENBQTBCLFlBQVksUUFBUSxlQUFhLEdBQUcsVUFBVSxJQUFJO0FBQUEsdUJBQ3ZFO0FBQ0wsNENBQTBCLFlBQVksUUFBUSxlQUFhLEdBQUcsVUFBVSxPQUFPO0FBQUE7QUFBQTtBQUFBLG1CQUc5RTtBQUNMLG9CQUFNLGtCQUFrQixHQUFHLHdCQUF3QjtBQUNuRCxvQkFBTSxhQUFhLFFBQVEsMEJBQTBCLFNBQVM7QUFDOUQsaUJBQUcsYUFBYSxTQUFTLFlBQVksZ0JBQWdCLE9BQU8sYUFBYSxLQUFLO0FBQUE7QUFBQSxpQkFFM0U7QUFDTCx1QkFBVyxVQUFVLFNBQVMsV0FBVyxVQUFVLFlBQVk7QUFFL0QsZ0JBQUksQ0FBQyxNQUFNLFFBQVcsT0FBTyxTQUFTLFFBQVE7QUFDNUMsaUJBQUcsZ0JBQWdCO0FBQUEsbUJBQ2Q7QUFDTCw0QkFBYyxZQUFZLGFBQWEsSUFBSSxVQUFVLFlBQVksYUFBYSxJQUFJLFVBQVU7QUFBQTtBQUFBO0FBQUE7QUFLbEcsOEJBQXNCLElBQUksVUFBVSxPQUFPO0FBQ3pDLGNBQUksR0FBRyxhQUFhLGFBQWEsT0FBTztBQUN0QyxlQUFHLGFBQWEsVUFBVTtBQUFBO0FBQUE7QUFJOUIsOEJBQXNCLElBQUksT0FBTztBQUMvQixnQkFBTSxvQkFBb0IsR0FBRyxPQUFPLE9BQU8sSUFBSSxZQUFTO0FBQ3RELG1CQUFPLFNBQVE7QUFBQTtBQUVqQixnQkFBTSxLQUFLLEdBQUcsU0FBUyxRQUFRLFlBQVU7QUFDdkMsbUJBQU8sV0FBVyxrQkFBa0IsU0FBUyxPQUFPLFNBQVMsT0FBTztBQUFBO0FBQUE7QUFJeEUscUNBQTZCLElBQUksUUFBUSxZQUFZO0FBRW5ELGNBQUksV0FBVyxVQUFhLE9BQU8sWUFBWSxNQUFNLE9BQU87QUFDMUQscUJBQVM7QUFBQTtBQUdYLGFBQUcsY0FBYztBQUFBO0FBR25CLHFDQUE2QixXQUFXLElBQUksWUFBWSxXQUFXO0FBQ2pFLGFBQUcsWUFBWSxVQUFVLHlCQUF5QixJQUFJLFlBQVk7QUFBQTtBQUdwRSxxQ0FBNkIsV0FBVyxJQUFJLE9BQU8sV0FBVyxnQkFBZ0IsT0FBTztBQUNuRixnQkFBTSxPQUFPLE1BQU07QUFDakIsZUFBRyxNQUFNLFVBQVU7QUFDbkIsZUFBRyxlQUFlO0FBQUE7QUFHcEIsZ0JBQU0sT0FBTyxNQUFNO0FBQ2pCLGdCQUFJLEdBQUcsTUFBTSxXQUFXLEtBQUssR0FBRyxNQUFNLFlBQVksUUFBUTtBQUN4RCxpQkFBRyxnQkFBZ0I7QUFBQSxtQkFDZDtBQUNMLGlCQUFHLE1BQU0sZUFBZTtBQUFBO0FBRzFCLGVBQUcsZUFBZTtBQUFBO0FBR3BCLGNBQUksa0JBQWtCLE1BQU07QUFDMUIsZ0JBQUksT0FBTztBQUNUO0FBQUEsbUJBQ0s7QUFDTDtBQUFBO0FBR0Y7QUFBQTtBQUdGLGdCQUFNLFNBQVMsQ0FBQyxTQUFTLFdBQVc7QUFDbEMsZ0JBQUksT0FBTztBQUNULGtCQUFJLEdBQUcsTUFBTSxZQUFZLFVBQVUsR0FBRyxnQkFBZ0I7QUFDcEQsNkJBQWEsSUFBSSxNQUFNO0FBQ3JCO0FBQUEsbUJBQ0MsUUFBUTtBQUFBO0FBR2Isc0JBQVEsTUFBTTtBQUFBO0FBQUEsbUJBQ1Q7QUFDTCxrQkFBSSxHQUFHLE1BQU0sWUFBWSxRQUFRO0FBQy9CLDhCQUFjLElBQUksTUFBTTtBQUN0QiwwQkFBUSxNQUFNO0FBQ1o7QUFBQTtBQUFBLG1CQUVELFFBQVE7QUFBQSxxQkFDTjtBQUNMLHdCQUFRLE1BQU07QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNwQixjQUFJLFVBQVUsU0FBUyxjQUFjO0FBQ25DLG1CQUFPLFlBQVUsVUFBVSxNQUFNO0FBQUE7QUFDakM7QUFBQTtBQU1GLGNBQUksVUFBVSw0QkFBNEIsQ0FBQyxVQUFVLHlCQUF5QixTQUFTLEtBQUs7QUFDMUYsc0JBQVU7QUFBQTtBQUdaLG9CQUFVLG1CQUFtQixLQUFLO0FBQ2xDLG9CQUFVLDJCQUEyQjtBQUFBO0FBR3ZDLG1DQUEyQixXQUFXLElBQUksa0JBQWtCLGVBQWUsV0FBVztBQUNwRixrQ0FBd0IsSUFBSTtBQUM1QixnQkFBTSw2QkFBNkIsR0FBRyxzQkFBc0IsR0FBRyxtQkFBbUIsb0JBQW9CO0FBRXRHLGNBQUksb0JBQXFCLEVBQUMsOEJBQThCLEdBQUcsaUJBQWlCO0FBQzFFLGtCQUFNLFFBQVEsU0FBUyxXQUFXLEdBQUcsU0FBUztBQUM5QyxlQUFHLGNBQWMsYUFBYSxPQUFPLEdBQUc7QUFDeEMseUJBQWEsR0FBRyxvQkFBb0IsTUFBTTtBQUFBLGVBQUksTUFBTTtBQUFBLGVBQUksV0FBVztBQUNuRSxzQkFBVSxtQkFBbUIsR0FBRyxvQkFBb0I7QUFDcEQsZUFBRyxtQkFBbUIsa0JBQWtCO0FBQUEscUJBQy9CLENBQUMsb0JBQW9CLDRCQUE0QjtBQUMxRCwwQkFBYyxHQUFHLG9CQUFvQixNQUFNO0FBQ3pDLGlCQUFHLG1CQUFtQjtBQUFBLGVBQ3JCLE1BQU07QUFBQSxlQUFJLFdBQVc7QUFBQTtBQUFBO0FBSTVCLGtDQUEwQixXQUFXLElBQUksT0FBTyxXQUFXLFlBQVksWUFBWSxJQUFJO0FBQ3JGLGdCQUFNLFVBQVU7QUFBQSxZQUNkLFNBQVMsVUFBVSxTQUFTO0FBQUE7QUFHOUIsY0FBSSxVQUFVLFNBQVMsVUFBVTtBQUMvQixvQkFBUSxVQUFVO0FBQUE7QUFHcEIsY0FBSSxTQUFTO0FBRWIsY0FBSSxVQUFVLFNBQVMsU0FBUztBQUM5Qiw2QkFBaUI7QUFFakIsc0JBQVUsT0FBSztBQUViLGtCQUFJLEdBQUcsU0FBUyxFQUFFO0FBQVM7QUFFM0Isa0JBQUksR0FBRyxjQUFjLEtBQUssR0FBRyxlQUFlO0FBQUc7QUFHL0MsaUNBQW1CLFdBQVcsWUFBWSxHQUFHO0FBRTdDLGtCQUFJLFVBQVUsU0FBUyxTQUFTO0FBQzlCLHlCQUFTLG9CQUFvQixPQUFPLFNBQVM7QUFBQTtBQUFBO0FBQUEsaUJBRzVDO0FBQ0wsNkJBQWlCLFVBQVUsU0FBUyxZQUFZLFNBQVMsVUFBVSxTQUFTLGNBQWMsV0FBVztBQUVyRyxzQkFBVSxPQUFLO0FBR2Isa0JBQUksbUJBQW1CLFVBQVUsbUJBQW1CLFVBQVU7QUFDNUQsb0JBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxLQUFLO0FBQy9CLGlDQUFlLG9CQUFvQixPQUFPLFNBQVM7QUFDbkQ7QUFBQTtBQUFBO0FBSUosa0JBQUksV0FBVyxRQUFRO0FBQ3JCLG9CQUFJLCtDQUErQyxHQUFHLFlBQVk7QUFDaEU7QUFBQTtBQUFBO0FBSUosa0JBQUksVUFBVSxTQUFTO0FBQVksa0JBQUU7QUFDckMsa0JBQUksVUFBVSxTQUFTO0FBQVMsa0JBQUU7QUFJbEMsa0JBQUksQ0FBQyxVQUFVLFNBQVMsV0FBVyxFQUFFLFdBQVcsSUFBSTtBQUNsRCxzQkFBTSxjQUFjLG1CQUFtQixXQUFXLFlBQVksR0FBRztBQUNqRSw0QkFBWSxLQUFLLFdBQVM7QUFDeEIsc0JBQUksVUFBVSxPQUFPO0FBQ25CLHNCQUFFO0FBQUEseUJBQ0c7QUFDTCx3QkFBSSxVQUFVLFNBQVMsU0FBUztBQUM5QixxQ0FBZSxvQkFBb0IsT0FBTyxTQUFTO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUS9ELGNBQUksVUFBVSxTQUFTLGFBQWE7QUFDbEMsZ0JBQUksZUFBZSxVQUFVLFVBQVUsUUFBUSxjQUFjLE1BQU07QUFDbkUsZ0JBQUksT0FBTyxVQUFVLGFBQWEsTUFBTSxNQUFNLE1BQU0sT0FBTyxhQUFhLE1BQU0sTUFBTSxNQUFNO0FBQzFGLHNCQUFVLFNBQVMsU0FBUztBQUFBO0FBRzlCLHlCQUFlLGlCQUFpQixPQUFPLFNBQVM7QUFBQTtBQUdsRCxvQ0FBNEIsV0FBVyxZQUFZLEdBQUcsV0FBVztBQUMvRCxpQkFBTyxVQUFVLDBCQUEwQixFQUFFLFFBQVEsWUFBWSxNQUFNO0FBQ3JFLG1CQUFPLGVBQWUsZUFBZSxJQUFJLGNBQWMsSUFBSTtBQUFBLGNBQ3pELFVBQVU7QUFBQTtBQUFBO0FBQUE7QUFLaEIsNEJBQW9CLE9BQU87QUFDekIsaUJBQU8sQ0FBQyxXQUFXLFNBQVMsU0FBUztBQUFBO0FBR3ZDLGdFQUF3RCxHQUFHLFdBQVc7QUFDcEUsY0FBSSxlQUFlLFVBQVUsT0FBTyxPQUFLO0FBQ3ZDLG1CQUFPLENBQUMsQ0FBQyxVQUFVLFlBQVksV0FBVyxRQUFRLFNBQVM7QUFBQTtBQUc3RCxjQUFJLGFBQWEsU0FBUyxhQUFhO0FBQ3JDLGdCQUFJLGdCQUFnQixhQUFhLFFBQVE7QUFDekMseUJBQWEsT0FBTyxlQUFlLFVBQVcsY0FBYSxnQkFBZ0IsTUFBTSxnQkFBZ0IsTUFBTSxNQUFNLE1BQU0sSUFBSTtBQUFBO0FBSXpILGNBQUksYUFBYSxXQUFXO0FBQUcsbUJBQU87QUFFdEMsY0FBSSxhQUFhLFdBQVcsS0FBSyxhQUFhLE9BQU8sY0FBYyxFQUFFO0FBQU0sbUJBQU87QUFFbEYsZ0JBQU0scUJBQXFCLENBQUMsUUFBUSxTQUFTLE9BQU8sUUFBUSxPQUFPO0FBQ25FLGdCQUFNLDZCQUE2QixtQkFBbUIsT0FBTyxjQUFZLGFBQWEsU0FBUztBQUMvRix5QkFBZSxhQUFhLE9BQU8sT0FBSyxDQUFDLDJCQUEyQixTQUFTO0FBRTdFLGNBQUksMkJBQTJCLFNBQVMsR0FBRztBQUN6QyxrQkFBTSw4QkFBOEIsMkJBQTJCLE9BQU8sY0FBWTtBQUVoRixrQkFBSSxhQUFhLFNBQVMsYUFBYTtBQUFTLDJCQUFXO0FBQzNELHFCQUFPLEVBQUUsR0FBRztBQUFBO0FBR2QsZ0JBQUksNEJBQTRCLFdBQVcsMkJBQTJCLFFBQVE7QUFFNUUsa0JBQUksYUFBYSxPQUFPLGNBQWMsRUFBRTtBQUFNLHVCQUFPO0FBQUE7QUFBQTtBQUt6RCxpQkFBTztBQUFBO0FBR1QsK0JBQXVCLEtBQUs7QUFDMUIsa0JBQVE7QUFBQSxpQkFDRDtBQUNILHFCQUFPO0FBQUEsaUJBRUo7QUFBQSxpQkFDQTtBQUNILHFCQUFPO0FBQUE7QUFHUCxxQkFBTyxPQUFPLFVBQVU7QUFBQTtBQUFBO0FBSTlCLHVDQUErQixXQUFXLElBQUksV0FBVyxZQUFZLFdBQVc7QUFHOUUsY0FBSSxRQUFRLEdBQUcsUUFBUSxrQkFBa0IsWUFBWSxDQUFDLFlBQVksU0FBUyxTQUFTLEdBQUcsU0FBUyxVQUFVLFNBQVMsVUFBVSxXQUFXO0FBQ3hJLGdCQUFNLHFCQUFxQixHQUFHLDhDQUE4QztBQUM1RSwyQkFBaUIsV0FBVyxJQUFJLE9BQU8sV0FBVyxvQkFBb0IsTUFBTTtBQUMxRSxtQkFBTyxlQUFlLGVBQWUsSUFBSSxjQUFjLElBQUk7QUFBQSxjQUN6RCx1QkFBdUIsZ0NBQWdDLElBQUksV0FBVztBQUFBO0FBQUE7QUFBQTtBQUs1RSxpREFBeUMsSUFBSSxXQUFXLFlBQVk7QUFDbEUsY0FBSSxHQUFHLFNBQVMsU0FBUztBQUl2QixnQkFBSSxDQUFDLEdBQUcsYUFBYTtBQUFTLGlCQUFHLGFBQWEsUUFBUTtBQUFBO0FBR3hELGlCQUFPLENBQUMsT0FBTyxpQkFBaUI7QUFFOUIsZ0JBQUksaUJBQWlCLGVBQWUsTUFBTSxRQUFRO0FBQ2hELHFCQUFPLE1BQU07QUFBQSx1QkFDSixHQUFHLFNBQVMsWUFBWTtBQUVqQyxrQkFBSSxNQUFNLFFBQVEsZUFBZTtBQUMvQixzQkFBTSxXQUFXLFVBQVUsU0FBUyxZQUFZLGdCQUFnQixNQUFNLE9BQU8sU0FBUyxNQUFNLE9BQU87QUFDbkcsdUJBQU8sTUFBTSxPQUFPLFVBQVUsYUFBYSxPQUFPLENBQUMsYUFBYSxhQUFhLE9BQU8sU0FBTSxDQUFDLHdCQUF3QixLQUFJO0FBQUEscUJBQ2xIO0FBQ0wsdUJBQU8sTUFBTSxPQUFPO0FBQUE7QUFBQSx1QkFFYixHQUFHLFFBQVEsa0JBQWtCLFlBQVksR0FBRyxVQUFVO0FBQy9ELHFCQUFPLFVBQVUsU0FBUyxZQUFZLE1BQU0sS0FBSyxNQUFNLE9BQU8saUJBQWlCLElBQUksWUFBVTtBQUMzRixzQkFBTSxXQUFXLE9BQU8sU0FBUyxPQUFPO0FBQ3hDLHVCQUFPLGdCQUFnQjtBQUFBLG1CQUNwQixNQUFNLEtBQUssTUFBTSxPQUFPLGlCQUFpQixJQUFJLFlBQVU7QUFDMUQsdUJBQU8sT0FBTyxTQUFTLE9BQU87QUFBQTtBQUFBLG1CQUUzQjtBQUNMLG9CQUFNLFdBQVcsTUFBTSxPQUFPO0FBQzlCLHFCQUFPLFVBQVUsU0FBUyxZQUFZLGdCQUFnQixZQUFZLFVBQVUsU0FBUyxVQUFVLFNBQVMsU0FBUztBQUFBO0FBQUE7QUFBQTtBQUt2SCxpQ0FBeUIsVUFBVTtBQUNqQyxnQkFBTSxTQUFTLFdBQVcsV0FBVyxZQUFZO0FBQ2pELGlCQUFPLFVBQVUsVUFBVSxTQUFTO0FBQUE7QUFNdEMsY0FBTSxFQUFFLFlBQVk7QUFDcEIsY0FBTSxFQUFFLGdCQUFnQixRQUFRLGNBQWMsZ0JBQWdCLHNCQUFzQixrQkFBa0Isd0JBQXdCLGNBQWMsMEJBQTBCLHFCQUFxQix1QkFBdUIsbUJBQW1CLG1CQUFvQjtBQUN6UCxjQUFNLEVBQUUsTUFBTSxXQUFXLFFBQVEsYUFBYSxLQUFLLGFBQWMsTUFBTTtBQUN2RSw2QkFBcUIsS0FBSztBQUN0QixpQkFBTyxRQUFRO0FBQUE7QUFFbkIsNEJBQW9CLEtBQUs7QUFDckIsaUJBQU8sT0FBTyxRQUFRO0FBQUE7QUFFMUIsMEJBQWtCLEtBQUs7QUFDbkIsaUJBQU8sT0FBTyxRQUFRO0FBQUE7QUFFMUIsY0FBTSxrQkFBa0IsSUFBSTtBQUM1QiwrQkFBdUIsT0FBTyxPQUFPO0FBQ2pDLDBCQUFnQixJQUFJLE9BQU87QUFBQTtBQUUvQixjQUFNLFNBQVMsQ0FBQyxpQkFBaUIsZ0JBQWdCLElBQUksaUJBQWlCO0FBRXRFLDJCQUFtQixVQUFVLE9BQU87QUFDaEMsaUJBQU8sU0FBUyxrQkFBa0IsU0FBUyxTQUFTLFNBQVMsU0FBUztBQUFBO0FBTzFFLGtDQUEwQixZQUFZO0FBQ2xDLGNBQUksZUFBZSxLQUFLLFlBQVksVUFBVTtBQUMxQyx1QkFBVyxRQUFRLE9BQU8sV0FBVztBQUFBO0FBRXpDLGlCQUFPO0FBQUE7QUFFWCxrQ0FBMEIsVUFBVSxjQUFjLGdCQUFnQjtBQUM5RCxnQkFBTSxhQUFhLFlBQVksS0FBSyxvQkFBb0IsaUJBQWlCLHNCQUFzQjtBQUMvRixxQkFBVyxRQUFRLENBQUMsUUFBUTtBQUN4QixnQkFBSSxhQUFhLHlCQUF5QixnQkFBZ0I7QUFNMUQsZ0JBQUksQ0FBQyxXQUFXLGNBQWM7QUFDMUIsMkJBQWEsZUFBZSxVQUFVLFlBQVk7QUFBQTtBQUV0RCxpQ0FBcUIsY0FBYyxLQUFLO0FBQUE7QUFFNUMsNEJBQWtCO0FBQUE7QUFFdEIsbUNBQTJCO0FBQUEsVUFDdkIsWUFBWSxVQUFVLE9BQU87QUFDekIsaUJBQUssaUJBQWlCO0FBQ3RCLGlCQUFLLFdBQVc7QUFBQTtBQUFBLFVBRXBCLElBQUksY0FBYyxLQUFLO0FBQ25CLGtCQUFNLEVBQUUsZ0JBQWdCLGFBQWE7QUFDckMsa0JBQU0sUUFBUSxlQUFlO0FBQzdCLGtCQUFNLEVBQUUsa0JBQWtCO0FBQzFCLDBCQUFjLGdCQUFnQjtBQUM5QixtQkFBTyxTQUFTLFNBQVM7QUFBQTtBQUFBLFVBRTdCLElBQUksY0FBYyxLQUFLLE9BQU87QUFDMUIsa0JBQU0sRUFBRSxnQkFBZ0IsVUFBVSxFQUFFLG1CQUFtQjtBQUN2RCxrQkFBTSxXQUFXLGVBQWU7QUFDaEMsZ0JBQUksYUFBYSxPQUFPO0FBQ3BCLDZCQUFlLE9BQU87QUFDdEIsMkJBQWEsZ0JBQWdCO0FBQUEsdUJBRXhCLFFBQVEsWUFBWSxRQUFRLGlCQUFpQjtBQUtsRCwyQkFBYSxnQkFBZ0I7QUFBQTtBQUVqQyxtQkFBTztBQUFBO0FBQUEsVUFFWCxlQUFlLGNBQWMsS0FBSztBQUM5QixrQkFBTSxFQUFFLGdCQUFnQixVQUFVLEVBQUUsbUJBQW1CO0FBQ3ZELG1CQUFPLGVBQWU7QUFDdEIseUJBQWEsZ0JBQWdCO0FBQzdCLG1CQUFPO0FBQUE7QUFBQSxVQUVYLE1BQU0sY0FBYyxTQUFTLFVBQVU7QUFBQTtBQUFBLFVBR3ZDLFVBQVUsUUFBUSxVQUFVLFdBQVc7QUFBQTtBQUFBLFVBR3ZDLElBQUksY0FBYyxLQUFLO0FBQ25CLGtCQUFNLEVBQUUsZ0JBQWdCLFVBQVUsRUFBRSxvQkFBb0I7QUFDeEQsMEJBQWMsZ0JBQWdCO0FBQzlCLG1CQUFPLE9BQU87QUFBQTtBQUFBLFVBRWxCLFFBQVEsY0FBYztBQUNsQixrQkFBTSxFQUFFLG1CQUFtQjtBQUMzQixtQkFBTyxZQUFZLEtBQUssb0JBQW9CLGlCQUFpQixzQkFBc0I7QUFBQTtBQUFBLFVBRXZGLGFBQWEsY0FBYztBQUN2QixrQkFBTSxxQkFBcUIsYUFBYTtBQUN4QyxnQkFBSSxDQUFDLG9CQUFvQjtBQUNyQixxQkFBTztBQUFBO0FBRVgsa0JBQU0sRUFBRSxnQkFBZ0IsYUFBYTtBQUNyQyxrQkFBTSxxQkFBcUIsYUFBYTtBQUN4QyxnQkFBSSxDQUFDLG9CQUFvQjtBQUNyQiwrQkFBaUIsVUFBVSxjQUFjO0FBQUE7QUFFN0MsbUJBQU87QUFBQTtBQUFBLFVBRVgsZUFBZSxjQUFjLFdBQVc7QUFBQTtBQUFBLFVBRXhDLGVBQWUsY0FBYztBQUN6QixrQkFBTSxFQUFFLG1CQUFtQjtBQUMzQixtQkFBTyxlQUFlO0FBQUE7QUFBQSxVQUUxQix5QkFBeUIsY0FBYyxLQUFLO0FBQ3hDLGtCQUFNLEVBQUUsZ0JBQWdCLGFBQWE7QUFDckMsa0JBQU0sRUFBRSxrQkFBa0IsS0FBSztBQUUvQiwwQkFBYyxnQkFBZ0I7QUFDOUIsZ0JBQUksT0FBTyx5QkFBeUIsZ0JBQWdCO0FBQ3BELGdCQUFJLFlBQVksT0FBTztBQUNuQixxQkFBTztBQUFBO0FBRVgsa0JBQU0sbUJBQW1CLHlCQUF5QixjQUFjO0FBQ2hFLGdCQUFJLENBQUMsWUFBWSxtQkFBbUI7QUFDaEMscUJBQU87QUFBQTtBQUtYLG1CQUFPLGVBQWUsVUFBVSxNQUFNO0FBQ3RDLGdCQUFJLENBQUMsS0FBSyxjQUFjO0FBTXBCLG1DQUFxQixjQUFjLEtBQUs7QUFBQTtBQUU1QyxtQkFBTztBQUFBO0FBQUEsVUFFWCxrQkFBa0IsY0FBYztBQUM1QixrQkFBTSxFQUFFLGdCQUFnQixhQUFhO0FBQ3JDLDZCQUFpQixVQUFVLGNBQWM7QUFDekMsOEJBQWtCO0FBQ2xCLG1CQUFPO0FBQUE7QUFBQSxVQUVYLGVBQWUsY0FBYyxLQUFLLFlBQVk7QUFDMUMsa0JBQU0sRUFBRSxnQkFBZ0IsYUFBYTtBQUNyQyxrQkFBTSxFQUFFLGlCQUFpQjtBQUN6QixrQkFBTSxFQUFFLGlCQUFpQjtBQVF6QixnQkFBSSxlQUFlLEtBQUssWUFBWSxlQUFlLENBQUMsZUFBZSxLQUFLLFlBQVksVUFBVTtBQUMxRixvQkFBTSxxQkFBcUIseUJBQXlCLGdCQUFnQjtBQUNwRSx5QkFBVyxRQUFRLG1CQUFtQjtBQUFBO0FBRTFDLGlDQUFxQixnQkFBZ0IsS0FBSyxpQkFBaUI7QUFDM0QsZ0JBQUksaUJBQWlCLE9BQU87QUFDeEIsbUNBQXFCLGNBQWMsS0FBSyxlQUFlLFVBQVUsWUFBWTtBQUFBO0FBRWpGLHlCQUFhLGdCQUFnQjtBQUM3QixtQkFBTztBQUFBO0FBQUE7QUFJZixtQ0FBMkIsVUFBVSxPQUFPO0FBQ3hDLGlCQUFPLFNBQVMsa0JBQWtCLFNBQVMsU0FBUyxpQkFBaUIsU0FBUztBQUFBO0FBRWxGLDhCQUFzQjtBQUFBLFVBQ2xCLFlBQVksVUFBVSxPQUFPO0FBQ3pCLGlCQUFLLGlCQUFpQjtBQUN0QixpQkFBSyxXQUFXO0FBQUE7QUFBQSxVQUVwQixJQUFJLGNBQWMsS0FBSztBQUNuQixrQkFBTSxFQUFFLFVBQVUsbUJBQW1CO0FBQ3JDLGtCQUFNLFFBQVEsZUFBZTtBQUM3QixrQkFBTSxFQUFFLGtCQUFrQjtBQUMxQiwwQkFBYyxnQkFBZ0I7QUFDOUIsbUJBQU8sU0FBUyxpQkFBaUI7QUFBQTtBQUFBLFVBRXJDLElBQUksY0FBYyxLQUFLLE9BQU87QUFDMUIsbUJBQU87QUFBQTtBQUFBLFVBRVgsZUFBZSxjQUFjLEtBQUs7QUFDOUIsbUJBQU87QUFBQTtBQUFBLFVBRVgsTUFBTSxjQUFjLFNBQVMsVUFBVTtBQUFBO0FBQUEsVUFHdkMsVUFBVSxRQUFRLFVBQVUsV0FBVztBQUFBO0FBQUEsVUFHdkMsSUFBSSxjQUFjLEtBQUs7QUFDbkIsa0JBQU0sRUFBRSxnQkFBZ0IsVUFBVSxFQUFFLG9CQUFvQjtBQUN4RCwwQkFBYyxnQkFBZ0I7QUFDOUIsbUJBQU8sT0FBTztBQUFBO0FBQUEsVUFFbEIsUUFBUSxjQUFjO0FBQ2xCLGtCQUFNLEVBQUUsbUJBQW1CO0FBQzNCLG1CQUFPLFlBQVksS0FBSyxvQkFBb0IsaUJBQWlCLHNCQUFzQjtBQUFBO0FBQUEsVUFFdkYsZUFBZSxjQUFjLFdBQVc7QUFBQTtBQUFBLFVBRXhDLHlCQUF5QixjQUFjLEtBQUs7QUFDeEMsa0JBQU0sRUFBRSxnQkFBZ0IsYUFBYTtBQUNyQyxrQkFBTSxFQUFFLGtCQUFrQjtBQUUxQiwwQkFBYyxnQkFBZ0I7QUFDOUIsZ0JBQUksT0FBTyx5QkFBeUIsZ0JBQWdCO0FBQ3BELGdCQUFJLFlBQVksT0FBTztBQUNuQixxQkFBTztBQUFBO0FBRVgsa0JBQU0sbUJBQW1CLHlCQUF5QixjQUFjO0FBQ2hFLGdCQUFJLENBQUMsWUFBWSxtQkFBbUI7QUFDaEMscUJBQU87QUFBQTtBQUtYLG1CQUFPLGVBQWUsVUFBVSxNQUFNO0FBQ3RDLGdCQUFJLGVBQWUsS0FBSyxNQUFNLFFBQVE7QUFDbEMsbUJBQUssTUFBTTtBQUFBO0FBRWYsZ0JBQUksQ0FBQyxLQUFLLGNBQWM7QUFNcEIsbUNBQXFCLGNBQWMsS0FBSztBQUFBO0FBRTVDLG1CQUFPO0FBQUE7QUFBQSxVQUVYLGtCQUFrQixjQUFjO0FBQzVCLG1CQUFPO0FBQUE7QUFBQSxVQUVYLGVBQWUsY0FBYyxLQUFLLFlBQVk7QUFDMUMsbUJBQU87QUFBQTtBQUFBO0FBR2Ysb0NBQTRCLE9BQU87QUFDL0IsY0FBSSxlQUFlO0FBQ25CLGNBQUksUUFBUSxRQUFRO0FBQ2hCLDJCQUFlO0FBQUEscUJBRVYsU0FBUyxRQUFRO0FBQ3RCLDJCQUFlO0FBQUE7QUFFbkIsaUJBQU87QUFBQTtBQUVYLGNBQU0scUJBQXFCLE9BQU87QUFDbEMsMENBQWtDLE9BQU87QUFFckMsY0FBSSxVQUFVLE1BQU07QUFDaEIsbUJBQU87QUFBQTtBQUdYLGNBQUksT0FBTyxVQUFVLFVBQVU7QUFDM0IsbUJBQU87QUFBQTtBQUVYLGNBQUksUUFBUSxRQUFRO0FBQ2hCLG1CQUFPO0FBQUE7QUFFWCxnQkFBTSxRQUFRLGVBQWU7QUFDN0IsaUJBQVEsVUFBVSxzQkFBc0IsVUFBVSxRQUFRLGVBQWUsV0FBVztBQUFBO0FBRXhGLGNBQU0sdUJBQXVCLENBQUMsS0FBSyxRQUFRO0FBQUE7QUFHM0MsY0FBTSxzQkFBc0IsQ0FBQyxLQUFLLFFBQVE7QUFBQTtBQUcxQyxjQUFNLHlCQUF5QixDQUFDLFVBQVU7QUFDMUMsZ0NBQXdCLFVBQVUsWUFBWSxVQUFVO0FBQ3BELGdCQUFNLEVBQUUsS0FBSyxRQUFRO0FBQ3JCLGNBQUksZUFBZSxLQUFLLFlBQVksVUFBVTtBQUMxQyx1QkFBVyxRQUFRLFNBQVMsVUFBVSxXQUFXO0FBQUEsaUJBRWhEO0FBQ0QsZ0JBQUksQ0FBQyxZQUFZLE1BQU07QUFDbkIseUJBQVcsTUFBTSxXQUFZO0FBRXpCLHVCQUFPLFNBQVMsVUFBVSxJQUFJLEtBQUssT0FBTztBQUFBO0FBQUE7QUFHbEQsZ0JBQUksQ0FBQyxZQUFZLE1BQU07QUFDbkIseUJBQVcsTUFBTSxTQUFVLE9BQU87QUFNOUIsb0JBQUksS0FBSyxPQUFPLE9BQU8sU0FBUyxZQUFZO0FBQUE7QUFBQTtBQUFBO0FBSXhELGlCQUFPO0FBQUE7QUFFWCwrQkFBdUI7QUFBQSxVQUNuQixZQUFZLFNBQVM7QUFDakIsaUJBQUssa0JBQWtCO0FBQ3ZCLGlCQUFLLGVBQWU7QUFDcEIsaUJBQUssZ0JBQWdCO0FBQ3JCLGlCQUFLLG9CQUFvQjtBQUN6QixpQkFBSyxjQUFjLElBQUk7QUFDdkIsZ0JBQUksQ0FBQyxZQUFZLFVBQVU7QUFDdkIsb0JBQU0sRUFBRSxpQkFBaUIsY0FBYyxlQUFlLHNCQUFzQjtBQUM1RSxtQkFBSyxrQkFBa0IsV0FBVyxtQkFBbUIsa0JBQWtCO0FBQ3ZFLG1CQUFLLGVBQWUsV0FBVyxnQkFBZ0IsZUFBZTtBQUM5RCxtQkFBSyxnQkFBZ0IsV0FBVyxpQkFBaUIsZ0JBQWdCO0FBQ2pFLG1CQUFLLG9CQUFvQixXQUFXLHFCQUFxQixvQkFBb0I7QUFBQTtBQUFBO0FBQUEsVUFHckYsU0FBUyxPQUFPO0FBQ1osa0JBQU0saUJBQWlCLE9BQU87QUFDOUIsa0JBQU0sWUFBWSxLQUFLLGdCQUFnQjtBQUN2QyxnQkFBSSxLQUFLLGtCQUFrQixZQUFZO0FBQ25DLG9CQUFNLElBQUksS0FBSyxpQkFBaUIsZ0JBQWdCO0FBR2hELHFCQUFPLEVBQUUsYUFBYSxRQUFRLFFBQVEsRUFBRTtBQUFBO0FBRTVDLG1CQUFPO0FBQUE7QUFBQSxVQUVYLGlCQUFpQixPQUFPO0FBQ3BCLG9CQUFRLE9BQU87QUFDZixrQkFBTSxZQUFZLEtBQUssZ0JBQWdCO0FBQ3ZDLGdCQUFJLEtBQUssa0JBQWtCLFlBQVk7QUFDbkMscUJBQU8sS0FBSyxpQkFBaUIsT0FBTyxXQUFXO0FBQUE7QUFFbkQsbUJBQU87QUFBQTtBQUFBLFVBRVgsWUFBWSxHQUFHO0FBQ1gsbUJBQU8sT0FBTztBQUFBO0FBQUEsVUFFbEIsaUJBQWlCLE9BQU8sZ0JBQWdCO0FBQ3BDLGtCQUFNLEVBQUUsZ0JBQWlCO0FBQ3pCLGdCQUFJLGdCQUFnQixZQUFZLElBQUk7QUFDcEMsZ0JBQUksZUFBZTtBQUNmLHFCQUFPO0FBQUE7QUFFWCxrQkFBTSxXQUFXO0FBQ2pCLDRCQUFnQjtBQUFBLGtCQUNSLFdBQVc7QUFDWCxzQkFBTSxrQkFBa0IsSUFBSSxxQkFBcUIsVUFBVTtBQUUzRCxzQkFBTSxRQUFRLElBQUksTUFBTSxtQkFBbUIsaUJBQWlCO0FBQzVELDhCQUFjLE9BQU87QUFDckIscUNBQXFCLE1BQU0sWUFBWSxFQUFFLE9BQU87QUFDaEQsdUJBQU87QUFBQTtBQUFBLGtCQUVQLFdBQVc7QUFDWCxzQkFBTSxrQkFBa0IsSUFBSSxnQkFBZ0IsVUFBVTtBQUV0RCxzQkFBTSxRQUFRLElBQUksTUFBTSxtQkFBbUIsaUJBQWlCO0FBQzVELDhCQUFjLE9BQU87QUFDckIscUNBQXFCLE1BQU0sWUFBWSxFQUFFLE9BQU87QUFDaEQsdUJBQU87QUFBQTtBQUFBO0FBR2Ysd0JBQVksSUFBSSxnQkFBZ0I7QUFDaEMsbUJBQU87QUFBQTtBQUFBO0FBS2Ysc0JBQWMsTUFBTSxrQkFBa0I7QUFFcEMsY0FBSSxXQUFXLElBQUksaUJBQWlCO0FBQUEsWUFDbEMsYUFBYSxRQUFRLEtBQUs7QUFDeEIsK0JBQWlCLFFBQVE7QUFBQTtBQUFBO0FBSTdCLGlCQUFPO0FBQUEsWUFDTCxNQUFNLFNBQVMsU0FBUztBQUFBLFlBQ3hCO0FBQUE7QUFBQTtBQUdKLDBCQUFrQixVQUFVLFlBQVk7QUFDdEMsY0FBSSxnQkFBZ0IsU0FBUyxZQUFZO0FBQ3pDLGNBQUksT0FBTztBQUNYLGlCQUFPLEtBQUssZUFBZSxRQUFRLFNBQU87QUFDeEMsZ0JBQUksQ0FBQyxPQUFPLFNBQVMsYUFBYSxVQUFVLFNBQVM7QUFBTTtBQUMzRCxpQkFBSyxPQUFPLGNBQWM7QUFBQTtBQUU1QixpQkFBTztBQUFBO0FBR1Qsd0JBQWdCO0FBQUEsVUFDZCxZQUFZLElBQUksb0JBQW9CLE1BQU07QUFDeEMsaUJBQUssTUFBTTtBQUNYLGtCQUFNLFdBQVcsS0FBSyxJQUFJLGFBQWE7QUFDdkMsa0JBQU0saUJBQWlCLGFBQWEsS0FBSyxPQUFPO0FBQ2hELGtCQUFNLGlCQUFpQixLQUFLLElBQUksYUFBYTtBQUM3QyxnQkFBSSxhQUFhO0FBQUEsY0FDZixLQUFLLEtBQUs7QUFBQTtBQUVaLGdCQUFJLHFDQUFxQyxvQkFBb0Isa0JBQWtCLE1BQU0sS0FBSztBQUMxRixtQkFBTyxRQUFRLE9BQU8saUJBQWlCLFFBQVEsQ0FBQyxDQUFDLE1BQU0sY0FBYztBQUNuRSxxQkFBTyxlQUFlLFlBQVksSUFBSSxRQUFRO0FBQUEsZ0JBQzVDLEtBQUssZUFBZTtBQUNsQix5QkFBTyxTQUFTO0FBQUE7QUFBQTtBQUFBO0FBSXRCLGlCQUFLLGlCQUFpQixvQkFBb0Isa0JBQWtCLHNCQUFzQixVQUFVLElBQUksZ0JBQWdCO0FBR2hILGdCQUFJO0FBQUEsY0FDRjtBQUFBLGNBQ0E7QUFBQSxnQkFDRSxLQUFLLHFCQUFxQixLQUFLO0FBQ25DLGlCQUFLLFFBQVE7QUFDYixpQkFBSyxXQUFXO0FBR2hCLGlCQUFLLGVBQWUsTUFBTSxLQUFLO0FBQy9CLGlCQUFLLGVBQWUsUUFBUSxLQUFLO0FBQ2pDLGlCQUFLLGdCQUFnQjtBQUVyQixpQkFBSyxlQUFlLFlBQVksY0FBWTtBQUMxQyxtQkFBSyxjQUFjLEtBQUs7QUFBQTtBQUcxQixpQkFBSyxXQUFXO0FBRWhCLGlCQUFLLGVBQWUsU0FBUyxDQUFDLFVBQVUsYUFBYTtBQUNuRCxrQkFBSSxDQUFDLEtBQUssU0FBUztBQUFXLHFCQUFLLFNBQVMsWUFBWTtBQUN4RCxtQkFBSyxTQUFTLFVBQVUsS0FBSztBQUFBO0FBUS9CLG1CQUFPLFFBQVEsT0FBTyxpQkFBaUIsUUFBUSxDQUFDLENBQUMsTUFBTSxjQUFjO0FBQ25FLHFCQUFPLGVBQWUsS0FBSyxnQkFBZ0IsSUFBSSxRQUFRO0FBQUEsZ0JBQ3JELEtBQUssZUFBZTtBQUNsQix5QkFBTyxTQUFTLG9DQUFvQyxLQUFLO0FBQUE7QUFBQTtBQUFBO0FBTS9ELGlCQUFLLHFCQUFxQjtBQUMxQixpQkFBSztBQUNMLGlDQUFxQixPQUFPLDhCQUE4QixRQUFRLGNBQVksU0FBUztBQUN2RixnQkFBSTtBQUVKLGdCQUFJLGtCQUFrQixDQUFDLG1CQUFtQjtBQUd4QyxtQkFBSyxrQkFBa0I7QUFDdkIscUNBQXVCLEtBQUsseUJBQXlCLEtBQUssS0FBSztBQUMvRCxtQkFBSyxrQkFBa0I7QUFBQTtBQU16QixpQkFBSyxtQkFBbUIsS0FBSyxLQUFLLE1BQU07QUFBQSxlQUFJO0FBRzVDLGlCQUFLO0FBRUwsZ0JBQUksT0FBTyx5QkFBeUIsWUFBWTtBQUc5QyxtQ0FBcUIsS0FBSyxLQUFLO0FBQUE7QUFHakMsaUNBQXFCLFdBQVcsTUFBTTtBQUNwQyxxQkFBTyx3QkFBd0IsUUFBUSxjQUFZLFNBQVM7QUFBQSxlQUMzRDtBQUFBO0FBQUEsVUFHTCxvQkFBb0I7QUFDbEIsbUJBQU8sU0FBUyxLQUFLLFVBQVUsS0FBSztBQUFBO0FBQUEsVUFHdEMscUJBQXFCLE1BQU07QUFDekIsZ0JBQUksUUFBTztBQUNYLGdCQUFJLFlBQVksU0FBUyxXQUFZO0FBQ25DLG9CQUFLLGVBQWUsTUFBSztBQUFBLGVBQ3hCO0FBQ0gsbUJBQU8sS0FBSyxNQUFNLENBQUMsUUFBUSxRQUFRO0FBQ2pDLGtCQUFJLE1BQUssU0FBUyxNQUFNO0FBRXRCLHNCQUFLLFNBQVMsS0FBSyxRQUFRLGNBQVksU0FBUyxPQUFPO0FBQUEseUJBQzlDLE1BQU0sUUFBUSxTQUFTO0FBRWhDLHVCQUFPLEtBQUssTUFBSyxVQUFVLFFBQVEsd0JBQXNCO0FBQ3ZELHNCQUFJLG1CQUFtQixtQkFBbUIsTUFBTTtBQUloRCxzQkFBSSxRQUFRO0FBQVU7QUFDdEIsbUNBQWlCLE9BQU8sQ0FBQyxnQkFBZ0IsU0FBUztBQUNoRCx3QkFBSSxPQUFPLEdBQUcsUUFBUSxlQUFlLFFBQVE7QUFDM0MsNEJBQUssU0FBUyxvQkFBb0IsUUFBUSxjQUFZLFNBQVM7QUFBQTtBQUdqRSwyQkFBTyxlQUFlO0FBQUEscUJBQ3JCLE1BQUs7QUFBQTtBQUFBLHFCQUVMO0FBR0wsdUJBQU8sS0FBSyxNQUFLLFVBQVUsT0FBTyxPQUFLLEVBQUUsU0FBUyxNQUFNLFFBQVEsd0JBQXNCO0FBQ3BGLHNCQUFJLG1CQUFtQixtQkFBbUIsTUFBTTtBQUdoRCxzQkFBSSxRQUFRLGlCQUFpQixpQkFBaUIsU0FBUztBQUFJO0FBRzNELG1DQUFpQixPQUFPLENBQUMsZ0JBQWdCLFNBQVM7QUFDaEQsd0JBQUksT0FBTyxHQUFHLFFBQVEsaUJBQWlCO0FBRXJDLDRCQUFLLFNBQVMsb0JBQW9CLFFBQVEsY0FBWSxTQUFTLE9BQU87QUFBQTtBQUd4RSwyQkFBTyxlQUFlO0FBQUEscUJBQ3JCLE1BQUs7QUFBQTtBQUFBO0FBS1osa0JBQUksTUFBSztBQUFpQjtBQUMxQjtBQUFBO0FBQUE7QUFBQSxVQUlKLDRCQUE0QixJQUFJLFVBQVUsOEJBQThCLE1BQU07QUFBQSxhQUFJO0FBQ2hGLGlCQUFLLElBQUksU0FBTTtBQUViLGtCQUFJLElBQUcsYUFBYSxXQUFXO0FBRTdCLG9CQUFJLENBQUMsSUFBRyxXQUFXLEtBQUssTUFBTTtBQUU1QixzQkFBSSxDQUFDLElBQUc7QUFBSyxnREFBNEI7QUFFekMseUJBQU87QUFBQTtBQUFBO0FBSVgscUJBQU8sU0FBUztBQUFBO0FBQUE7QUFBQSxVQUlwQixtQkFBbUIsUUFBUSxZQUFZLE1BQU07QUFBQSxhQUFJLG9CQUFvQixPQUFPO0FBQzFFLGlCQUFLLDRCQUE0QixRQUFRLFFBQU07QUFFN0Msa0JBQUksR0FBRyxnQkFBZ0I7QUFBVyx1QkFBTztBQUV6QyxrQkFBSSxHQUFHLG9CQUFvQjtBQUFXLHVCQUFPO0FBQzdDLG1CQUFLLGtCQUFrQixJQUFJLFdBQVcsb0JBQW9CLFFBQVE7QUFBQSxlQUNqRSxRQUFNO0FBQ1Asa0JBQUksQ0FBQztBQUFtQixtQkFBRyxNQUFNLElBQUksVUFBVTtBQUFBO0FBRWpELGlCQUFLO0FBQ0wsaUJBQUssNkJBQTZCO0FBQUE7QUFBQSxVQUdwQyxrQkFBa0IsSUFBSSxXQUFXLDBCQUEwQixNQUFNO0FBRy9ELGdCQUFJLEdBQUcsYUFBYSxZQUFZLFVBQVUsSUFBSSxNQUFNLFNBQVMsR0FBRztBQUM5RCxpQkFBRyx1QkFBdUIsMEJBQTBCLEdBQUcsYUFBYTtBQUFBO0FBR3RFLHVDQUEyQixLQUFLLGtCQUFrQixJQUFJO0FBQ3RELGlCQUFLLHVCQUF1QixJQUFJLE1BQU07QUFBQTtBQUFBLFVBR3hDLGVBQWUsUUFBUSxZQUFZLE1BQU07QUFBQSxhQUFJO0FBQzNDLGlCQUFLLDRCQUE0QixRQUFRLFFBQU07QUFFN0Msa0JBQUksR0FBRyxnQkFBZ0IsVUFBYSxDQUFDLEdBQUcsV0FBVyxLQUFLO0FBQU0sdUJBQU87QUFDckUsbUJBQUssY0FBYyxJQUFJO0FBQUEsZUFDdEIsUUFBTTtBQUNQLGlCQUFHLE1BQU0sSUFBSSxVQUFVO0FBQUE7QUFFekIsaUJBQUs7QUFDTCxpQkFBSyw2QkFBNkI7QUFBQTtBQUFBLFVBR3BDLDZCQUE2QixJQUFJO0FBRS9CLGdCQUFJLE9BQU8sS0FBSyxPQUFPLEtBQUssY0FBYyxTQUFTLEdBQUc7QUFHcEQsb0NBQXNCLE1BQU07QUFDMUIsdUJBQU8sS0FBSyxjQUFjLFNBQVMsR0FBRztBQUNwQyx1QkFBSyxjQUFjO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQU0zQiw2Q0FBNkM7QUFJM0MsaUJBQUssbUJBQW1CLFVBQVUsSUFBSSxhQUFXO0FBQy9DLHFCQUFPLElBQUksUUFBUSxDQUFDLFNBQVMsV0FBVztBQUN0Qyx3QkFBUSxTQUFTO0FBQUE7QUFBQSxlQUVsQixPQUFPLENBQUMsY0FBYyxZQUFZO0FBQ25DLHFCQUFPLGFBQWEsS0FBSyxNQUFNO0FBQzdCLHVCQUFPLFFBQVEsS0FBSyxtQkFBaUI7QUFDbkM7QUFBQTtBQUFBO0FBQUEsZUFHSCxRQUFRLFFBQVEsTUFBTTtBQUFBLGdCQUFLLE1BQU0sT0FBSztBQUN2QyxrQkFBSSxNQUFNO0FBQXNCLHNCQUFNO0FBQUE7QUFHeEMsaUJBQUsscUJBQXFCO0FBQzFCLGlCQUFLLDJCQUEyQjtBQUFBO0FBQUEsVUFHbEMsY0FBYyxJQUFJLFdBQVc7QUFDM0IsaUJBQUssdUJBQXVCLElBQUksT0FBTztBQUFBO0FBQUEsVUFHekMsa0JBQWtCLElBQUksV0FBVztBQUMvQixzQkFBVSxJQUFJLE1BQU0sUUFBUSxDQUFDO0FBQUEsY0FDM0I7QUFBQSxjQUNBO0FBQUEsY0FDQTtBQUFBLGNBQ0E7QUFBQSxrQkFDSTtBQUNKLHNCQUFRO0FBQUEscUJBQ0Q7QUFDSCxtQ0FBaUIsTUFBTSxJQUFJLE9BQU8sV0FBVyxZQUFZO0FBQ3pEO0FBQUEscUJBRUc7QUFDSCx3Q0FBc0IsTUFBTSxJQUFJLFdBQVcsWUFBWTtBQUN2RDtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBS1IsdUJBQXVCLElBQUksZ0JBQWdCLE9BQU8sV0FBVztBQUMzRCxnQkFBSSxRQUFRLFVBQVUsSUFBSTtBQUMxQixrQkFBTSxRQUFRLENBQUM7QUFBQSxjQUNiO0FBQUEsY0FDQTtBQUFBLGNBQ0E7QUFBQSxjQUNBO0FBQUEsa0JBQ0k7QUFDSixzQkFBUTtBQUFBLHFCQUNEO0FBQ0gsa0RBQWdDLE1BQU0sSUFBSSxTQUFTLFlBQVksV0FBVyxNQUFNO0FBQ2hGO0FBQUEscUJBRUc7QUFFSCxzQkFBSSxHQUFHLFFBQVEsa0JBQWtCLGNBQWMsVUFBVTtBQUFPO0FBQ2hFLGtEQUFnQyxNQUFNLElBQUksT0FBTyxZQUFZLFdBQVcsTUFBTTtBQUM5RTtBQUFBLHFCQUVHO0FBQ0gsc0JBQUksU0FBUyxLQUFLLHlCQUF5QixJQUFJLFlBQVk7QUFDM0Qsc0NBQW9CLElBQUksUUFBUTtBQUNoQztBQUFBLHFCQUVHO0FBQ0gsc0NBQW9CLE1BQU0sSUFBSSxZQUFZO0FBQzFDO0FBQUEscUJBRUc7QUFDSCxzQkFBSSxTQUFTLEtBQUsseUJBQXlCLElBQUksWUFBWTtBQUMzRCxzQ0FBb0IsTUFBTSxJQUFJLFFBQVEsV0FBVztBQUNqRDtBQUFBLHFCQUVHO0FBR0gsc0JBQUksTUFBTSxLQUFLLE9BQUssRUFBRSxTQUFTO0FBQVE7QUFDdkMsc0JBQUksU0FBUyxLQUFLLHlCQUF5QixJQUFJLFlBQVk7QUFDM0Qsb0NBQWtCLE1BQU0sSUFBSSxRQUFRLGVBQWU7QUFDbkQ7QUFBQSxxQkFFRztBQUNILHFDQUFtQixNQUFNLElBQUksWUFBWSxlQUFlO0FBQ3hEO0FBQUEscUJBRUc7QUFDSCxxQkFBRyxnQkFBZ0I7QUFDbkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUtSLHlCQUF5QixJQUFJLFlBQVksWUFBWSxNQUFNO0FBQUEsYUFBSTtBQUM3RCxtQkFBTyxVQUFVLElBQUksWUFBWSxLQUFLLE9BQU8sZUFBZSxlQUFlLElBQUksY0FBYyxJQUFJO0FBQUEsY0FDL0YsV0FBVyxLQUFLLG9CQUFvQjtBQUFBO0FBQUE7QUFBQSxVQUl4QywwQkFBMEIsSUFBSSxZQUFZLFlBQVksTUFBTTtBQUFBLGFBQUk7QUFDOUQsbUJBQU8sa0JBQWtCLElBQUksWUFBWSxLQUFLLE9BQU8sZUFBZSxlQUFlLElBQUksY0FBYyxJQUFJO0FBQUEsY0FDdkcsV0FBVyxLQUFLLG9CQUFvQjtBQUFBO0FBQUE7QUFBQSxVQUl4QyxvQkFBb0IsSUFBSTtBQUN0QixtQkFBTyxDQUFDLE9BQU8sU0FBUyxPQUFPO0FBQzdCLGlCQUFHLGNBQWMsSUFBSSxZQUFZLE9BQU87QUFBQSxnQkFDdEM7QUFBQSxnQkFDQSxTQUFTO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFLZixtQ0FBbUM7QUFDakMsa0JBQU0sYUFBYSxLQUFLO0FBQ3hCLGtCQUFNLGtCQUFrQjtBQUFBLGNBQ3RCLFdBQVc7QUFBQSxjQUNYLFlBQVk7QUFBQSxjQUNaLFNBQVM7QUFBQTtBQUVYLGtCQUFNLFdBQVcsSUFBSSxpQkFBaUIsZUFBYTtBQUNqRCx1QkFBUyxJQUFJLEdBQUcsSUFBSSxVQUFVLFFBQVEsS0FBSztBQUV6QyxzQkFBTSx5QkFBeUIsVUFBVSxHQUFHLE9BQU8sUUFBUTtBQUMzRCxvQkFBSSxDQUFFLDJCQUEwQix1QkFBdUIsV0FBVyxLQUFLO0FBQU87QUFFOUUsb0JBQUksVUFBVSxHQUFHLFNBQVMsZ0JBQWdCLFVBQVUsR0FBRyxrQkFBa0IsVUFBVTtBQUNqRix3QkFBTSxRQUFRLFVBQVUsR0FBRyxPQUFPLGFBQWEsYUFBYTtBQUM1RCx3QkFBTSxVQUFVLFVBQVUsS0FBSyxLQUFLLE9BQU87QUFBQSxvQkFDekMsS0FBSyxLQUFLO0FBQUE7QUFFWix5QkFBTyxLQUFLLFNBQVMsUUFBUSxTQUFPO0FBQ2xDLHdCQUFJLEtBQUssTUFBTSxTQUFTLFFBQVEsTUFBTTtBQUNwQywyQkFBSyxNQUFNLE9BQU8sUUFBUTtBQUFBO0FBQUE7QUFBQTtBQUtoQyxvQkFBSSxVQUFVLEdBQUcsV0FBVyxTQUFTLEdBQUc7QUFDdEMsNEJBQVUsR0FBRyxXQUFXLFFBQVEsVUFBUTtBQUN0Qyx3QkFBSSxLQUFLLGFBQWEsS0FBSyxLQUFLO0FBQWlCO0FBRWpELHdCQUFJLEtBQUssUUFBUSxlQUFlLENBQUMsS0FBSyxLQUFLO0FBQ3pDLDJCQUFLLE1BQU0sSUFBSSxVQUFVO0FBQ3pCO0FBQUE7QUFHRix5QkFBSyxtQkFBbUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtoQyxxQkFBUyxRQUFRLFlBQVk7QUFBQTtBQUFBLFVBRy9CLGVBQWU7QUFDYixnQkFBSSxRQUFPO0FBQ1gsZ0JBQUksU0FBUztBQU1iLG1CQUFPLElBQUksTUFBTSxRQUFRO0FBQUEsY0FDdkIsSUFBSSxRQUFRLFVBQVU7QUFDcEIsb0JBQUksYUFBYTtBQUFrQix5QkFBTztBQUMxQyxvQkFBSTtBQUdKLHNCQUFLLDRCQUE0QixNQUFLLEtBQUssUUFBTTtBQUMvQyxzQkFBSSxHQUFHLGFBQWEsWUFBWSxHQUFHLGFBQWEsYUFBYSxVQUFVO0FBQ3JFLDBCQUFNO0FBQUE7QUFBQTtBQUdWLHVCQUFPO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRZixjQUFNLFNBQVM7QUFBQSxVQUNiLFNBQVM7QUFBQSxVQUNULHVCQUF1QjtBQUFBLFVBQ3ZCLGlCQUFpQjtBQUFBLFVBQ2pCLHlCQUF5QjtBQUFBLFVBQ3pCLCtCQUErQjtBQUFBLFVBQy9CLDhCQUE4QjtBQUFBLFVBQzlCLE9BQU8saUJBQXVCO0FBQUE7QUFDNUIsa0JBQUksQ0FBQyxhQUFhO0FBQ2hCLHNCQUFNO0FBQUE7QUFHUixtQkFBSyxtQkFBbUIsUUFBTTtBQUM1QixxQkFBSyxvQkFBb0I7QUFBQTtBQUkzQix1QkFBUyxpQkFBaUIsbUJBQW1CLE1BQU07QUFDakQscUJBQUssZ0NBQWdDLFFBQU07QUFDekMsdUJBQUssb0JBQW9CO0FBQUE7QUFBQTtBQUc3QixtQkFBSztBQUFBO0FBQUE7QUFBQSxVQUVQLG9CQUFvQiw0QkFBNEIsVUFBVTtBQUN4RCxrQkFBTSxVQUFVLFNBQVMsaUJBQWlCO0FBQzFDLG9CQUFRLFFBQVEsWUFBVTtBQUN4Qix1QkFBUztBQUFBO0FBQUE7QUFBQSxVQUdiLGlDQUFpQyx5Q0FBeUMsVUFBVSxLQUFLLE1BQU07QUFDN0Ysa0JBQU0sVUFBVyxPQUFNLFVBQVUsaUJBQWlCO0FBQ2xELGtCQUFNLEtBQUssU0FBUyxPQUFPLFNBQU0sSUFBRyxRQUFRLFFBQVcsUUFBUSxZQUFVO0FBQ3ZFLHVCQUFTO0FBQUE7QUFBQTtBQUFBLFVBR2IsOENBQThDLHdEQUF3RDtBQUNwRyxrQkFBTSxhQUFhLFNBQVMsY0FBYztBQUMxQyxrQkFBTSxrQkFBa0I7QUFBQSxjQUN0QixXQUFXO0FBQUEsY0FDWCxZQUFZO0FBQUEsY0FDWixTQUFTO0FBQUE7QUFFWCxrQkFBTSxXQUFXLElBQUksaUJBQWlCLGVBQWE7QUFDakQsa0JBQUksS0FBSztBQUF1QjtBQUVoQyx1QkFBUyxJQUFJLEdBQUcsSUFBSSxVQUFVLFFBQVEsS0FBSztBQUN6QyxvQkFBSSxVQUFVLEdBQUcsV0FBVyxTQUFTLEdBQUc7QUFDdEMsNEJBQVUsR0FBRyxXQUFXLFFBQVEsVUFBUTtBQUV0Qyx3QkFBSSxLQUFLLGFBQWE7QUFBRztBQUd6Qix3QkFBSSxLQUFLLGlCQUFpQixLQUFLLGNBQWMsUUFBUTtBQUFhO0FBQ2xFLHlCQUFLLGdDQUFnQyxRQUFNO0FBQ3pDLDJCQUFLLG9CQUFvQjtBQUFBLHVCQUN4QixLQUFLO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLaEIscUJBQVMsUUFBUSxZQUFZO0FBQUE7QUFBQSxVQUUvQixxQkFBcUIsNkJBQTZCLElBQUk7QUFDcEQsZ0JBQUksQ0FBQyxHQUFHLEtBQUs7QUFHWCxrQkFBSTtBQUNGLG1CQUFHLE1BQU0sSUFBSSxVQUFVO0FBQUEsdUJBQ2hCLE9BQVA7QUFDQSwyQkFBVyxNQUFNO0FBQ2Ysd0JBQU07QUFBQSxtQkFDTDtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBSVQsT0FBTyxlQUFlLFdBQVcsT0FBTztBQUN0QyxnQkFBSSxDQUFDLE1BQU0sS0FBSztBQUNkLG9CQUFNLE1BQU0sSUFBSSxVQUFVLE9BQU87QUFBQTtBQUFBO0FBQUEsVUFHckMsa0JBQWtCLDBCQUEwQixNQUFNLFVBQVU7QUFDMUQsaUJBQUssZ0JBQWdCLFFBQVE7QUFBQTtBQUFBLFVBRS9CLHdCQUF3QixnQ0FBZ0MsVUFBVTtBQUNoRSxpQkFBSyx3QkFBd0IsS0FBSztBQUFBO0FBQUEsVUFFcEMsOEJBQThCLHNDQUFzQyxVQUFVO0FBQzVFLGlCQUFLLDhCQUE4QixLQUFLO0FBQUE7QUFBQTtBQUk1QyxZQUFJLENBQUMsYUFBYTtBQUNoQixpQkFBTyxTQUFTO0FBRWhCLGNBQUksT0FBTyxvQkFBb0I7QUFDN0IsbUJBQU8sbUJBQW1CLFdBQVk7QUFDcEMscUJBQU8sT0FBTztBQUFBO0FBQUEsaUJBRVg7QUFDTCxtQkFBTyxPQUFPO0FBQUE7QUFBQTtBQUlsQixlQUFPO0FBQUE7QUFBQTtBQUFBOzs7QUMxNURULHdCQUFPO0FBV1AsTUFBTSxpQkFBaUIsV0FBVztBQUM5QixVQUFNLE9BQU8sU0FBUztBQUV0QixRQUFJLEtBQUssbUJBQW1CO0FBQ3hCLFdBQUs7QUFBQTtBQUlULFFBQUksS0FBSyx5QkFBeUI7QUFDOUIsV0FBSztBQUFBO0FBSVQsUUFBSSxLQUFLLHFCQUFxQjtBQUMxQixXQUFLO0FBQUE7QUFBQTtBQUliLE1BQU0sa0JBQWtCLFdBQVc7QUFDL0IsUUFBSSxTQUFTLGdCQUFnQjtBQUN6QixlQUFTO0FBQUE7QUFJYixRQUFJLFNBQVMsc0JBQXNCO0FBQy9CLGVBQVM7QUFBQTtBQUliLFFBQUksU0FBUyxrQkFBa0I7QUFDM0IsZUFBUztBQUFBO0FBQUE7QUFJakIsU0FBTyxpQkFBaUI7QUFDeEIsU0FBTyxrQkFBa0I7IiwKICAibmFtZXMiOiBbXQp9Cg==
