from os.path import abspath, dirname, join
from logging import DEBUG, WARNING


# Define root directory
root_dir = dirname(dirname(abspath(__file__)))


class BaseConfig(object):
    """
    Default config
    """

    # Configure log handler
    # (1) Logfile size limit
    LOG_MAX_BYTES = 1024 * 1024

    # (2) Logfile backup limit
    LOG_BACKUP_COUNT = 5

    # Limit upload size to 128 KB
    MAX_CONTENT_LENGTH = 128 * 1024


class Development(BaseConfig):
    """
    'Development' config
    """

    # Define logfile
    LOG_FILE = join(root_dir, 'logs', 'debug.log')

    # Define loglevel
    LOG_LEVEL = DEBUG


class Production(BaseConfig):
    """
    'Production' config
    """

    # Define logfile
    LOG_FILE = join(root_dir, 'logs', 'live.log')

    # Define loglevel
    LOG_LEVEL = WARNING


config = {
    'development': Development,
    'production': Production,
}
