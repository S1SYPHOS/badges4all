from os.path import exists, join
from io import BytesIO
from urllib.parse import unquote

from bs4 import BeautifulSoup as bs
from cairosvg import svg2png
from cb_badges import Badges
from flask import Blueprint, abort, current_app, render_template_string, request, send_file
from markupsafe import escape


# Initialize blueprint
api = Blueprint('api', __name__)


@api.route('/getiton')
@api.route('/getiton/<string:theme>.<string:extension>')
def getiton(theme: str = 'light', extension: str = 'svg'):
    # Check API parameters
    # (1) Retrieve extension and ..
    # (a) .. escape it & convert to lowercase
    extension = escape(extension).lower()

    # (b) .. validate it
    if extension not in ['svg', 'png']:
        # If invalid, abort with 'Not Found' error
        abort(404)

    # Map themes & their corresponding background colors
    themes = {
        'light': '#ffffff',
        'dark': '#000000',
        'legacy': '#000000',
    }

    # (2) Retrieve theme and ..
    # (a) .. escape it
    theme = escape(theme)

    # (b) .. validate it
    if theme not in themes:
        # If invalid, abort with 'Not Found' error
        abort(404)

    # Process query parameters
    # (1) Text to be inserted
    text = escape(request.args.get('text', 'GET IT ON'))

    # (2) Background color
    color = escape(unquote(request.args.get('color', themes[theme])))

    # (2) Background color
    stroke = escape(unquote(request.args.get('stroke', '#a6a6a6')))

    # (3) Image width (PNG only)
    png_width = escape(request.args.get('width', 564))

    # Attempt to ..
    try:
        # .. initalize object
        badges = Badges('codeberg')

        # .. prepare data
        data = {
            'bg': color,
            'text': text,
            'stroke': stroke,
        }

        # .. write SVG string to file
        xml = badges.render(data, theme)

    # .. otherwise ..
    except Exception as error:
        # (1) .. print error message
        # TODO: Custom error message
        abort(404)

    # Create file-like SVG object
    svgio = BytesIO(bytes(str(xml), 'utf-8'))

    # Set pointer to beginning
    svgio.seek(0)

    if extension == 'svg':
        return send_file(svgio, mimetype = 'image/svg+xml')

    if extension == 'png':
        # Create file-like object
        pngio = BytesIO()

        # Write PNG to it
        svg2png(file_obj = svgio, write_to = pngio, scale = int(png_width) / 564)

        # Set pointer to beginning
        pngio.seek(0)

        return send_file(pngio, mimetype = 'image/png')


@api.route('/badge')
@api.route('/badge/<string:template>.svg')
def badge(template: str):
    pass
