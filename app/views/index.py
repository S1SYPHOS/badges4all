from flask import Blueprint, render_template


# Initialize blueprint
index = Blueprint('index', __name__)


@index.route('/')
def idx():
    return render_template('index.html')
