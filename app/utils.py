from hashlib import md5


def data2hash(data: list) -> str:
    """
    Creates hash from data structure
    """

    return md5(str(data).encode('utf-8')).hexdigest()


def dedupe(duped_data: list, encoding: str = 'utf-8') -> list:
    """
    Removes duplicates from a given data structure
    """

    codes = set()
    deduped_data = []

    for item in duped_data:
        hash_digest = md5(str(item).encode(encoding)).hexdigest()

        if hash_digest not in codes:
            codes.add(hash_digest)
            deduped_data.append(item)

    return deduped_data


# See https://stackoverflow.com/a/14996816
def human_size(string: str) -> str:
    """
    Provides human-readable filesize
    """

    # Import functions
    from os.path import exists, getsize

    # Prepare supported filesize abbreviations
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']

    # Get relevant length (assuming `base64` string)
    nbytes = len(string) * 3 / 4 - string.count('=', -2)

    # If string resembles filepath ..
    if exists(string):
        # .. get its filesize instead
        nbytes = getsize(string)

    index = 0

    while nbytes >= 1024 and index < len(suffixes) - 1:
        nbytes /= 1024.
        index += 1

    size = ('%.2f' % nbytes).rstrip('0').rstrip('.')

    return '%s %s' % (size, suffixes[index])


def load_json(json_file: str) -> dict:
    """
    Loads contents of given JSON file
    """

    # Import library
    from json import load
    from json.decoder import JSONDecodeError

    # Create data array
    data = {}

    # Attempt to ..
    try:
        # .. open JSON file and ..
        with open(json_file, 'r') as file:
            # .. load its contents
            data = load(file)

    # .. otherwise
    except JSONDecodeError:
        # .. let it go
        pass

    return data
